<div class="container-fluid py-4">
    <div class="row g-2">
        <?= $this->session->flashdata('pesan') ?>
    </div>
</div>
<div class="container-fluid py-4">
    <div class="container mb-5">
        <div class="col-lg my-auto text-end">
            <button type="button" class="btn btn-primary ms-auto col-lg-2 justify-content-end py-2 px-2" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">Add Team </button>
        </div>

        <div class="row g-2">
            <?php foreach ($select_teams as $key => $value) {
            ?>
                <div class="col-md-4" onclick="window.location='<?= base_url(); ?>admin/team_detail/<?= $value['id']; ?> '">
                    <a href="<?= base_url(); ?>admin/team_detail/<?= $value['user_id']; ?> "></a>
                    <div class="card p-2 py-3 text-center" style="background-image: url('../assets/img/curved-images/curved7.jpg'); background-position-y:50%;">
                        <div class="img mb-2"> <img src="<?= base_url(); ?>assets/img/profile/<?= $value['images']; ?>" width="80" height="80" class="rounded-circle">
                        </div>
                        <h5 class="mb-0" style="color: aliceblue;"><?= $value['nama']; ?></h5>
                        <small style="color: aliceblue" ;><?= $value['jabatan']; ?></small>

                        <div class="mt-4">
                            <div class="row bg-opa-50 p-2 m-0 border-radius-top-start border-radius-top-end bg-opacity-10">

                                <div class="col-md-3">
                                    <p class="font-team">Pending</p>
                                    <p class="font-team"><?= $taskPending[$value['user_id']] ?></p>

                                </div>
                                <div class="col-md-3">
                                    <p class="font-team">Progress</p>
                                    <p class="font-team"><?= $taskProgress[$value['user_id']] ?></p>

                                </div>
                                <div class="col-md-3">
                                    <p class="font-team">Done</p>
                                    <p class="font-team"><?= $taskDone[$value['user_id']] ?></p>

                                </div>
                                <div class="col-md-3">
                                    <p class="font-team">Total</p>
                                    <p class="font-team"><?= $totTask[$value['user_id']] ?></p>

                                </div>



                            </div>
                        </div>
                        <div class="">
                            <div class="row bg-kencana-50 p-2 m-0 border-radius-bottom-start border-radius-bottom-end ">

                                <div class="col-md-12">
                                    <p class="font-team-point">Point</p>
                                    <p class="font-team-point"><?= $taskPending[$value['user_id']] ?></p>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
</div>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="register">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Team</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <!-- add project -->
                <section class="section dashboard">


                    <form id="addTeamForm" class="user" method="post" novalidate action="<?= base_url('admin/add_team'); ?>" onsubmit="return validateForm()">


                        <div class="form-group row">
                            <div class="col-lg mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" id="username" name="username" placeholder="User Name" value="<?= set_value('username') ?>" required>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder="Nama" value="<?= set_value('nama') ?>" required>
                                <?= form_error('name', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="E-mail" value="<?= set_value('email') ?>" required>
                                <?= form_error('email', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="text" class="form-control form-control-user" id="jabatan" name="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan') ?>" required>
                                <?= form_error('jabatan', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <input type="number" class="form-control form-control-user" id="nohp" name="nohp" placeholder="No Telepon" value="<?= set_value('nohp') ?>" required>
                                <?= form_error('nohp', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password" required>
                                <?= form_error('password1', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Retype Password" required>
                                <?= form_error('password2', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg position-relative">
                                <select class="form-select" aria-label="Default select example" name="role_id" id="role_id" required>
                                    <option value="" disabled selected>Role</option>
                                    <option value="1">Administrator</option>
                                    <option value="2">Developer</option>
                                    <option value="3">Bussines Analyst</option>
                                    <option value="4">System Analyst</option>
                                    <option value="5">Project Qulity Asurance</option>
                                    <option value="6">Project Manager Officer</option>
                                </select>
                                <?= form_error('role_id', '<small class="text-danger" pl-3>', '</small>'); ?>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Add Team
                        </button>

                    </form>
                </section>
            </div>
        </div>
    </div>
</div>
<script>
    function validateForm() {
        var nama = document.getElementById('nama').value.trim();
        var username = document.getElementById('username').value.trim();
        var email = document.getElementById('email').value.trim();
        var jabatan = document.getElementById('jabatan').value.trim();
        var nohp = document.getElementById('nohp').value.trim();
        var password1 = document.getElementById('password1').value.trim();
        var password2 = document.getElementById('password2').value.trim();
        var role_id = document.getElementById('role_id').value.trim();

        if (nama === '' || username === '' || email === '' || jabatan === '' || nohp === '' || password1 === '' ||
            password2 === '' || role_id === '') {
            alert('isi dulu dong guys, jangan dikosongin, ini bukan hati.');
            return false;
        }


        if (password1 !== password2) {
            alert('hhmm password gak sama, gak sejalan nih.');
            return false;
        }

        if (password1.length < 4) {
            alert('Passwordnya kependekan tuh, panjangin lah napa.');
            return false;
        }

        var emailRegex = /^[\w-]+@(gmail\.com|jalin\.co\.id)$/;
        if (!email.includes('@gmail.com') && !email.includes('@jalin.co.id')) {
            alert('Masukin Emailnya yang bener, contoh nya @gmail.com atau @jalin.co.id.');
            return false;
        }
    }
</script>