var DropzoneDemo = {
    init: function() {
        Dropzone.options.mDropzoneOne = {
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            accept: function(e, o) {
                "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
            }
        }, Dropzone.options.mDropzoneTwo = {
			url: base_url + 'C_Branch/uploadThumb',
            paramName: "file",
            maxFiles: 20,
            maxFilesize: 2,
            addRemoveLinks: !0,
            acceptedFiles: "image/*",
			init: function() {
		        var $this = this;
				// $this.on("addedfile", function(file) {
				// 	if (this.files.length) {
				// 	    var _i, _len;
				// 	    for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
				// 	        if( this.files[_i].name === file.name ) {
				// 	            this.removeFile(file);
				// 	        }
				// 	    }
				// 	}
				// });
				$this.on('removedfile', function (file) {
		            $.ajax({
		                url: base_url + 'C_Branch/removeUploaded',
		                type: "POST",
		                data: {'file_name': file.name}
		            });
		        });
		    },
        }, Dropzone.options.mDropzoneThree = {
			url: base_url + 'C_Branch/uploadIE',
            paramName: "file",
            maxFiles: 20,
            maxFilesize: 2,
            addRemoveLinks: !0,
            acceptedFiles: "image/*",
			init: function() {
		        var $this = this;
				// $this.on("addedfile", function(file) {
				// 	if (this.files.length) {
				// 	    var _i, _len;
				// 	    for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
				// 	        if( this.files[_i].name === file.name ) {
				// 	            this.removeFile(file);
				// 	        }
				// 	    }
				// 	}
				// });
				$this.on('removedfile', function (file) {
		            $.ajax({
		                url: base_url + 'C_Branch/removeUploaded',
		                type: "POST",
		                data: {'file_name': file.name}
		            });
		        });
		    },
			success:function(file, response) {
		        console.log(response);
		    }
        }
    }
};
DropzoneDemo.init();
