lots = false;
function changeLot(a) {
	lots = a;
}
function removeMat(id) {
	$('#matgroup' + id).remove();
}

function calculateTagihan() {
	if (!lots) {
		var totTagihan = 0;
		console.log("mat:" + mat)
		for (let i = 0; i < mat; i++) {
			var idx = i + 1;
			var totcost = $('#totcost' + idx).val();
			totTagihan += curToNum(totcost);
		}
		$('#tagihan').val(formatMoney(totTagihan));
	}
}
function formatMoney(number) {
	return parseInt(number).toLocaleString('in-ID', {
		style: "currency",
		currency: "IDR",
		minimumFractionDigits: 0,
		maximumFractionDigits: 0
	});

}
function calculateMan(no){
	var totcost = $('#totcost' + no);
	totcost.val(formatMoney(isNaN(totcost.val()) ? 0 : totcost.val()));
	calculateTagihan();
}
function calculate(no) {


	var p = $('#p' + no).val();
	var l = $('#l' + no).val();
	var t = $('#t' + no).val();
	var b = $('#b' + no).val();
	var qty = $('#q' + no).val();
	var cost = $('#cost' + no).val();
	var costbill = $('#costbill' + no).val();
	cost = curToNum(cost);
	var k = $('#k' + no);
	var ton = $('#ton' + no);
	var totcost = $('#totcost' + no);
	totcost.attr("readonly", true); 
	var kubikasi = (p * l * t / 1000000) * qty;
	var tonase = b * qty;
	k.val(kubikasi.toFixed(3));
	ton.val(tonase);
	var total = 0;
	if (cost.toString().length > 0) {
		if (costbill == 1) {
			total = (ton.val() / 1000) * cost;
		} else if (costbill == 2) {
			total = ton.val() * cost;
		} else if (costbill == 3) {
			total = kubikasi.toFixed(3) * cost;
		} else if (costbill == 4) {
			total = qty * cost;
		} else if (costbill == 5) {
			totcost.attr("readonly", false); 
			total = cost;
		}else {
			if (kubikasi.toFixed(3) > (ton.val() / 1000)) {
				// alert(kubikasi.toFixed(3));
				total = kubikasi.toFixed(3) * cost;
			} else {
				total = (ton.val() / 1000) * cost;
			}
		}
		$('#cost' + no).val(formatMoney(cost));
	}
	totcost.val(formatMoney(isNaN(total) ? 0 : total));
	if (!lots) {
		calculateTagihan();
	}
}
