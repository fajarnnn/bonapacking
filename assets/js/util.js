function formatMoney(number) {
    return parseInt(number).toLocaleString('in-ID', {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
    });

}


