<?php
use Mpdf\Mpdf;

class tcpddf extends Mpdf
{
	function __construct()
	{
		parent::__construct();
	}
	protected function ci()
	{
		return get_instance();
	}
	function loadpdf($view, $data = array())
	{
		$pdf = new Mpdf();

		// Create new PDF document
		$html = $this->ci()->load->view($view, $data, TRUE);
		$pdf->WriteHTML($html);
		$pdf->Output();
	}
}
?>
