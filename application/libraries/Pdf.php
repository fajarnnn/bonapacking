<?php

use Dompdf\Dompdf;

class Pdf extends DOMPDF
{
	/**
	 * Get an instance of CodeIgniter
	 *
	 * @access  protected
	 * @return  void
	 */
	protected function ci()
	{
		return get_instance();
	}

	/**
	 * Load a CodeIgniter view into domPDF
	 *
	 * @access  public
	 * @param   string  $view The view to load
	 * @param   array   $data The view data
	 * @return  void
	 */
	public function load_view($view, $data = array(), $pl)
	{
		$dompdf = new Dompdf(['isRemoteEnabled' => true]);
		// $dompdf->set_base_path(realpath(base_url() . '/assets/css/'));
		$html = $this->ci()->load->view($view, $data, TRUE);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('Letter', 'landscape');

		// Render the HTML as PDF
		$dompdf->render();
		$time = time();
		// ob_end_clean();
		// Output the generated PDF to Browser
		$dompdf->stream($pl);
	}
	public function load_lampiran($view, $data = array(), $pl)
	{
		$dompdf = new Dompdf(['isRemoteEnabled' => true]);
		// $dompdf->set_base_path(realpath(base_url() . '/assets/css/'));
		$html = $this->ci()->load->view($view, $data, TRUE);
		// $html = preg_replace('/>\s+</', "><", $html);
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('Letter', 'potrait');

		// Render the HTML as PDF
		$dompdf->render();
		$time = time();
		// ob_end_clean();
		// Output the generated PDF to Browser
		$dompdf->stream($pl);
	}
	public function printSJN($view, $data = array(), $sjn)
	{
		$dompdf = new Dompdf(['isRemoteEnabled' => true]);
		// $dompdf->set_base_path(realpath(base_url() . '/assets/css/'));
		$html = $this->ci()->load->view($view, $data, TRUE);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper(array(0, 0, 595, 396), 'potrait');

		// Render the HTML as PDF
		$dompdf->render();
		$time = time();
		// ob_end_clean();
		// Output the generated PDF to Browser
		$dompdf->stream("SJN-" + $sjn);
	}
	public function printInvoices($view, $data = array(), $invoiceNumber)
	{
		$dompdf = new Dompdf(['isRemoteEnabled' => true]);
		// $dompdf->set_base_path(realpath(base_url() . '/assets/css/'));
		$html = $this->ci()->load->view($view, $data, TRUE);

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		// $dompdf->setPaper(array(0, 0, 594.72, 419.76), 'landscape');
// exit;
		// Render the HTML as PDF
		// $dompdf->setPaper('A4', 'potrait');

		$dompdf->render();
		// $time = time();
		// ob_end_clean();
		// Output the generated PDF to Browser
		// echo $invoiceNumber;
		$dompdf->stream("Invoice-" . $invoiceNumber);
	}
	function geninv()
	{

		$browserFactory = new HeadlessChromium\BrowserFactory();

		// starts headless Chrome
		$browser = $browserFactory->createBrowser();

		try {
			// creates a new page and navigate to an URL
			$page = $browser->createPage();
			$page->navigate('http://localhost:8081/CodeIgniter-3.1.12/packing/printinvoice')->waitForNavigation();

			// get page title
			$pageTitle = $page->evaluate('document.title')->getReturnValue();

			// screenshot - Say "Cheese"! 😄
			// $page->screenshot()->saveToFile(base_url('assets/'));

			// pdf
			$page->pdf(['printBackground' => false])->saveToFile("./assets/test.pdf");
		} finally {
			// bye
			$browser->close();
		}
	}
}
