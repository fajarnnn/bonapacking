<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FileGenerator extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('isLogin'))
			redirect(base_url());
		$this->load->Model('M_packing');
		$this->load->Model('M_accounting');

	}
	public function index()
	{
	}
	function generatePDF($pl, $ara)
	{
		$where = array(
			'no_pl' => $pl,
			'area' => rawurldecode($ara)
		);
		$packlist = $this->M_packing->genPdf($where)->result();
		$group = array();
		foreach ($packlist as $key => $value) {
			$penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br><strong>" . $value->plant . "</strong>";
			$name = $value->name;
			$noship = $value->no_shipping;
			$method = $value->method;
			$noveh = $value->no_vehicle;
			$voyage = $value->no_voyage;
			$po = $value->no_po;
			$sji = $value->no_sji;
			$dateOut = $value->date_out;
			if (!in_array($value->list_group, $group)) {
				$group[] = $value->list_group;
			}

			if (!isset($po) || $po == '-') {
				$dKey = $sji;
			} else {
				$dKey = $po;
			}
			if (isset($sji) && isset($po)) {
				$dKey = $po . '<br/>' . $sji;
			}

			if (!isset($arr[$dKey])) {
				$arr[$penerima][$dKey][] = $value;
			}
		}
		// echo $driver;
		$datas['pl_detail'] = array(
			'driver' => $name,
			'vehicle' => $noveh,
			'phone' => $noship,
			'pl' => $pl,
			'area' => rawurldecode($ara),
			'grup' => implode(",", $group),
			'date_out' => $dateOut,
			'method' => $method
		);
		// $datetime = "20130409163705"; 
		// $d = DateTime::createFromFormat("YmdHis", $datas['pl_detail']['date_out']);
		// echo $d->format("d/m/Y H:i:s");

		if ($pl != "-") {
			$dd = strtotime($datas['pl_detail']['date_out']);
			setlocale(LC_ALL, 'ID_IN');
			$datas['pl_detail']['date_out'] = strftime('%d %B %Y', $dd);
		}
		// exit();
		// array_push($arr[$penerima][$po], $mat);
		// array_push($arr[$penerima][$po], $jml);
		// var_dump($arr);
		// exit;
		// foreach ($arr as $key => $value) {
		// 	# code...
		// 	foreach ($value as $key2 => $value2) {
		// 		# code...
		// echo $key. $value2->material ."<br/>";
		// 	}
		// 	// break;
		// }
		$datas['details'] = $arr;
		$this->load->library('pdf');
		$this->pdf->load_view('printNative', $datas, $pl);
		// $data['packlist'] = $arr;
		// $this->load->view('printNative', $datas);
	}
	function lampiranRetail($packlist, $no)
	{
		$total = 0;
		$totalQty = 0;
		foreach ($packlist as $key => $value) {
			$data['cost'] = $value->cost;
			// $kubikasi += $value->kubikasi;
			$data['inkubik'] = $value->in_kubikasi;
			// $data['pos'][$value->no_po . $value->no_sji] = 0;
			$data['ppn'] = $value->ppn;
			$data['cost_type'] = $value->cost_type;
			$data['rekening_type'] = $value->rekening_type;
			$data['tujuanInvoice'] = $value->tujuan;
			$data['invoice_date'] = $value->invoice_date;
			$data['supplier'] = $value->supplier;
			$data['corporate'] = $value->corporate;

			$mats = array(
				"qty" => $value->qty,
				"material" => $value->material,
				"satuan" => $value->satuan,
				"tonase" => $value->tonase,
				"berat" => $value->berat,
				"kubikasi" => $value->kubikasi,
				"cost" => number_format($value->cost, 0, ",", "."),
				"totalcost" => $value->totalcost,
				"no_po" => $value->no_po,
				"no_sji" => $value->no_sji,
				"costbill" => $value->costbill,
			);
			$total += $value->totalcost;
			$totalQty += $value->qty;
			if ($value->costbill > 0) {
				$sumton = $value->cost * ($value->tonase / 1000);
				$sumkub = round($value->cost * ($value->kubikasi));
				// echo $sumkub."-".$value['totalcost'];
				if ($sumkub == $value->totalcost) {
					$volum = $value->kubikasi;
				} else if ($sumton == $value->totalcost && $value->tonase != null) {
					$volum = $value->tonase;
				} else {
					if ($value->costbill == 1) {
						$volum = $value->berat;
					} else if ($value->costbill == 2) {
						$volum = $value->tonase;
					} else if ($value->costbill == 3) {
						$volum = $value->kubikasi;
					} else if ($value->costbill == 4) {
						$volum = "PERUNIT";
					} else if ($value->costbill == 5) {
						$volum = "LOT";
					}
				}
				$data['cb'][$value->costbill] = 0;
			} else {
				// echo ($plntval['cost'] * ($plntval['tonase']/1000));
				$berat = is_numeric($value->tonase) ? floatval($value->tonase) : 0;
				$kubik = is_numeric($value->kubikasi) ? floatval($value->kubikasi) : 0;
				$ton = $berat / 1000;

				if ($ton > $kubik) {
					$volum = $ton;
					$data['cb']['1'] = 0;
				} else {
					$volum = $kubik;
					$data['cb']['3'] = 0;
				}
			}
			$mats['volum'] = $volum;
			$posji = $value->no_po . ($value->no_sji != "-" ? "____" . $value->no_sji : "");
			$data['data'][$value->no_pl][$value->corporate][$posji][] = $mats;
			usort($data['data'][$value->no_pl][$value->corporate][$posji], function ($a, $b) {
				return [$a['satuan'], $a['material']] <=> [$b['satuan'], $b['material']];
			});
		}
		$data['total'] = $total;
		$data['totalQty'] = $totalQty;
		return $data;

	}
	function lampiranMultiCorp($packlist, $no)
	{
		// echo $no;
		$total = 0;
		$totalQty = 0;
		foreach ($packlist as $key => $value) {
			$data['cost'] = $value->cost;
			// $kubikasi += $value->kubikasi;
			$data['inkubik'] = $value->in_kubikasi;
			// $data['pos'][$value->no_po . $value->no_sji] = 0;
			$data['ppn'] = $value->ppn;
			$data['cost_type'] = $value->cost_type;
			$data['rekening_type'] = $value->rekening_type;
			$data['tujuanInvoice'] = $value->tujuan;
			$data['invoice_date'] = $value->invoice_date;
			$data['supplier'] = $value->supplier;
			$data['corporate'] = $value->corporate;
			if ($no == 1 || $no == 3)
				$arrval = $value->area;
			else
				$arrval = "-";

			if ($no == 1 || $no == 3)
				$corval = $value->corporate;
			else
				$corval = "-";
			// echo $corval;
			// $data['data'][$value->corporate][$arrval][$value->no_po . "____" . $value->no_sji];
			$mats = array(
				"qty" => $value->qty,
				"material" => $value->material,
				"satuan" => $value->satuan,
				"tonase" => $value->tonase,
				"berat" => $value->berat,
				"kubikasi" => $value->kubikasi,
				"cost" => number_format($value->cost, 0, ",", "."),
				"totalcost" => $value->totalcost,
				"no_po" => $value->no_po,
				"no_sji" => $value->no_sji,
				"costbill" => $value->costbill,
			);
			$total += $value->totalcost;
			$totalQty += $value->qty;
			$volum = "";
			if ($value->costbill > 0) {
				$sumton = $value->cost * ($value->tonase / 1000);
				$sumkub = round($value->cost * ($value->kubikasi));
				// echo $sumkub."-".$value['totalcost'];
				if ($sumkub == $value->totalcost) {
					$volum = $value->kubikasi;
				} else if ($sumton == $value->totalcost && $value->tonase != null) {
					$volum = $value->tonase;
				} else {
					if ($value->costbill == 1) {
						$volum = $value->berat;
					} else if ($value->costbill == 2) {
						$volum = $value->tonase;
					} else if ($value->costbill == 3) {
						$volum = $value->kubikasi;
					} else if ($value->costbill == 4) {
						$volum = "PERUNIT";
					} else if ($value->costbill == 5) {
						$volum = "LOT";
					}
				}
				$data['cb'][$value->costbill] = 0;
			} else {
				// echo ($plntval['cost'] * ($plntval['tonase']/1000));
				$berat = is_numeric($value->tonase) ? floatval($value->tonase) : 0;
				$kubik = is_numeric($value->kubikasi) ? floatval($value->kubikasi) : 0;
				$ton = $berat / 1000;

				if ($ton > $kubik) {
					$volum = $ton;
					$data['cb']['1'] = 0;
				} else {
					$volum = $kubik;
					$data['cb']['3'] = 0;
				}
			}
			$mats['volum'] = $volum;
			$val = "-";
			if ($value->list_group == "LONSUM") {
				$val = $value->plant;
			}
			$posji = $value->no_po . ($value->no_sji != "-" ? "____" . $value->no_sji : "");
			$data['data'][$corval][$value->no_pl][$arrval][$val][$posji][] = $mats;
			// usort($data, fn($a, $b) => $a['data'][$corval][$arrval][$val][$posji][][] <=> $b['earnest_money_due']['value']);
			usort($data['data'][$corval][$value->no_pl][$arrval][$val][$posji], function ($a, $b) {
				return [$a['satuan'], $a['material']] <=> [$b['satuan'], $b['material']];
			});
		}
		$data['total'] = $total;
		$data['totalQty'] = $totalQty;
		return $data;
	}
	function generateLampiran($invNums)
	{
		$invNum = base64url_decode($invNums);
		$where = array(
			'no_invoice' => $invNum
		);
		$packlist = $this->M_accounting->genPdf($where)->result();
		$map = function ($v) {
			return $v->corporate;
		};
		$corpCount = array_count_values(array_map($map, $packlist));
		$map = function ($v) {
			return $v->area;
		};
		$areaCount = array_count_values(array_map($map, $packlist));
		$tujuan = array_keys($areaCount);
		$mats = [];
		$total = 0;
		$totalQty = 0.0;
		$views = "";
		$views = "lampiranInvoiceMultiCorp";
		if(strtoupper($packlist[0]->list_group) == "RETAIL" && strpos(strtoupper($packlist[0]->area), "SMART") != true){
			$data = $this->lampiranRetail($packlist, 1);
			$views = "lampiranInvoiceRetail";
		}else if (count($corpCount) > 1) {
			$data = $this->lampiranMultiCorp($packlist, 1);
		} else if (count($corpCount) == 1 && count($areaCount) > 1) {
			$data = $this->lampiranMultiCorp($packlist, 3);
		} else {
			$data = $this->lampiranMultiCorp($packlist, 2);
		}
		$total = $data['total'];
		$totalQty = $data['totalQty'];
		$data['invoice_number'] = $invNum;
		$data['tujuan'] = implode(",", $tujuan);
		$allTotal = 0;
		$ppn = 0;
		// var_dump($data['rekening_type']);
		if ($data['rekening_type'] == "PPN" && $data['cost_type'] == "Belum PPN") {

			$ppn = floatval($total) * 1.1 / 100;
			if (intval($data['ppn']) > 0)
				$ppn = $data['ppn'];
			$allTotal = $total + $ppn;

		} else if ($data['rekening_type'] == "PPN" && $data['cost_type'] == "Sudah PPN") {
			$ppn = floatval($total) * 1.1 / 100;
			if (intval($data['ppn']) > 0)
				$ppn = $data['ppn'];
			$allTotal = $total;
			$data['dpp'] = number_format(round($total - $ppn), 0, ",", ".");
		} else if ($data['rekening_type'] == "NON PPN") {
			$allTotal = $total;
			$ppn = 0;
		} else {
			$allTotal = $total;
			$ppn = 0;
		}
		$data['total'] = number_format($total, 0, ',', '.');
		$data['allTotal'] = number_format($allTotal, 0, ',', '.');
		$data['ppn'] = number_format($ppn, 0, ',', '.');
		$data['totqty'] = number_format($totalQty, 0, ',', '.');

		// echo "<pre>";
		// var_dump($data);
		// exit;
		$filenumber = str_replace("/", "_", $data['invoice_number']);
		$this->load->library('pdf');
		$this->pdf->load_lampiran($views, $data, $filenumber . "-Lampiran");
		// $data['packlist'] = $arr;
		$this->load->view($views, $data);
	}
	function generateAllExcel()
	{
		$packlist = $this->M_packing->joinAll()->result();
		// echo "<pre>";
		// // var_dump($packlist);
		// echo "</pre>";
		// ;
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("assets/media/PackingListTemp.xlsx");
		$sheet = $spreadsheet->getActiveSheet();
		$no = 3;
		foreach ($packlist as $key => $val) {
			$sheet->setCellValue('A' . $no, $val->no_pl);
			$sheet->setCellValue('B' . $no, $val->no_po);
			$sheet->setCellValue('C' . $no, $val->no_sji);
			$sheet->setCellValue('D' . $no, $val->date_in);
			$sheet->setCellValue('E' . $no, $val->date_out);
			$sheet->setCellValue('F' . $no, $val->doc_return);
			$sheet->setCellValue('G' . $no, $val->unloading_date);
			$sheet->setCellValue('H' . $no, $val->supplier);
			$sheet->setCellValue('I' . $no, $val->material);
			$sheet->setCellValue('J' . $no, $val->qty);
			$sheet->setCellValue('K' . $no, $val->satuan);
			$sheet->setCellValue('L' . $no, $val->panjang);
			$sheet->setCellValue('M' . $no, $val->lebar);
			$sheet->setCellValue('N' . $no, $val->tinggi);
			$sheet->setCellValue('O' . $no, $val->kubikasi);
			$sheet->setCellValue('P' . $no, $val->berat);
			$sheet->setCellValue('Q' . $no, $val->area);
			$sheet->setCellValue('R' . $no, $val->corporate);
			$sheet->setCellValue('S' . $no, $val->address);
			$sheet->setCellValue('T' . $no, $val->street);
			$sheet->setCellValue('U' . $no, $val->pic);
			$sheet->setCellValue('V' . $no, $val->plant);
			$sheet->setCellValue('W' . $no, $val->cost);
			$sheet->setCellValue('X' . $no, $val->totalcost);
			$sheet->setCellValue('Y' . $no, $val->no_pot);
			$sheet->setCellValue('Z' . $no, $val->remark);
			$sheet->setCellValue('AA' . $no, $val->invoice_date);
			$sheet->setCellValue('AB' . $no, $val->no_invoice);
			$sheet->setCellValue('AC' . $no, $val->list_group);
			$driver = "";
			$vessel = "";
			$plate = "";
			$noCont = "";
			$phone = "";
			$nosegel = "";
			if ($val->method == "Truck") {
				$driver = $val->name;
				$plate = $val->no_vehicle;
				$phone = $val->no_shipping;
			} else {
				$vessel = $val->name;
				$noCont = $val->no_vehicle;
				$nosegel = $val->no_shipping;
			}
			$sheet->setCellValue('AD' . $no, $driver);
			$sheet->setCellValue('AE' . $no, $plate);
			$sheet->setCellValue('AF' . $no, $phone);
			$sheet->setCellValue('AG' . $no, $vessel);
			$sheet->setCellValue('AH' . $no, $noCont);
			$sheet->setCellValue('AI' . $no, $nosegel);
			$sheet->setCellValue('AJ' . $no, $val->input_by);
			$no++;
		}
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$filename = "PackingList.xlsx";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		ob_end_clean();
		exit($writer->save('php://output'));
	}
	function generateSheet()
	{
	}
	function printSjn($sjn, $prt, $po)
	{
		// echo angkaKeTeks("5600	");
		// exit;
		$isprint = $prt == "print" ? true : false;
		$sjn = base64url_decode($sjn);
		$pl = base64url_decode($po);
		$where = array(
			'no_sji' => $sjn,
			'pol.id' => $pl,
		);
		// } else {

		// 	$where = array('no_sji' => $sjn);
		// }

		// echo"<pre>";
		// var_dump($where);
		// exit;
		$datas = $this->M_packing->getSJNMaterial($where);
		usort($datas, function ($a, $b) {
			return [$a->no_po, $a->material] <=> [$b->no_po, $b->material];
		});
		$data['sjn'] = $datas;
		$data['isPrint'] = $isprint;
		$dt = $data['sjn'][0];
		$sheetNeed = ceil(count($datas) / 3);
		$nolist = 0;
		$totQty = 0;
		$lastPO = "";
		$last = false;
		for ($i = 0; $i < count($datas); $i++) {
			$spreadsheet[$i] = \PhpOffice\PhpSpreadsheet\IOFactory::load("assets/media/SJNTemplate.xlsx");
			$sheet = $spreadsheet[$i]->getActiveSheet();
			$no = 3;
			$sheet->setCellValue('A3', $dt->supplier);
			$sheet->setCellValue('F3', $dt->corporate);
			$addr = $dt->address . " " . $dt->street . "\n" . $dt->area . "\n" . $dt->pic;
			// if ($dt->list_group == "LONSUM") {
			$richText = new \PhpOffice\PhpSpreadsheet\RichText\RichText();
			$richText->createText("");
			$payable = $richText->createTextRun($dt->plant);
			$payable->getFont()->setBold(true);
			$richText->createText("\n" . $addr);
			// $addr = $HTMLCODE . "\n" . $dt->address . " " . $dt->street . "\n" . $dt->area . "\n" . $dt->pic;
			$sheet->setCellValue('F4', $richText);

			// } else {
			// 	$sheet->setCellValue('F4', $addr);

			// }
			// $sheet->setCellValue('F5', $dt->area);
			// $sheet->setCellValue('F6', $dt->pic);
			$no = 9;
			$stripcnt = 0;
			for ($k = $nolist; $k < count($datas); $k++) {
				$val = $datas[$k];
				$sheet->setCellValue('F' . 9, $val->no_pl);
				if ($val->material == "-" || strlen($val->material) < 3) {
					$stripcnt = $val->qty;
					$totQty += $val->qty;
					$nolist++;
					continue;
				}
				if ($lastPO == "") {
					$lastPO = $val->no_po;
				} else if ($lastPO != $val->no_po) {
					if (($no + 2) > 18) {
						break;
					}
					$sheet->setCellValue('D' . $no, $lastPO);
					$no++;
					$sheet->setCellValue('D' . $no, $val->no_sji);
					$no++;
					$lastPO = $val->no_po;
				}
				if (($no + 1) > 18) {
					break;
				}
				if (!$last) {
					$sheet->setCellValue('A' . $no, $val->qty == 0 ? "" : intval($val->qty) + $stripcnt);
					$sheet->setCellValue('C' . $no, $val->satuan);
					$sheet->setCellValue('D' . $no, $val->material);
					$no++;
				}
				// echo $nolist."=".count($datas);
				if ($nolist == (count($datas) - 1)) {
					if (($no + 2) > 18) {
						$last = true;
						break;
					}

					$nolist++;
					// echo $nolist."=".count($datas);

					$sheet->setCellValue('D' . $no, "PO.NO " . $val->no_po);
					$no++;
					$sheet->setCellValue('D' . $no, "SJN.NO " . $val->no_sji);
					$no++;
				} else {
					$nolist++;
				}
				$totQty += $val->qty;
				$stripcnt = 0;
			}
			if ($i > 0) {
				$sheet->setTitle("SURAT JALAN " . $i);

				$spreadsheet[0]->addExternalSheet($sheet);
			}
			$sheet->setCellValue('F20', "Jakarta, " . toIndoWord(date("d F Y")));
			if ($nolist == count($datas)) {
				$sheet->setCellValue('A18', $totQty);
				$sheet->setCellValue('C18', "COLLY");
				$sheet->setCellValue('D18', angkaKeTeks($totQty));
				break;
			}
		}
		// exit;
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet[0]);
		$filename = "SJN-" . $dt->no_sji . ".xlsx";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		ob_end_clean();
		exit($writer->save('php://output'));
		// $this->load->view('printSjn', $data);
		// $this->load->library('pdf');
		// $this->pdf->load_view('printSjn', $data, str_replace("/","_",$sjn));
	}
	function generateInvoice()
	{
		$datas = [];
		$this->load->library('pdf');
		$this->pdf->printInvoice('nativeInvoice', $datas, "456456");
		// $data['packlist'] = $arr;
		// $this->load->view('printNative', $datas);
	}
	function printInv($invNums)
	{
		$invNum = base64url_decode($invNums);
		$datas['inv'] = $this->M_accounting->printInvoice($invNum)->result_array();
		// $invNum = base64url_decode($a);
		// echo $invNum;
		// exit;
		// $datas['inv'] = $this->M_accounting->printInvoice($invNum)->result_array();
		// $inkb = $datas['inv'][(count($datas['inv']) - 1)]['in_kubikasi'];

		// if ($inkb != "0") {

		// 	$sum = array_reduce($datas['inv'], function ($carry, $item) {
		// 		return $carry + $item['kubikasi'];
		// 	});
		// 	// echo "<pre>";
		// 	// 	var_dump(end($datas['inv']));
		// 	$diff = $inkb - $sum;
		// 	$end = $datas['inv'][(count($datas['inv']) - 1)];
		// 	$datas['inv'][(count($datas['inv']) - 1)]['kubikasi'] = floatval($datas['inv'][(count($datas['inv']) - 1)]['kubikasi']) + $diff;
		// 	$datas['inv'][(count($datas['inv']) - 1)]['totalcost'] = floatval($end['cost']) * floatval($datas['inv'][(count($datas['inv']) - 1)]['kubikasi']);
		// 	// var_dump(floatval($inkb));
		// 	// var_dump(floatval($sum));
		// 	// var_dump(floatval($diff));
		// 	// var_dump(end($datas['inv']));
		// 	// exit;
		// }
		$isPO = true;
		$isSjn = true;
		$totalHarga = 0;
		$totalBerat = 0;
		$totalkubik = 0;
		$hargaBefore = 0;
		$total_cost = 0;
		$cost = 0;
		$ppn = 0;
		$qty = 0;
		$dt = [];
		$materialStr = "";
		$no = 0;
		$discountNom = 0;
		$addCost = 0;
		$totcol = 0;
		$discount = 0;
		$downPayment = 0;
		$subcost = 0;
		$matdata = [];
		$hargakubik = 0;
		$areas = [];
		foreach ($datas['inv'] as $keys => $values) {
			$areas[$values['area']] = 0;
			// $areas[] = $values->area;
			$no++;
			foreach ($values as $key => $value) {
				# code...
				if ($key == "no_po")
					$dt['no_po'][$value] = 0;
				if ($key == 'no_sji')
					$dt['no_sji'][$value] = 0;
				if ($key == 'satuan')
					$dt['satuan'][$value] = 0;
				if ($key == 'area')
					$dt['area'][$value] = 0;
				if ($key == 'cost' && $value != "0")
					$dt['cost'][$value] = 0;

			}
			$plus = " + ";
			if ($no < count($datas['inv']))
				$materialStr .= $values['material'] . $plus;
			else
				$materialStr .= $values['material'];
			$qty += $values['qty'];
			$totcol += $values['qty'];
			$totalBerat += $values['berat'] * $values['qty'];
			$totalkubik += $values['kubikasi'];
			$hargaBefore += $values['totalcost'];
			$addCost = $values['additional_cost'];
			$discount = $values['discount'];
			$downPayment = $values['down_payment'];
			if ($values['cost'] != 0)
				$cost = $values['cost'];
			$ppnDB = $values['ppn'];
			if ($values['costbill'] > 0) {
				$sumton = $values['cost'] * ($values['tonase'] / 1000);
				$sumkub = round($values['cost'] * ($values['kubikasi']));
				// echo $sumkub . "-" . $values['totalcost'];
				if ($sumkub == $values['totalcost']) {
					$matdata['cb']['3'] = 0;
				} else if ($sumton == $values['totalcost'] && $values['tonase'] != null) {
					$matdata['cb']['1'] = 0;
				} else {
					$matdata['cb'][$values['costbill']] = 0;
				}
				// $matdata['cb'][$values['cost']] = 0;
			} else {
				// echo ($values['cost'] * ($values['tonase']/1000));
				$berat = is_numeric($values['tonase']) ? floatval($values['tonase']) : 0;
				$kubik = is_numeric($values['kubikasi']) ? floatval($values['kubikasi']) : 0;
				$ton = $berat / 1000;
				if ($ton == 0 && $kubik == 0) {

				} else {

					if ($ton > $kubik) {
						$matdata['cb']['1'] = 0;
					} else {
						$matdata['cb']['3'] = 0;
					}
					$matdata['cost'][$values['cost']] = 0;
				}
			}

		}

		if (count($matdata['cb']) == 1) {
			foreach ($matdata['cb'] as $key => $value) {
				if ($key == 1) {
					$dt['qty'] = $totalBerat . " kg";
				} else if ($key == 3) {
					// echo "A";
					$dt['qty'] = $totalkubik . " m3";

				} else if ($key == 2) {
					$dt['qty'] = $totalBerat . " ton";
				} else if ($key == 4) {
					$dt['qty'] = $qty . " Unit";
				} else if ($key == 5) {
					$dt['qty'] = $qty . " Lot";
				}
			}
		} else {

			$cost = 0;
			$dt['qty'] = 1;
		}
		$dt['materialStr'] = $materialStr;

		if (count($dt['no_po']) > 1) {
			$dt['no_po'] = false;
			$dt['materialStr'] = "NAMA BARANG, JUMLAH BARANG DAN NOMOR PO SEMUA TERLAMPIR DI DALAM INVOICE (" . $totcol . " COLLY)";

		} else {
			if (strlen($materialStr) > 200) {
				$dt['materialStr'] = "NAMA BARANG, JUMLAH BARANG DAN NOMOR PO SEMUA TERLAMPIR DI DALAM INVOICE (" . $totcol . " COLLY)";
			}
		}
		if (count($dt['cost']) > 1) {
			$cost = 0;
			$dt['qty'] = 1;
		}
		if (count($dt['no_sji']) > 1) {
			$dt['no_sji'] = false;
		}
		if (count($dt['area']) > 1) {
			$dt['area'] = false;
		}
		$totalHarga = round(floatval($hargaBefore));
		if ($hargakubik > 0) {
			$totalHarga = round(floatval($hargakubik));
		}
		// var_dump($dt);
		// echo $cost;
		// exit;
		// echo "<br>HARGA AWAL:".$totalHarga;
		if (isset($discount)) {
			// echo "<br>DISCOUNT:".$discount;
			if (strpos($discount, "%") > 0) {
				$totdis = $totalHarga * (intval(str_replace("%", "", $discount)) / 100);
				$totalHarga -= $totdis;
				$discountNom = $totdis;
			} else if ($discount > 0) {
				$totalHarga -= $discount;
				$discountNom = $discount;
			}
		}

		if ($datas['inv'][0]['rekening_type'] == "NON PPN") {
			$ppn = 0;
		} else {

			// echo "<br>AFTER DISCOUNT:".$totalHarga;
			if ($datas['inv'][0]['cost_type'] == "Belum PPN") {
				$ppn = round($totalHarga * 1.1 / 100);
				if (isset($ppnDB)) {
					if ($ppnDB > 0)
						$ppn = $ppnDB;
				}
				$totalHarga = $totalHarga + $ppn;
			} else {
				$ppn = round($totalHarga * 1.1 / 100);
				if (isset($ppnDB)) {
					if ($ppnDB > 0)
						$ppn = $ppnDB;
				}
				$hargaBefore = $totalHarga - $ppn;
			}
		}
		if (isset($ppnDB)) {
			// echo "<br>PPN DB:".$ppn;
		}
		// echo "<br>AFTER PPN:".$totalHarga;
		if (isset($downPayment)) {
			// echo "<br>DP:".$downPayment;
			if ($downPayment > 0) {
				$totalHarga -= $downPayment;
			}
		}
		// echo "<br>AFTER DB:".$totalHarga;
		if (isset($addCost)) {
			// echo "<br>BIAYA LAIN:".$addCost;
			if ($addCost > 0) {
				$totalHarga += $addCost;
			}
		}

		// $this->generateLampiran($invNums);
		// echo "<br>AFTER BIAYA LAIN:".$totalHarga;
		// exit;
		$dt['areas'] = $areas;
		$dt['cost'] = number_format($cost, 0, ',', '.');
		// $dt['cost'] = $total_cost;
		$dt['hargaBefore'] = number_format($hargaBefore, 0, ',', '.');
		$dt['total_harga'] = number_format($totalHarga, 0, ',', '.');
		$dt['ppn'] = number_format($ppn, 0, ',', '.');
		$dt['discountNom'] = number_format($discountNom, 0, ',', '.');
		$dt['discount'] = $discount;
		$dt['downPayment'] = number_format($downPayment, 0, ',', '.');
		$dt['addCost'] = number_format($addCost, 0, ',', '.');
		$dt['terbilang'] = angkaKeTeks($totalHarga) . " rupiah";
		$datas['addon'] = $dt;
		// 		echo "<pre>";
// 		// var_dump($dt);
// 		var_dump($dt);
// exit;
		// var_dump($dt);
		$this->load->view('nativeInvoice', $datas);
		$this->load->library('pdf');
		$this->pdf->printInvoices('nativeInvoice', $datas, $datas['inv'][0]['no_invoice']);
		// echo "a";
		// exit;

	}
}

/* End of file  Generator.php */
