<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Partner extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displaayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/userguide3/general/urls.html
     */
    function __construct()
    {
        parent::__construct();

        $this->load->Model('M_partner');
        $this->load->library('Form_validation');
    }
    public function index()
    {
        $group = array('no_pl', 'area');
        $datas = $this->M_partner->init('packing_list', $group)->result();
        // echo "<pre>";
        // var_dump($datas);
        // echo "</pre>";;
        // exit();
        $data['all'] = $datas;
        $this->load->view('partner/dashboard', $data);
    }


    public function print()
    {

        $this->load->view('partner/print');
    }
    public function seacrhPO($pl, $area)
    {
        $where = array(
            'no_pl' => $pl,
            'area' => rawurldecode($area)
        );
        $packlist = $this->M_partner->getPrintsDetail('packing_list', $where)->result();
        // echo "<pre>";
        // var_dump($data);
        // echo "</pre>";
        // ;
        $arr = array();
        $driver = null;
        $vessel = null;
        $phone = null;
        $group = array();
        foreach ($packlist as $key => $value) {
            $penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br><strong>" . $value->plant . "</strong>";
            $driver = $value->driver;
            $vessel = $value->vessel_name;
            $phone = $value->phone;
            $po = $value->no_po;
            $sji = $value->no_sji;
            if (!in_array($value->list_group, $group)) {
                $group[] = $value->list_group;
            }

            if (!isset($po) || $po == '-') {
                $dKey = $sji;
            } else {
                $dKey = $po;
            }
            if (!isset($arr[$dKey])) {
                $arr[$penerima][$dKey][] = $value;
            }
        }
        // echo $driver;
        if (!isset($driver) || $driver == "-") {
            $driver = $vessel;
            $vehile = "VESSEL";
        } else {
            $vehile = "TRUCK";
        }
        // echo $driver;
        $datas['pl_detail'] = array(
            'driver' => $driver,
            'vehicle' => $vehile,
            'phone' => $phone,
            'pl' => $pl,
            'area' => $area,
            'grup' => implode(",", $group)
        );
        // array_push($arr[$penerima][$po], $mat);
        // array_push($arr[$penerima][$po], $jml);
        // var_dump($arr);
        // exit;
        // foreach ($arr as $key => $value) {
        // 	# code...
        // 	foreach ($value as $key2 => $value2) {
        // 		# code...
        // 		echo $key. $value2->material ."<br/>";
        // 	}
        // 	// break;
        // }
        $datas['details'] = $arr;
        $this->load->library('pdf');
        $this->pdf->load_view('partner/printNative', $datas);
        // $data['packlist'] = $arr;
        // $this->load->view('printNative', $datas);
    }
    public function packingList()
    {
        $data = $this->M_partner->init('packing_list')->result();
        echo "<pre>";
        var_dump($data);
        echo "</pre>";;
    }
    public function printNative()
    {
        $this->load->view('partner/printNative');
    }
    public function pdf_gen($nopo, $id)
    {
        if ($nopo[0] == "-") {
            $where = array(
                'id' => $id
            );
        } else {
            $where = array(
                'no_po' => $nopo
            );
        }
        $packlist = $this->M_partner->getDetail('packing_list', $where)->result();
        $datas["details"] = $packlist;
        // $this->load->view('print', $data);
        $this->load->library('pdf');
        $this->pdf->load_view('partner/printNative', $datas);
    }
    public function poDetails()
    {
        $nopo = $this->input->post('no_po');
        $nosji = $this->input->post('id');
        // echo $nopo;
        if ($nopo == "-") {
            $where = array(
                'no_sji' => $nosji
            );
        } else {
            $where = array(
                'no_po' => $nopo
            );
        }
        $packlist = $this->M_partner->getDetail('packing_list', $where)->result();
        $datas["details"] = $packlist;
        $this->load->view('partner/poDetails', $datas);
    }

    public function detail($nopl, $area)
    {
        if ($nopl == "-") {
            $nopl = null;
        }
        $where = array(
            'no_pl' => $nopl,
            'area' => rawurldecode($area)
        );
        $packlist = $this->M_partner->getDetail('packing_list', $where)->result();
        $datas["details"] = $packlist;
        $this->load->view('partner/detail3', $datas);
    }
    public function data_form()
    {
        $this->load->view('partner/add_data');
    }
    public function validateAddData()
    {
        $result = true;
        $this->form_validation->set_rules('no_pl', 'No PL', '|trim');
        $this->form_validation->set_rules('no_po', 'No PO', '|trim');
        $this->form_validation->set_rules('no_sji', 'No SJI', '|trim');
        $this->form_validation->set_rules('date_in', 'Date In', '|trim');
        $this->form_validation->set_rules('date_out', 'Date Out', '|trim');
        $this->form_validation->set_rules('supplier', 'Supplier', '|trim');
        $this->form_validation->set_rules('material', 'Material', '|trim');
        $this->form_validation->set_rules('qty', 'Quantity', '|trim');
        $this->form_validation->set_rules('satuan', 'Satuan', '|trim');
        $this->form_validation->set_rules('panjang', 'Panjang', '|trim');
        $this->form_validation->set_rules('lebar', 'Lebar', '|trim');
        $this->form_validation->set_rules('tinggi', 'Tinggi', '|trim');
        $this->form_validation->set_rules('kubikasi', 'Kubikasi', '|trim');
        $this->form_validation->set_rules('berat', 'Berat', '|trim');
        $this->form_validation->set_rules('area', 'Area', '|trim');
        $this->form_validation->set_rules('corporate', 'Receiver', '|trim');
        $this->form_validation->set_rules('address', 'Address', '|trim');
        $this->form_validation->set_rules('street', 'Street', '|trim');
        $this->form_validation->set_rules('pic', 'PIC', '|trim');
        $this->form_validation->set_rules('plant', 'Plant', '|trim');
        $this->form_validation->set_rules('doc_return', 'Doc Return', '|trim');
        $this->form_validation->set_rules('cost_kubik', 'Cost m3', '|trim');
        $this->form_validation->set_rules('cost_berat', 'Cost Kg', '|trim');
        $this->form_validation->set_rules('tagihan_kubik', 'Bill m3', '|trim');
        $this->form_validation->set_rules('tagihan_berat', 'Bill Kg', '|trim');
        $this->form_validation->set_rules('no_pot', 'No POT', '|trim');
        $this->form_validation->set_rules('remark', 'Remark', '|trim');
        $this->form_validation->set_rules('invoice_date', 'Invoice Date', '|trim');
        $this->form_validation->set_rules('no_invoice', 'Invoice Number', '|trim');
        $this->form_validation->set_rules('list_group', 'Group', '|trim');
        $this->form_validation->set_rules('vessel_name', 'Vessel Name', '|trim');
        $this->form_validation->set_rules('vessel_no_cont', 'Vessel Number', '|trim');
        $this->form_validation->set_rules('vessel_no_segel', 'Vessel Segel', '|trim');
        $this->form_validation->set_rules('input_by', 'Input By', '|trim');
        $this->form_validation->set_rules('no_pl', 'No PL', '|trim');
        if ($this->form_validation->run() == FALSE) {
            $result = false;
        }
        return $result;
    }
    public function loadMaterial()
    {
        $data["nomat"] = $this->input->post('noMat');
        $this->load->view('partner/material', $data);
    }
    public function mappingAddData()
    {
        $datein = strtotime($this->input->post('date_in'));
        $dateout =  strtotime($this->input->post('date_out'));
        $invdate =  strtotime($this->input->post('invoice_date'));
        $docreturn = strtotime($this->input->post('doc_return'));
        setlocale(LC_ALL, 'id_in');
        $sdatein = strftime('%d %B %Y', $datein);
        $sdateout = strftime('%d %B %Y', $dateout);
        $sinvdate = strftime('%d %B %Y', $invdate);
        $sdocreturn = strftime('%d %B %Y', $docreturn);
        // $newformat = date('Y-m-d', $time);
        $data = array(
            'no_pl' => $this->input->post('no_pl'),
            'no_po' => $this->input->post('no_po'),
            'no_sji' => $this->input->post('no_sji'),
            'date_in' => $sdatein,
            'date_out' => $sdateout,
            'supplier' => $this->input->post('supplier'),
            'material' => $this->input->post('material'),
            'qty' => $this->input->post('qtty'),
            'satuan' => $this->input->post('satuan'),
            'panjang' => $this->input->post('panjang'),
            'lebar' => $this->input->post('lebar'),
            'tinggi' => $this->input->post('tinggi'),
            'kubikasi' => $this->input->post('kubikasi'),
            'berat' => $this->input->post('berat'),
            'area' => $this->input->post('area'),
            'corporate' => $this->input->post('corporate'),
            'address' => $this->input->post('address'),
            'street' => $this->input->post('street'),
            'pic' => $this->input->post('pic') . " / " . $this->input->post('Phone'),
            'plant' => $this->input->post('plant'),
            'doc_return' => $sdocreturn,
            'tagihan_kubik' => $this->input->post('tagihan_kubik'),
            'tagihan_berat' => $this->input->post('tagihan_berat'),
            'no_pot' => $this->input->post('nopot'),
            'remark' => $this->input->post('remark'),
            'invoice_date' => $sinvdate,
            'no_invoice' => $this->input->post('no_invoice'),
            'list_group' => $this->input->post('list_group'),
            'input_by' => $this->input->post('input_by'),
            'vessel_no_voyage' => $this->input->post('no_voyage')
        );
        if ($this->input->post('shipping') == "Truck") {
            $data['driver'] = $this->input->post('vessel_name');
            $data['vessel_name'] = NULL;
            $data['phone'] = $this->input->post('driverPhone');
            $data['vessel_no_segel'] = null;
            $data['vessel_no_cont'] = null;
            $data['plate'] = $this->input->post('plate');
        } else {
            $data['vessel_name'] = $this->input->post('vessel_name');
            $data['driver'] = NULL;
            $data['vessel_no_cont'] = $this->input->post('driverPhone');
            $data['phone'] = null;
            $data['plate'] = null;
            $data['vessel_no_segel'] = $this->input->post('plate');
        }
        return $data;
    }
    public function add_data()
    {
        // if($this->validateAddData()){

        // }
        $dataMat = array(
            'material' => $this->input->post('material'),
            'panjang' => $this->input->post('panjang'),
            'lebar' => $this->input->post('lebar'),
            'tinggi' => $this->input->post('tinggi'),
            'kubikasi' => $this->input->post('kubikasi'),
            'qtty' => $this->input->post('qtty'),
            'satuan' => $this->input->post('satuan'),
            'costKubik' => str_replace("Rp. ", "", $this->input->post('cost_kubik')),
            'berat' => $this->input->post('berat'),
            'tagihan_berat' => str_replace("Rp. ", "", $this->input->post('tagihan'))
        );
        $cost = str_replace("Rp. ", "", $this->input->post('cost'));
        for ($i = 0; $i < count($dataMat['material']); $i++) {
            $data['material'] = $dataMat['material'][$i];
            $data['panjang'] = $dataMat['panjang'][$i];
            $data['lebar'] = $dataMat['lebar'][$i];
            $data['tinggi'] = $dataMat['tinggi'][$i];
            $data['kubikasi'] = $dataMat['kubikasi'][$i];
            $data['qty'] = 1;
            $data['satuan'] = $dataMat['satuan'][$i];
            $data['berat'] = $dataMat['berat'][$i];
            $this->M_partner->add_data($data);
        }
        // if ($
        exit;
        // this->form_validation->run() == FALSE) {
        // 	$this->load->view('add_data');
        // } else {
        // 	$material = $this->input->post('material');
        // 	var_dump($material);
        // 	exit;
        // 	$this->M_partner->add_data();
        // 	$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data has been added</div>');
        // 	redirect('dashboard');
        // }
    }
    public function editPO($nopo, $nosji)
    {
        // echo $nopo;
        if ($nopo == "-") {
            $where = array(
                'no_sji' => rawurldecode(str_replace('_', '/', $nosji))
            );
        } else {
            $where = array(
                'no_po' => rawurldecode(str_replace('_', '/', $nopo))
            );
        }
        $packlist = $this->M_partner->getPrintsDetail('packing_list', $where)->result();
        $datas["details"] = $packlist;
        // echo "<pre>";
        // var_dump($datas);
        // echo "</pre>";;
        // exit();
        $this->load->view('partner/edit_data', $datas);
    }
    public function edit()
    {
        $this->form_validation->set_rules('no_pl', 'No PL', '|trim');
        $this->form_validation->set_rules('no_po', 'No PO', '|trim');
        $this->form_validation->set_rules('no_sji', 'No SJI', '|trim');
        $this->form_validation->set_rules('date_in', 'Date In', '|trim');
        $this->form_validation->set_rules('date_out', 'Date Out', '|trim');
        $this->form_validation->set_rules('supplier', 'Supplier', '|trim');
        $this->form_validation->set_rules('material', 'Material', '|trim');
        $this->form_validation->set_rules('qty', 'Quantity', '|trim');
        $this->form_validation->set_rules('satuan', 'Satuan', '|trim');
        $this->form_validation->set_rules('panjang', 'Panjang', '|trim');
        $this->form_validation->set_rules('lebar', 'Lebar', '|trim');
        $this->form_validation->set_rules('tinggi', 'Tinggi', '|trim');
        $this->form_validation->set_rules('kubikasi', 'Kubikasi', '|trim');
        $this->form_validation->set_rules('berat', 'Berat', '|trim');
        $this->form_validation->set_rules('area', 'Area', '|trim');
        $this->form_validation->set_rules('corporate', 'Receiver', '|trim');
        $this->form_validation->set_rules('address', 'Address', '|trim');
        $this->form_validation->set_rules('street', 'Street', '|trim');
        $this->form_validation->set_rules('pic', 'PIC', '|trim');
        $this->form_validation->set_rules('plant', 'Plant', '|trim');
        $this->form_validation->set_rules('doc_return', 'Doc Return', '|trim');
        $this->form_validation->set_rules('cost_kubik', 'Cost m3', '|trim');
        $this->form_validation->set_rules('cost_berat', 'Cost Kg', '|trim');
        $this->form_validation->set_rules('tagihan_kubik', 'Bill m3', '|trim');
        $this->form_validation->set_rules('tagihan_berat', 'Bill Kg', '|trim');
        $this->form_validation->set_rules('no_pot', 'No POT', '|trim');
        $this->form_validation->set_rules('remark', 'Remark', '|trim');
        $this->form_validation->set_rules('invoice_date', 'Invoice Date', '|trim');
        $this->form_validation->set_rules('no_invoice', 'Invoice Number', '|trim');
        $this->form_validation->set_rules('list_group', 'Group', '|trim');
        $this->form_validation->set_rules('vessel_name', 'Vessel Name', '|trim');
        $this->form_validation->set_rules('vessel_no_cont', 'Vessel Number', '|trim');
        $this->form_validation->set_rules('vese_no_segel', 'Vessel Segel', '|trim');
        $this->form_validation->set_rules('input_by', 'Input By', '|trim');



        if ($this->form_validation->run() == FALSE) {
            $id = $this->input->get('id');
            $data['packing_list'] = $this->M_partner->data_detail($id);
            $this->load->view('partner/detail2', $data);
        } else {
            $this->M_partner->edit_data_all()();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data has been Change</div>');
            redirect('partner/detail2');
        }
    }
    public function generateExcel($pl, $area)
    {
        $where = array(
            'no_pl' => $pl,
            'area' => $area
        );
        $packlist = $this->M_partner->getPrintsDetail('packing_list', $where)->result();
        // echo "<pre>";
        // var_dump($data);
        // echo "</pre>";
        // ;
        $arr = array();
        $driver = null;
        $vessel = null;
        $phone = null;
        $dateout = null;
        $group = array();
        foreach ($packlist as $key => $value) {
            $penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br>" . $value->plant;
            $driver = $value->driver;
            $vessel = $value->vessel_name;
            $phone = $value->phone;
            $dateout = $value->date_out;
            $po = $value->no_po;
            $sji = $value->no_sji;
            if (!in_array($value->list_group, $group)) {
                $group[] = $value->list_group;
            }

            if (!isset($po) || $po == '-') {
                $dKey = $sji;
            } else {
                $dKey = $po;
            }
            if (!isset($arr[$dKey])) {
                $arr[$penerima][$dKey][] = $value;
            }
        }
        // echo $driver;
        if (!isset($driver) || $driver == "-") {
            $driver = $vessel;
            $vehile = "VESSEL";
        } else {
            $vehile = "TRUCK";
        }
        // echo $driver;
        $datas['pl_detail'] = array(
            'driver' => $driver,
            'vehicle' => $vehile,
            'phone' => $phone,
            'pl' => $pl,
            'area' => $area,
            'grup' => implode(",", $group)
        );
        // array_push($arr[$penerima][$po], $mat);
        // array_push($arr[$penerima][$po], $jml);
        // var_dump($arr);
        // exit;
        // foreach ($arr as $key => $value) {
        // 	# code...
        // 	foreach ($value as $key2 => $value2) {
        // 		# code...
        // 		echo $key. $value2->material ."<br/>";
        // 	}
        // 	// break;
        // }
        $datas['details'] = $arr;
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("assets/media/reporttemp.xlsx");
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B3', $vehile);
        $sheet->setCellValue('B4', $area);
        $sheet->setCellValue('B5', 'Jakarta, ' . $dateout);
        $sheet->setCellValue('B6', $pl);
        $sheet->setCellValue('A7', $driver . "/" . $phone);
        $sheet->getStyle('A7:T7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->mergeCells("A7:T7");
        $no = 0;
        $nocell = 10;
        $lastcell = 10;
        $lastpenCell = 10;
        $lastpocell = 10;
        foreach ($datas['details'] as $key => $value) {
            $no++;
            $borderStart = $lastpenCell;
            $lastcell = $lastpenCell;
            $sheet->setCellValue('A' . $lastpenCell, $no);
            $receiver = explode("</br>", $key);
            foreach ($receiver as $rec => $valrec) {
                $sheet->setCellValue('B' . $lastpenCell, $valrec);
                $lastpenCell++;
            }
            $tonase = 0;
            $kubikasi = 0.00;

            foreach ($value as $key2 => $value2) {
                $sheet->setCellValue('D' . $lastcell, $key2);
                $tonasecell = $lastcell;
                $nosji = "";
                $inputBy = "";
                $invoice_date = "";
                foreach ($value2 as $key3 => $value3) {
                    $cost = isset($value3->cost_berat) ? $value3->cost_berat : $value3->cost_kubik;
                    $nosji = $value3->no_sji;
                    $inputBy =  $value3->input_by;
                    $sheet->setCellValue('C' . $lastcell, $value3->material);
                    $sheet->setCellValue('F' . $lastcell, $value3->qty);
                    $sheet->setCellValue('G' . $lastcell, $value3->satuan);
                    $sheet->setCellValue('I' . $lastcell, $cost);
                    $sheet->setCellValue('M' . $lastcell, $value3->panjang);
                    $sheet->setCellValue('N' . $lastcell, $value3->lebar);
                    $sheet->setCellValue('O' . $lastcell, $value3->tinggi);
                    $sheet->setCellValue('P' . $lastcell, $value3->kubikasi);
                    $sheet->setCellValue('Q' . $lastcell, $value3->date_in);
                    $sheet->setCellValue('R' . $lastcell, $value3->doc_return);
                    $invoice_date = $value3->invoice_date;

                    $tonase += $value3->berat;
                    $kubikasi += $value3->kubikasi;
                    $lastcell++;
                }
                $compare = (($tonase / 1000) > $kubikasi) ? $tonase / 1000 : $kubikasi;
                $tagihan = $compare * $cost;
                $sheet->setCellValue('E' . $tonasecell, $nosji);
                $sheet->setCellValue('J' . $tonasecell, $tagihan);
                $sheet->setCellValue('K' . $tonasecell, $tonase);
                $sheet->setCellValue('L' . $tonasecell, $kubikasi);
                $sheet->setCellValue('S' . $tonasecell, $inputBy);
                $sheet->setCellValue('T' . $tonasecell, $invoice_date);
            }
            if ($lastcell > $lastpenCell) {
                $lastpenCell = $lastcell;
            }
            $borderEnd = $lastpenCell - 1;
            echo $borderStart . ":" . $borderEnd;
            $spreadsheet
                ->getActiveSheet()
                ->getStyle('A' . $borderStart . ':T' . $borderEnd)
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color('000000'));
        }
        $this->addBorder("A10", "A" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("B10", "B" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("C10", "C" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("D10", "D" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("E10", "E" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("F10", "F" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("G10", "G" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("H10", "H" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("I10", "I" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("J10", "J" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("K10", "K" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("L10", "L" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("M10", "M" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("N10", "N" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("O10", "O" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("P10", "P" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("Q10", "Q" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("R10", "R" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("S10", "S" . ($lastpenCell - 1), $spreadsheet);
        $this->addBorder("T10", "T" . ($lastpenCell - 1), $spreadsheet);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $filename = $pl . "_Details_" . $area . ".xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        ob_end_clean();
        exit($writer->save('php://output'));
    }
    function addBorder($borderStart, $borderEnd, $spreadsheet)
    {
        $spreadsheet
            ->getActiveSheet()
            ->getStyle($borderStart . ':' . $borderEnd)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            ->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color('000000'));
    }


    public function login()
    {

        $this->load->view('Login');
    }

    public function user()
    {

        $this->load->view('partner/user');
    }
}
