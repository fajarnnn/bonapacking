<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Packing extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displaayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('isLogin'))
			redirect(base_url());
		$this->load->Model('M_packing');
		$this->load->Model('M_accounting');
		$this->load->library('Form_validation');
	}
	public function index()
	{
		// $group = array('no_pl', 'area');
		// if ($this->session->userdata('role') == 3) {
		// 	$where = array('list_group' => $this->session->userdata('corporate'));
		// 	$datas = $this->M_packing->init('packing_list', $group, $where)->result();
		// } else {
		// 	$where = array('no_pl' => "-");
		// 	$datas = $this->M_packing->init('packing_list', $group, $where)->result();
		// 	$where = array('no_pl !=' => "-");
		// 	$dt2 = $this->M_packing->init('packing_list', $group, $where)->result();
		// }
		// $data['all'] = $datas;
		// $data['all'] = array_merge($datas, $dt2);

		$this->session->unset_userdata('lastPL');

		if ($this->session->userdata('role') != 3) {
			if ($this->session->userdata('lastPL')) {
				$no_pld = $this->session->userdata('lastPL');
				$where = array('no_pl' => $no_pld, 'date_out' => NULL);
				$datas = $this->M_packing->plDashboard($where)->result();
				$dt2 = array();
			} else {
				$where = array('no_pl' => "-", 'date_out' => NULL);
				$datas = $this->M_packing->plDashboard($where)->result();
				$where = array('no_pl !=' => "-", 'date_out' => NULL);
				$dt2 = $this->M_packing->plDashboard($where)->result();
			}
		} else {
			$where = array('p.list_group' => $this->session->userdata('corporate'), 'no_pl' => "-");
			$datas = $this->M_packing->partnerDashboard($where)->result();
			$where = array('p.list_group' => $this->session->userdata('corporate'), 'no_pl !=' => "-");
			$dt2 = $this->M_packing->partnerDashboard($where)->result();
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$data['all'] = array_merge($datas, $dt2);
		$this->callHead();
		$this->load->view('dashboard', $data);
	}
	function history($pl)
	{
		// var_dump($this->session->userdata());
		// exit;
		if ($this->session->userdata('role') != 3) {

			if ($this->session->userdata('lastPL')) {
				$no_pld = base64url_decode($pl);
				$where = array('no_pl' => $no_pld);
				$datas = $this->M_packing->plDashboard($where)->result();
				$dt2 = array();
			} else {
				$where = array('no_pl' => "-");
				$datas = $this->M_packing->plDashboard($where)->result();
				$where = array('no_pl !=' => "-");
				$dt2 = $this->M_packing->plDashboard($where)->result();
			}
		} else {
			$where = array('p.list_group' => $this->session->userdata('corporate'), 'no_pl' => "-");
			$datas = $this->M_packing->partnerDashboard($where)->result();
			$where = array('p.list_group' => $this->session->userdata('corporate'), 'no_pl !=' => "-");
			$dt2 = $this->M_packing->partnerDashboard($where)->result();
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$data['all'] = array_merge($datas, $dt2);
		$this->callHead();
		$this->load->view('dashboard', $data);
	}
	public function print()
	{

		$this->load->view('print');
	}
	public function seacrhPO($pl, $area)
	{
		$where = array(
			'no_pl' => $pl,
			'area' => rawurldecode($area)
		);
		$packlist = $this->M_packing->getPrintsDetail('packing_list', $where)->result();
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// ;
		$arr = array();
		$driver = null;
		$vessel = null;
		$phone = null;
		$dateOut = null;
		$group = array();
		foreach ($packlist as $key => $value) {
			$penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br><strong>" . $value->plant . "</strong>";
			$driver = $value->driver;
			$vessel = $value->vessel_name;
			$phone = $value->phone;
			$voyage = $value->vessel_no_voyage;
			$po = $value->no_po;
			$sji = $value->no_sji;
			$dateOut = $value->date_out;
			if (!in_array($value->list_group, $group)) {
				$group[] = $value->list_group;
			}

			if (!isset($po) || $po == '-') {
				$dKey = $sji;
			} else {
				$dKey = $po;
			}
			if (isset($sji) && isset($po)) {
				$dKey = $po . '<br/>' . $sji;
			}

			if (!isset($arr[$dKey])) {
				$arr[$penerima][$dKey][] = $value;
			}
		}
		// echo $driver;
		if (!isset($driver) || $driver == "-") {
			$driver = $vessel;
			$phone = $voyage;
			$vehile = "VESSEL";
		} else {
			$vehile = "TRUCK";
		}
		// echo $driver;
		$datas['pl_detail'] = array(
			'driver' => $driver,
			'vehicle' => $vehile,
			'phone' => $phone,
			'pl' => $pl,
			'area' => rawurldecode($area),
			'grup' => implode(",", $group),
			'date_out' => $dateOut
		);
		// array_push($arr[$penerima][$po], $mat);
		// array_push($arr[$penerima][$po], $jml);
		// var_dump($arr);
		// exit;
		// foreach ($arr as $key => $value) {
		// 	# code...
		// 	foreach ($value as $key2 => $value2) {
		// 		# code...
		// 		echo $key. $value2->material ."<br/>";
		// 	}
		// 	// break;
		// }
		$datas['details'] = $arr;
		$this->load->library('pdf');
		// $this->pdf->load_view('printNative', $datas, $pl);
		// $data['packlist'] = $arr;
		$this->load->view('printNative', $datas);
	}
	public function seacrhPOS($pl, $area)
	{
		$where = array(
			'no_pl' => $pl,
			'area' => rawurldecode($area)
		);
		$packlist = $this->M_packing->getPrintsDetail('packing_list', $where)->result();
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// ;
		$arr = array();
		$driver = null;
		$vessel = null;
		$phone = null;
		$dateOut = null;
		$group = array();
		foreach ($packlist as $key => $value) {
			$penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br><strong>" . $value->plant . "</strong>";
			$driver = $value->driver;
			$vessel = $value->vessel_name;
			$phone = $value->phone;
			$voyage = $value->vessel_no_voyage;
			$po = $value->no_po;
			$sji = $value->no_sji;
			$dateOut = $value->date_out;
			if (!in_array($value->list_group, $group)) {
				$group[] = $value->list_group;
			}

			if (!isset($po) || $po == '-') {
				$dKey = $sji;
			} else {
				$dKey = $po;
			}
			if (isset($sji) && isset($po)) {
				$dKey = $po . '<br/>' . $sji;
			}

			if (!isset($arr[$dKey])) {
				$arr[$penerima][$dKey][] = $value;
			}
		}
		// echo $driver;
		if (!isset($driver) || $driver == "-") {
			$driver = $vessel;
			$phone = $voyage;
			$vehile = "VESSEL";
		} else {
			$vehile = "TRUCK";
		}
		// echo $driver;
		$datas['pl_detail'] = array(
			'driver' => $driver,
			'vehicle' => $vehile,
			'phone' => $phone,
			'pl' => $pl,
			'area' => rawurldecode($area),
			'grup' => implode(",", $group),
			'date_out' => $dateOut
		);
		// array_push($arr[$penerima][$po], $mat);
		// array_push($arr[$penerima][$po], $jml);
		// var_dump($arr);
		// exit;
		// foreach ($arr as $key => $value) {
		// 	# code...
		// 	foreach ($value as $key2 => $value2) {
		// 		# code...
		// 		echo $key. $value2->material ."<br/>";
		// 	}
		// 	// break;
		// }
		$datas['details'] = $arr;
		// $this->load->library('pdf');
		// $this->pdf->load_view('printNative', $datas, $pl);
		// $data['packlist'] = $arr;
		$this->load->view('printNative', $datas);
	}
	public function packingList()
	{
		$data = $this->M_packing->init('packing_list')->result();
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// ;
	}
	function deleteimage()
	{
		$id = str_replace("_exist", "", $this->input->post('id'));
		$where['id'] = $id;
		$img = $this->M_packing->getImages($where)->result();
		// echo base_url().$img[0]->path.$img[0]->filename;
		if (unlink("./" . $img[0]->path . $img[0]->filename)) {
			$res = $this->M_packing->deleteImages($where);
			if ($res) {
				echo "success";
			} else {
				echo "fail";
			}
		} else {
			echo "fail";
		}
	}
	public function getDocreturn()
	{
		$data['docret'] = $this->M_packing->getdocreturn()->result();
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// exit();
		$this->callHead();
		$this->load->view('docreturn', $data);
	}
	public function getInvoices()
	{
		$data['docret'] = $this->M_packing->getInvoices()->result();
		$latestpn = $this->M_accounting->getlatestPPN()->row()->lastest;
		$latestnpn = $this->M_accounting->getlatestNonPPN()->row()->lastest;
		$data['lpn'] = isset($latestpn) ? "KW-BONA/" . date("m") . "/" . date("y") . "/" . str_pad(intval($latestpn) + 1, "4", "0", STR_PAD_LEFT) : "KW-BONA/" . date("m") . "/" . date("y") . "/" . "0001";
		$data['lnpn'] = isset($latestnpn) ? "KW-BNRS/" . date("m") . "/" . date("y") . "/" . str_pad(intval($latestnpn) + 1, "4", "0", STR_PAD_LEFT) : "KW-BNRS/" . date("m") . "/" . date("y") . "/" . "0001";
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// exit();
		$this->callHead();
		$this->load->view('accounting', $data);
	}
	public function getCollection()
	{
		$data['docret'] = $this->M_packing->getCollection()->result();
		$this->callHead();
		$this->load->view('collections', $data);
	}
	public function collection($d)
	{
		if ($d == "PPN") {
			$data['docret'] = $this->M_packing->getCollectionPPN()->result();
			$this->callHead();
			$this->load->view('collections', $data);
		} else {
			$data['docret'] = $this->M_packing->getCollectionNonPPN()->result();
			$this->callHead();
			$this->load->view('collections', $data);
		}
	}

	public function getImages()
	{
		$where = array(
			'id_po' => $id = $this->input->post('id')
		);
		$dtimg = $this->M_packing->getImages($where)->result();
		$data['images'] = $dtimg;
		$this->load->view("showImages", $data);
	}
	public function rejectDoc()
	{
		$nopo = $this->input->post('po');
		$id = isset($nopo['id_po']) ? $nopo['id_po'] : $nopo['id'];

		if ($this->M_packing->rejectDoc($id)) {
			echo "success";
		} else {
			echo "failed";
		}
	}
	public function rejectInvoices()
	{
		$nopo = $this->input->post('po');
		$id = $nopo['no_invoice'];

		if ($this->M_packing->rejectInvoice($id)) {
			echo "success";
		} else {
			echo "failed";
		}
	}
	public function rejectPL()
	{
		$nopo = $this->input->post('po');
		$id = isset($nopo['pl_id']) ? $nopo['pl_id'] : "";

		if ($this->M_packing->rejectPL($id)) {
			echo "success";
		} else {
			echo "failed";
		}
	}
	public function updateDocreturn()
	{

		$datapoX['id'] = $this->input->post('id_po');
		$uploadPath = './uploads/images/po/' . $datapoX['id'] . '/';
		if (isset($_FILES))
			$dataImg = $this->do_upload($uploadPath);
		if (isset($dataImg)) {
			if (count($dataImg)) {
				$dataX['upload'] = str_replace("./", "", $uploadPath);
				$dataX['images'] = $dataImg;
				$this->M_packing->saveImages($dataX['images'], $datapoX['id'], $dataX['upload']);
			}
		}

		$id = $this->input->post('id_po');

		$docreturn = strtotime($this->input->post('doc_return'));
		$unloadingDate = strtotime($this->input->post('unloading_date'));
		// setlocale(LC_ALL, 'id_IN');
		setlocale(LC_ALL, 'ID_IN');
		$sdocreturn = $this->checkdate(strftime('%d %B %Y', $docreturn));
		$sunloadingDate = $this->checkdate(strftime('%d %B %Y', $unloadingDate));
		$data['doc_return'] = $sdocreturn;
		$data['unloading_date'] = $sunloadingDate;
		if ($this->M_packing->updateDocReturn($id, $data)) {
			echo 'success';
		} else {
			echo 'failed';
		}
	}
	public function updatePayment()
	{
		// $id = $this->input->post('id');
		$payDate = strtotime($this->input->post('payment_date'));
		$invoiceNumber = $this->input->post('invoice_number');
		$spayDate = $this->checkdate(strftime('%d %B %Y', $payDate));
		$data['payment_date'] = $spayDate;
		// $data['no_invoice'] = $invoiceNumber;
		if ($this->M_packing->updatePayment($invoiceNumber, $data)) {
			echo 'success';
		} else {
			echo 'failed';
		}
	}
	public function updateInvoice()
	{
		$id = $this->input->post('poids');
		$ids = explode(",", $id);
		$invoiceDate = strtotime($this->input->post('invoice_date'));
		$invoiceNumber = $this->input->post('invoice_number');
		$rekeingType = $this->input->post('rekening_type');
		$costType = $this->input->post('cost_type');
		$invoiceName = $this->input->post('nama');
		setlocale(LC_ALL, 'ID_IN');
		$sinvoiceDate = $this->checkdate(strftime('%d %B %Y', $invoiceDate));
		$data['invoice_date'] = $sinvoiceDate;
		$data['no_invoice'] = $invoiceNumber;
		$data['invoice_name'] = $invoiceName;
		$data['rekening_type'] = $rekeingType;
		$data['cost_type'] = $costType;
		if ($this->M_packing->updateInvoice($ids, $data)) {
			echo 'success';
		} else {
			echo 'failed';
		}
	}
	public function do_upload($uploadPath)
	{
		$files = $_FILES;
		$this->load->library('upload');
		$cpt = count($_FILES['userfile']['name']);
		$result = array();
		for ($i = 0; $i < $cpt; $i++) {
			$_FILES['userfile']['name'] = $files['userfile']['name'][$i];
			$_FILES['userfile']['type'] = $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error'] = $files['userfile']['error'][$i];
			$_FILES['userfile']['size'] = $files['userfile']['size'][$i];

			$this->upload->initialize($this->set_upload_options($uploadPath));
			if (!$this->upload->do_upload('userfile')) {
				echo ($this->upload->display_errors());
			} else {
				$data = array('upload_data' => $this->upload->data());
				$dataInfo[] = $this->upload->data();
			}
		}
		if (isset($dataInfo)) {
			return $dataInfo;
		} else {
			return null;
		}
	}
	function callHead($var = NULL)
	{
		$data = [];
		$datas = $this->M_packing->getArchive()->result();
		foreach ($datas as $key => $value) {
			$keys = localIndoToDateFormat($value->date_out, "YYYY");
			$data[$keys][] = $value;
		}
		$dt['data'] = $data;
		if (isset($var)) {
			if (count($data) > 0)
				$vData['arch'] = $data[$var];
			else
				$vData['arch'] = [];
			$this->load->view('header_page', $dt);
			$this->load->view('archive', $vData);
		} else {
			$this->load->view('header_page', $dt);
		}
	}
	function set_upload_options($uploadPath)
	{
		if (!is_dir($uploadPath)) {
			mkdir($uploadPath, 0777, TRUE);
		}
		$config = array();
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|mp4|3gp|mov|mkv|';
		$config['max_size'] = '0';
		$config['overwrite'] = TRUE;

		return $config;
	}
	public function printNative()
	{
		$this->load->view('printNative');
	}
	public function pdf_gen($nopo, $id)
	{
		if ($nopo[0] == "-") {
			$where = array(
				'id' => $id
			);
		} else {
			$where = array(
				'no_po' => $nopo
			);
		}
		$packlist = $this->M_packing->getDetail('packing_list', $where)->result();
		$datas["details"] = $packlist;
		// $this->load->view('print', $data);
		$this->load->library('pdf');
		$this->pdf->load_view('printNative', $datas);
	}
	public function poDetails()
	{
		$nopo = $this->input->post('po');
		$where = array(
			'id_po' => isset($nopo['id_po']) ? $nopo['id_po'] : $nopo['id'],
		);
		$dtimg = $this->M_packing->getImages($where)->result();
		$det = json_decode(json_encode($nopo), FALSE);
		$mat = $this->M_packing->getMaterial($where)->result();
		$where = array("pl.id" => $nopo['pl_id']);
		$ship = $this->M_packing->plDashboard($where)->result();

		$datas["details"] = $det;
		$datas['material'] = $mat;
		$datas['images'] = $dtimg;
		$shipping = array(
			'date_out' => $ship[0]->date_out,
			'isLot' => $ship[0]->isLot,
			'tagihan' => $ship[0]->tagihan,
			'method' => $ship[0]->method,
			'name' => $ship[0]->name,
			'no_shipping' => $ship[0]->no_shipping,
			'no_voyage' => $ship[0]->no_voyage,
			'no_vehicle' => $ship[0]->no_vehicle,
		);
		$datas['shipping'] = $shipping;
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit();
		$this->load->view('poDetails', $datas);
	}

	public function checkPO()
	{
		$po = $this->input->post('noPo');
		$where = array('no_po' => $po);
		$dt = $this->M_packing->getDetail('packing_list', $where);
		if ($dt->num_rows() < 1 || $po == "-" || $po = "" || !isset($po)) {
			echo 'Avail';
		} else {
			echo 'Exist';
		}
	}
	public function detail($nopl)
	{

		// var_dump($nopl);

		// var_dump($this->session->userdata());

		$datas = explode('_BONAN_', base64url_decode($nopl));
		// var_dump($datas);exit;
		$nopl = $datas[0];
		$area = $datas[1];
		// exit;
		// $pls = mb_convert_encoding($nopl, "UTF-8");
		$where = array(
			'no_pl' => $nopl,
			'area' => $area
		);
		$wawa = "LOKALS___" . $nopl;
		// var_dump($wawa);
		// $this->session->unset_userdata('lastPL');
		$this->session->set_userdata('lastPL', $wawa);
		// $this->session->set_userdata($where);
		$q = $this->M_packing->getPLID($where);
		if (!isset($q)) {
			redirect('Packing', 'refresh');
		}
		$id = $q->id;
		$where = array(
			'pl_id' => $id
		);
		if ($this->session->userdata('role') == 3) {
			$where['list_group'] = $this->session->userdata('corporate');
		}

		$packlist = $this->M_packing->getPoList($where);
		$datas["details"] = $packlist;
		$datas["pl"] = $nopl;
		$datas["area"] = $area;
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$this->callHead();
		$this->load->view('detail3', $datas);
	}
	public function data_form()
	{
		$q = $this->db->query('select max(SUBSTRING(no_pl, -4, 4)) as no_pl from pl');
		$dt = $q->row();
		$data['last'] = str_pad(intval($dt->no_pl) + 1, 6, "0", STR_PAD_LEFT);
		$this->callHead();
		$this->load->view('add_data', $data);
	}
	public function validateAddData()
	{
		$result = true;
		$this->form_validation->set_rules('no_pl', 'No PL', '|trim');
		$this->form_validation->set_rules('no_po', 'No PO', '|trim');
		$this->form_validation->set_rules('no_sji', 'No SJI', '|trim');
		$this->form_validation->set_rules('date_in', 'Date In', '|trim');
		$this->form_validation->set_rules('date_out', 'Date Out', '|trim');
		$this->form_validation->set_rules('supplier', 'Supplier', '|trim');
		$this->form_validation->set_rules('material', 'Material', '|trim');
		$this->form_validation->set_rules('qty', 'Quantity', '|trim');
		$this->form_validation->set_rules('satuan', 'Satuan', '|trim');
		$this->form_validation->set_rules('panjang', 'Panjang', '|trim');
		$this->form_validation->set_rules('lebar', 'Lebar', '|trim');
		$this->form_validation->set_rules('tinggi', 'Tinggi', '|trim');
		$this->form_validation->set_rules('kubikasi', 'Kubikasi', '|trim');
		$this->form_validation->set_rules('berat', 'Berat', '|trim');
		$this->form_validation->set_rules('area', 'Area', '|trim');
		$this->form_validation->set_rules('corporate', 'Receiver', '|trim');
		$this->form_validation->set_rules('address', 'Address', '|trim');
		$this->form_validation->set_rules('street', 'Street', '|trim');
		$this->form_validation->set_rules('pic', 'PIC', '|trim');
		$this->form_validation->set_rules('plant', 'Plant', '|trim');
		$this->form_validation->set_rules('doc_return', 'Doc Return', '|trim');
		$this->form_validation->set_rules('cost_kubik', 'Cost m3', '|trim');
		$this->form_validation->set_rules('cost_berat', 'Cost Kg', '|trim');
		$this->form_validation->set_rules('tagihan_kubik', 'Bill m3', '|trim');
		$this->form_validation->set_rules('tagihan_berat', 'Bill Kg', '|trim');
		$this->form_validation->set_rules('no_pot', 'No POT', '|trim');
		$this->form_validation->set_rules('remark', 'Remark', '|trim');
		$this->form_validation->set_rules('invoice_date', 'Invoice Date', '|trim');
		$this->form_validation->set_rules('no_invoice', 'Invoice Number', '|trim');
		$this->form_validation->set_rules('list_group', 'Group', '|trim');
		$this->form_validation->set_rules('vessel_name', 'Vessel Name', '|trim');
		$this->form_validation->set_rules('vessel_no_cont', 'Vessel Number', '|trim');
		$this->form_validation->set_rules('vessel_no_segel', 'Vessel Segel', '|trim');
		$this->form_validation->set_rules('input_by', 'Input By', '|trim');
		$this->form_validation->set_rules('no_pl', 'No PL', '|trim');
		if ($this->form_validation->run() == FALSE) {
			$result = false;
		}
		return $result;
	}
	public function loadMaterial()
	{
		$data["nomat"] = $this->input->post('noMat');
		$this->load->view('material', $data);
	}
	function checkLength($str)
	{
		if (strlen($str) < 1 || !isset($str)) {
			return "-";
		} else {
			return $str;
		}
	}
	function checkdate($strdate)
	{
		if (strpos($strdate, '1970')) {
			return null;
		} else {
			return $strdate;
		}
	}
	function updateSinglePL()
	{
		$idpl = $this->input->post('pl_id');
		$data['pl']['no_pl'] = $this->input->post('noPL');
		$data['pl']['id'] = $idpl;
		$data['pl']['area'] = $this->input->post('area');
		$data['pl']['date_out'] = $this->input->post('date_out');
		$data['pl']['isLot'] = $this->input->post('lot') == 'true' ? true : false;
		$data['pl']['tagihan'] = $this->input->post('tagihan');
		$data['shipping']['method'] = $this->input->post('shipping');
		$data['shipping']['name'] = $this->input->post('shippingName');
		$data['shipping']['no_voyage'] = $this->input->post('noVoyage');
		$data['shipping']['no_vehicle'] = $this->input->post('noKendaraan');
		$data['shipping']['no_shipping'] = $this->input->post('noPengangkut');
		foreach ($data as $key => $value) {
			if ($key == "pl") {
				$this->M_packing->singlePL($key, $value);
			} else if ($key == "shipping") {
				$value['pl_id'] = $idpl;
				$this->M_packing->singlePL($key, $value);
			}
		}
		echo "Success";
	}
	public function bulkPL()
	{
		$idpl = $this->input->post('pl_id');
		if (isset($idpl))
			$data['exist'] = true;

		$dtout = strtotime($this->input->post('date_out'));
		$sdtout = $this->checkdate(strftime('%d %B %Y', $dtout));
		if (strlen($idpl) < 1)
			$idpl = null;
		$data['pl']['no_pl'] = $this->input->post('noPL');
		$data['pl']['pl_id'] = $idpl;
		$data['pl']['area'] = $this->input->post('area');
		$data['pl']['date_out'] = $sdtout;
		$data['pl']['isLot'] = $this->input->post('lot');
		$data['pl']['tagihan'] = NULL;
		$data['po_list']['id'] = $this->input->post('poNums');
		$data['po_list']['poNum'] = $this->input->post('poList');
		$data['shipping']['method'] = $this->input->post('shipping');
		$data['shipping']['name'] = $this->input->post('shippingName');
		$data['shipping']['no_voyage'] = $this->input->post('noVoyage');
		$data['shipping']['no_vehicle'] = $this->input->post('noKendaraan');
		$data['shipping']['no_shipping'] = $this->input->post('noPengangkut');
		// $shipping = $this->input->post('shipping');
		$plid = 0;

		// echo isset($data['pl']['pl_id']) ? "oke" : "nah";
		// exit;
		// $data['exist'] = false;
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";;
		// exit();
		foreach ($data as $key => $value) {
			if ($key == "pl") {
				$plid = $this->M_packing->updateBulk($key, $value);
			} else if ($key == "po_list") {
				$value['pl_id'] = isset($idpl) ? $idpl : $plid;

				$this->M_packing->updateBulk($key, $value);
			} else if ($key == "shipping") {
				if (isset($idpl))
					$value['exist'] = true;
				$value['pl_id'] = isset($idpl) ? $idpl : $plid;
				$this->M_packing->updateBulk($key, $value);
			}
		}
		$this->M_packing->removeEMptyPL();
		echo "Success";
	}
	public function getlastPL($area)
	{
		$area = base64url_decode($area);
		$q = $this->db->query('select max(SUBSTRING(no_pl, -4, 4)) as no_pl from pl');
		if ($q->num_rows() > 0) {

			$dt = $q->row();
			$last = intval($dt->no_pl) + 1;
			echo str_pad($last, 6, "0", STR_PAD_LEFT);
		} else {
			echo "000001";
		}
	}
	public function mappingAddData()
	{
		$unload_date = strtotime($this->input->post('unload_date'));
		$datein = strtotime($this->input->post('date_in'));
		$dateout = strtotime($this->input->post('date_out'));
		$invdate = strtotime($this->input->post('inv_date'));
		$docreturn = strtotime($this->input->post('doc_return'));
		setlocale(LC_ALL, 'id_IN');
		$sdatein = $this->checkdate(strftime('%d %B %Y', $datein));
		$sdateout = $this->checkdate(strftime('%d %B %Y', $dateout));
		$sinvdate = $this->checkdate(strftime('%d %B %Y', $invdate));
		$sdocreturn = $this->checkdate(strftime('%d %B %Y', $docreturn));
		$sunload_date = $this->checkdate(strftime('%d %B %Y', $unload_date));
		// $newformat = date('Y-m-d', $time);

		$data = array(
			'no_pl' => $this->checkLength($this->input->post('no_pl')),
			'no_po' => $this->checkLength($this->input->post('no_po')),
			'no_sji' => $this->checkLength($this->input->post('no_sji')),
			'date_in' => $sdatein,
			'date_out' => $sdateout,
			'supplier' => $this->input->post('supplier'),
			'material' => $this->input->post('material'),
			'qty' => $this->input->post('qtty'),
			'fixbill' => $this->input->post('fixbill'),
			'satuan' => $this->input->post('satuan'),
			'panjang' => $this->input->post('panjang'),
			'lebar' => $this->input->post('lebar'),
			'tinggi' => $this->input->post('tinggi'),
			'kubikasi' => $this->input->post('kubikasi'),
			'berat' => $this->input->post('berat'),
			'area' => $this->input->post('area'),
			'corporate' => $this->input->post('corporate'),
			'address' => $this->input->post('address'),
			'street' => $this->input->post('street'),
			'pic' => $this->input->post('pic') . " / " . $this->input->post('phone'),
			'plant' => $this->input->post('plant'),
			'doc_return' => $sdocreturn,
			'no_pot' => $this->input->post('no_pot'),
			'remark' => $this->input->post('remark'),
			'invoice_date' => $sinvdate,
			'no_invoice' => $this->input->post('inv_no'),
			'list_group' => $this->input->post('list_group'),
			'input_by' => $this->input->post('input_by'),
			'vessel_no_voyage' => $this->input->post('no_voyage'),
			'shipping' => $this->input->post('shipping'),
			'unloading_date' => $sunload_date,
		);
		if ($this->input->post('shipping') == "Truck") {
			$data['driver'] = $this->input->post('vessel_name');
			$data['vessel_name'] = NULL;
			$data['phone'] = $this->input->post('driverPhone');
			$data['vessel_no_segel'] = null;
			$data['vessel_no_cont'] = null;
			$data['plate'] = $this->input->post('plate');
		} else {
			$data['vessel_name'] = $this->input->post('vessel_name');
			$data['driver'] = NULL;
			$data['vessel_no_cont'] = $this->input->post('driverPhone');
			$data['phone'] = null;
			$data['plate'] = null;
			$data['vessel_no_segel'] = $this->input->post('plate');
		}
		$data['tagihan'] = $this->convertCurrency($this->input->post('tagihan'));
		return $data;
	}
	public function mappingPO()
	{
		$docreturn = strtotime($this->input->post('doc_return'));
		$unload_date = strtotime($this->input->post('unload_date'));
		$datein = strtotime($this->input->post('date_in'));
		setlocale(LC_ALL, 'id_IN');
		$sdatein = $this->checkdate(strftime('%d %B %Y', $datein));
		$sunload_date = $this->checkdate(strftime('%d %B %Y', $unload_date));
		$sdocreturn = $this->checkdate(strftime('%d %B %Y', $docreturn));
		$data = array(
			'no_po' => $this->checkLength($this->input->post('no_po')),
			'no_sji' => $this->checkLength($this->input->post('no_sji')),
			'date_in' => $sdatein,
			'doc_return' => $sdocreturn,
			'supplier' => $this->input->post('supplier'),
			'corporate' => $this->input->post('corporate'),
			'address' => $this->input->post('address'),
			'street' => $this->input->post('street'),
			'pic' => $this->input->post('pic') . " / " . $this->input->post('phone'),
			'plant' => $this->input->post('plant'),
			'remark' => $this->input->post('remark'),
			'list_group' => $this->input->post('list_group'),
			'input_by' => $this->session->userdata('nama'),
			'unloading_date' => $sunload_date,
		);
		return $data;
	}
	public function data_add()
	{
		$unload_date = strtotime($this->input->post('unload_date'));
		$dateout = strtotime($this->input->post('date_out'));
		$invdate = strtotime($this->input->post('inv_date'));

		setlocale(LC_ALL, 'id_IN');
		$sdateout = $this->checkdate(strftime('%d %B %Y', $dateout));
		$sinvdate = $this->checkdate(strftime('%d %B %Y', $invdate));

		$sunload_date = $this->checkdate(strftime('%d %B %Y', $unload_date));
		// $newformat = date('Y-m-d', $time);
		$sdateout = $this->checkdate(strftime('%d %B %Y', $dateout));
		$dataMat = array(
			'material' => $this->input->post('material'),
			'panjang' => $this->input->post('panjang'),
			'lebar' => $this->input->post('lebar'),
			'tinggi' => $this->input->post('tinggi'),
			'kubikasi' => $this->input->post('kubikasi'),
			'qtty' => $this->input->post('qty'),
			'satuan' => $this->input->post('satuan'),
			'berat' => $this->input->post('berat'),
			'cost' => $this->input->post('cost'),
			'costbill' => $this->input->post('costbill'),
			'tonase' => $this->input->post('ton'),
			'totalcost' => $this->input->post('totcost'),
			'fixbill' => $this->input->post('fixbill'),
		);
		$datapl = array(
			'no_pl' => $this->checkLength($this->input->post('no_pl')),
			'area' => $this->input->post('area'),
			'date_out' => $sdateout,
			'tagihan' => NULL,
			'isLot' => $this->input->post('isLot') == 'true' ? true : false,
			'created_by' => $this->input->post('input_by'),
		);
		$datashipping = array(
			'method' => $this->input->post('shipping'),
			'name' => $this->input->post('vessel_name'),
			'no_vehicle' => $this->input->post('plate'),
			'no_shipping' => $this->input->post('driverPhone'),
			'no_voyage' => $this->input->post('no_voyage'),
		);
		$dataInv = array(
			'no_invoice' => null,
			'no_pot' => null,
			'invoice_date' => null,
		);
		$datapo = $this->mappingPO();
		$data['pl'] = $datapl;
		$data['shipping'] = $datashipping;
		$data['po_list'] = $datapo;
		$data['invoice'] = $dataInv;
		$data['material'] = $dataMat;
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";;
		// exit();
		$this->M_packing->dataAdd($data);
		$this->M_packing->removeEMptyPL();
		redirect(base_url());
	}
	public function data_edit()
	{
		$datapo = $this->mappingPO();
		$datapo['id'] = $this->input->post('idpo');
		$uploadPath = './uploads/images/po/' . $datapo['id'] . '/';
		$dataImg = $this->do_upload($uploadPath);
		// echo "<pre>";
		// var_dump($dataImg);
		// echo "</pre>";;
		// exit();
		$unload_date = strtotime($this->input->post('unload_date'));
		$dateout = strtotime($this->input->post('date_out'));
		$invdate = strtotime($this->input->post('inv_date'));
		setlocale(LC_ALL, 'id_IN');
		$sdateout = $this->checkdate(strftime('%d %B %Y', $dateout));
		$sinvdate = $this->checkdate(strftime('%d %B %Y', $invdate));
		$sunload_date = $this->checkdate(strftime('%d %B %Y', $unload_date));
		// $newformat = date('Y-m-d', $time);
		$sdateout = $this->checkdate(strftime('%d %B %Y', $dateout));
		$dataMat = array(
			'id' => $this->input->post('ids'),
			'material' => $this->input->post('material'),
			'panjang' => $this->input->post('panjang'),
			'lebar' => $this->input->post('lebar'),
			'tinggi' => $this->input->post('tinggi'),
			'kubikasi' => $this->input->post('kubikasi'),
			'qtty' => $this->input->post('qty'),
			'satuan' => $this->input->post('satuan'),
			'berat' => $this->input->post('berat'),
			'cost' => $this->input->post('cost'),
			'costbill' => $this->input->post('costbill'),
			'tonase' => $this->input->post('ton'),
			'totalcost' => $this->input->post('totcost'),
			'fixbill' => $this->input->post('fixbill'),
		);
		$datapl = array(
			'no_pl' => $this->checkLength($this->input->post('no_pl')),
			'area' => $this->input->post('area'),
			'date_out' => $sdateout,
			'isLot' => $this->input->post('isLot') == 'true' ? true : false,
			'created_by' => $this->input->post('input_by'),
		);
		$datashipping = array(
			'method' => $this->input->post('shipping'),
			'name' => $this->input->post('vessel_name'),
			'no_vehicle' => $this->input->post('plate'),
			'no_shipping' => $this->input->post('driverPhone'),
			'no_voyage' => $this->input->post('no_voyage'),
		);
		$dataInv = array(
			'no_invoice' => $this->input->post('inv_no'),
			'no_pot' => $this->input->post('no_pot'),
			'invoice_date' => $this->input->post('inv_date'),
		);
		if ($datapl['isLot']) {
			$datapl['tagihan'] = $this->convertCurrency($this->input->post('tagihan'));
		}
		$datapo['pl_id'] = $datapl['no_pl'];
		$data['pl'] = $datapl;
		$data['shipping'] = $datashipping;
		$data['po_list'] = $datapo;
		$data['invoice'] = $dataInv;
		$data['material'] = $dataMat;
		$data['last_pl'] = $this->input->post('lastpl');
		if (count($dataImg)) {
			$data['upload'] = str_replace("./", "", $uploadPath);
			$data['images'] = $dataImg;
		}

		$this->M_packing->dataEdit($data);
		$pl = $datapl['no_pl'];
		$area = $datapl['area'];
		$base64 = base64url_encode($pl . "_BONAN_" . $area);
		redirect(base_url("Packing/detail/" . $base64));
		// redirect(base_url());
		// if ($
		// this->form_validation->run() == FALSE) {
		// 	$this->load->view('add_data');
		// } else {
		// 	$material = $this->input->post('material');
		// 	var_dump($material);
		// 	exit;
		// 	$this->M_packing->add_data();
		// 	$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data has been added</div>');
		// 	redirect('dashboard');
		// }
	}
	public function add_data()
	{
		// if($this->validateAddData()){

		// }
		$dataMat = array(
			'material' => $this->input->post('material'),
			'panjang' => $this->input->post('panjang'),
			'lebar' => $this->input->post('lebar'),
			'tinggi' => $this->input->post('tinggi'),
			'kubikasi' => $this->input->post('kubikasi'),
			'qtty' => $this->input->post('qty'),
			'satuan' => $this->input->post('satuan'),
			'berat' => $this->input->post('berat'),
			'cost' => $this->input->post('cost'),
			'costbill' => $this->input->post('costbill'),
			'tonase' => $this->input->post('ton'),
			'totalcost' => $this->input->post('totcost'),
		);
		$data = $this->mappingAddData();
		for ($i = 0; $i < count($dataMat['material']); $i++) {
			$data['material'] = $dataMat['material'][$i];
			$data['panjang'] = $dataMat['panjang'][$i];
			$data['lebar'] = $dataMat['lebar'][$i];
			$data['tinggi'] = $dataMat['tinggi'][$i];
			$data['kubikasi'] = $dataMat['kubikasi'][$i];
			$data['qty'] = $dataMat['qtty'][$i];
			$data['satuan'] = $dataMat['satuan'][$i];
			$data['berat'] = $dataMat['berat'][$i];
			$data['cost'] = $this->convertCurrency($dataMat['cost'][$i]);
			$data['costbill'] = $dataMat['costbill'][$i];
			$data['tonase'] = $dataMat['tonase'][$i];
			$data['totalcost'] = $this->convertCurrency($dataMat['totalcost'][$i]);
			$data['input_by'] = $this->session->userdata('nama');
			$this->M_packing->add_data($data);
		}
		redirect(base_url());
	}
	public function edit_data()
	{
		$this->do_upload();
		// if($this->validateAddData()){

		// }
		$dataMat = array(
			'id' => $this->input->post('ids'),
			'material' => $this->input->post('material'),
			'panjang' => $this->input->post('panjang'),
			'lebar' => $this->input->post('lebar'),
			'tinggi' => $this->input->post('tinggi'),
			'kubikasi' => $this->input->post('kubikasi'),
			'qtty' => $this->input->post('qty'),
			'satuan' => $this->input->post('satuan'),
			'berat' => $this->input->post('berat'),
			'cost' => $this->input->post('cost'),
			'costbill' => $this->input->post('costbill'),
			'tonase' => $this->input->post('ton'),
			'totalcost' => $this->input->post('totcost'),
		);

		$data = $this->mappingAddData();
		for ($i = 0; $i < count($dataMat['material']); $i++) {
			$iddt = isset($dataMat['id'][$i]) ? $dataMat['id'][$i] : null;
			$data['material'] = $dataMat['material'][$i];
			$data['panjang'] = $dataMat['panjang'][$i];
			$data['lebar'] = $dataMat['lebar'][$i];
			$data['tinggi'] = $dataMat['tinggi'][$i];
			$data['kubikasi'] = $dataMat['kubikasi'][$i];
			$data['qty'] = $dataMat['qtty'][$i];
			$data['satuan'] = $dataMat['satuan'][$i];
			$data['berat'] = $dataMat['berat'][$i];
			$data['cost'] = $this->convertCurrency($dataMat['cost'][$i]);
			$data['costbill'] = $dataMat['costbill'][$i];
			$data['tonase'] = $dataMat['tonase'][$i];
			$data['totalcost'] = $this->convertCurrency($dataMat['totalcost'][$i]);
			$data['input_by'] = $this->session->userdata('nama');
			if (isset($iddt))
				$this->M_packing->edit_data_all($data, $iddt);
			else
				$this->M_packing->add_data($data);
		}
		$this->db->query("update packing_list u inner join (
			select no_pl,area,max(driver) as driver, max(vessel_name) as vessel_name, max(phone) as phone, max(plate) as plate, max(vessel_no_cont) as vessel_no_cont, max(vessel_no_segel) as vessel_no_segel, max(vessel_no_voyage) as vessel_no_voyage, max(shipping) as shipping from packing_list GROUP by no_pl, area
			) s ON u.no_pl = s.no_pl AND u.area = s.area 
			set 
			u.shipping = s.shipping,
			u.driver = s.driver,
			u.plate = s.plate,
			u.phone = s.phone,
			u.vessel_name = s.vessel_name,
			u.vessel_no_cont = s.vessel_no_cont,
			u.vessel_no_segel = s.vessel_no_segel,
			u.vessel_no_voyage = s.vessel_no_voyage
			");
		$pl = $data['no_pl'];
		$area = $data['area'];
		$base64 = base64url_encode($pl . "_BONAN_" . $area);
		redirect(base_url("Packing/detail/" . $base64));
	}
	function convertCurrency($curr)
	{
		return preg_replace('/[^0-9]/', '', $curr);
	}
	public function poEdit($podet)
	{
		$datas = base64url_decode($podet);
		$d = json_decode($datas);
		$idpo = $d->id_po;
		$idpl = $d->pl_id;
		$nopo = $this->input->post('po');
		$where = array(
			'id_po' => $idpo,
		);
		$dtimg = $this->M_packing->getImages($where)->result();
		$mat = $this->M_packing->getMaterial($where)->result();
		$where = array("pl.id" => $idpl);
		$ship = $this->M_packing->plDashboard($where)->result();
		$data["details"] = $d;
		$data['material'] = $mat;
		$data['pl'] = $ship;
		$data['images'] = $dtimg;
		$this->callHead();
		$this->load->view('edit_data', $data);
	}
	function checkPL()
	{
		$data['no_pl'] = $this->input->post('no_pl');
		$data['area'] = $this->input->post('area');
		$q = $this->M_packing->checkPLexistant($data);
		$cnt = $q->num_rows();
		if ($cnt > 0) {
			echo json_encode($q->row_array());
		} else {
			echo "Not Exist";
		}
	}
	public function editPO($nopl)
	{
		$datas = explode('_BONAN_', base64url_decode($nopl));
		$nopo = $datas[0];
		$nosji = $datas[1];
		$nopl = $datas[2];
		$area = $datas[3];
		// echo $nopo;
		if ($nopo == "-") {
			$where = array(
				'no_sji' => rawurldecode(str_replace('_', '/', $nosji)),
				'no_pl' => rawurldecode(str_replace('_', '/', $nopl)),
				'area' => rawurldecode(str_replace('_', '/', $area))
			);
		} else {
			$where = array(
				'no_po' => rawurldecode(str_replace('_', '/', $nopo)),
				'no_pl' => rawurldecode(str_replace('_', '/', $nopl)),
				'area' => rawurldecode(str_replace('_', '/', $area))
			);
		}
		$packlist = $this->M_packing->getPrintsDetail('packing_list', $where)->result();
		$datas["details"] = $packlist;

		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";;
		// exit();
		$this->load->view('edit_data', $datas);
	}
	public function deleteMaterial()
	{
		$id = $this->input->post("idmat");
		if ($this->M_packing->delete_material($id)) {
			echo 'success';
		} else {
			echo 'false';
		}
	}
	public function deletePO()
	{
		$poid = $this->input->post('data');
		if ($this->M_packing->poDelete($poid)) {
			$this->M_packing->removeEMptyPL();

			echo 'success';
		} else {
			echo 'failed';
		}
	}
	public function generateExcel($pl, $ara)
	{
		$area = rawurldecode($ara);
		$where = array(
			'no_pl' => $pl,
			'area' => $area
		);
		$packlist = $this->M_packing->genPdf($where)->result();
		// echo "<pre>";
		// var_dump($packlist);
		// echo "</pre>";
		// exit();
		$group = array();
		foreach ($packlist as $key => $value) {
			$penerima = $value->corporate . "</br>" . $value->address . "</br>" . $value->street . "</br>" . $value->pic . "</br>" . $value->plant;
			$name = $value->name;
			$noship = $value->no_shipping;
			$noveh = $value->no_vehicle;
			$voyage = $value->no_voyage;
			$po = $value->no_po;
			$sji = $value->no_sji;
			$dateOut = $value->date_out;
			if (!in_array($value->list_group, $group)) {
				$group[] = $value->list_group;
			}

			if (!isset($po) || $po == '-') {
				$dKey = $sji;
			} else {
				$dKey = $po;
			}

			if (!isset($arr[$dKey])) {
				$arr[$penerima][$dKey][] = $value;
			}
		}
		// echo $driver;
		$datas['pl_detail'] = array(
			'driver' => $name,
			'vehicle' => $noveh,
			'phone' => $noship,
			'pl' => $pl,
			'area' => $area,
			'grup' => implode(",", $group),
			'date_out' => $dateOut
		);
		// array_push($arr[$penerima][$po], $mat);
		// array_push($arr[$penerima][$po], $jml);
		// var_dump($arr);
		// exit;
		// foreach ($arr as $key => $value) {
		// 	# code...
		// 	foreach ($value as $key2 => $value2) {
		// 		# code...
		// 		echo $key. $value2->material ."<br/>";
		// 	}
		// 	// break;
		// }
		$datas['details'] = $arr;
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("assets/media/reporttemp.xlsx");
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('B3', $noveh);
		$sheet->setCellValue('B4', $area);
		$sheet->setCellValue('B5', 'Jakarta, ' . $dateOut);
		$sheet->setCellValue('B6', $pl);
		$sheet->setCellValue('A7', $name . "/" . $noship);
		$sheet->getStyle('A7:T7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->mergeCells("A7:T7");
		$no = 0;
		$nocell = 10;
		$lastcell = 10;
		$lastpenCell = 10;
		$lastpocell = 10;
		$totalqty = 0;
		$totalTagihan = 0;
		$totalCost = 0;
		$totalTonase = 0;
		$totalKubik = 0.00;
		foreach ($datas['details'] as $key => $value) {
			$no++;
			$borderStart = $lastpenCell;
			$lastcell = $lastpenCell;
			$sheet->setCellValue('A' . $lastpenCell, $no);
			$receiver = explode("</br>", $key);
			foreach ($receiver as $rec => $valrec) {
				$sheet->setCellValue('B' . $lastpenCell, $valrec);
				$lastpenCell++;
			}
			$tonase = 0;
			$kubikasi = 0.00;

			foreach ($value as $key2 => $value2) {
				$sheet->setCellValue('D' . $lastcell, $key2);
				$tonasecell = $lastcell;
				$nosji = "";
				$inputBy = "";
				$invoice_date = "";
				$tagihan = 0;
				foreach ($value2 as $key3 => $value3) {
					$costbill = $value3->costbill;
					$totalqty += $value3->qty;
					$costend = $costbill == 1 ? " /Ton" : " /Kg";
					$cost = $value3->cost;
					$tagihan += $value3->totalcost;
					$nosji = $value3->no_sji;
					$inputBy = $value3->input_by;
					$sheet->setCellValue('C' . $lastcell, $value3->material);
					$sheet->setCellValue('F' . $lastcell, $value3->qty);
					$sheet->setCellValue('G' . $lastcell, $value3->satuan);
					$sheet->setCellValue('I' . $lastcell, $cost);
					$sheet->setCellValue('M' . $lastcell, $value3->panjang);
					$sheet->setCellValue('N' . $lastcell, $value3->lebar);
					$sheet->setCellValue('O' . $lastcell, $value3->tinggi);
					$sheet->setCellValue('P' . $lastcell, $value3->kubikasi);
					$sheet->setCellValue('Q' . $lastcell, $value3->date_in);
					$sheet->setCellValue('R' . $lastcell, $value3->doc_return);
					$invoice_date = $value3->invoice_date;

					$tonase += $value3->berat * $value3->qty;
					if (is_numeric($value3->kubikasi))
						$kubikasi += $value3->kubikasi;
					$lastcell++;
				}

				$totalTonase += $tonase;
				$totalKubik += $kubikasi;
				$totalTagihan += $tagihan;
				$sheet->setCellValue('E' . $tonasecell, $nosji);
				$sheet->setCellValue('J' . $tonasecell, $tagihan);
				$sheet->setCellValue('K' . $tonasecell, $tonase);
				$sheet->setCellValue('L' . $tonasecell, $kubikasi);
				$sheet->setCellValue('S' . $tonasecell, $inputBy);
				$sheet->setCellValue('T' . $tonasecell, $invoice_date);
				$tonase = 0;
				$kubikasi = 0.00;
			}
			if ($lastcell > $lastpenCell) {
				$lastpenCell = $lastcell;
			}
			$borderEnd = $lastpenCell - 1;
			echo $borderStart . ":" . $borderEnd;
			$spreadsheet
				->getActiveSheet()
				->getStyle('A' . $borderStart . ':T' . $borderEnd)
				->getBorders()
				->getOutline()
				->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
				->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color('000000'));
		}
		$sheet->getStyle('A' . $lastpenCell . ':E' . $lastpenCell)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->mergeCells('A' . $lastpenCell . ':E' . $lastpenCell);
		$sheet->setCellValue('A' . $lastpenCell, "TOTAL");
		$sheet->setCellValue('F' . $lastpenCell, $totalqty);
		$sheet->setCellValue('G' . $lastpenCell, "COLLY");
		$sheet->setCellValue('J' . $lastpenCell, $totalTagihan);
		$sheet->setCellValue('K' . $lastpenCell, $totalTonase);
		$sheet->setCellValue('L' . $lastpenCell, $totalKubik);
		$sheet->getStyle('A' . $lastpenCell . ':T' . $lastpenCell)->getFont()->setBold(true);
		$this->addBorder("A10", "A" . ($lastpenCell), $spreadsheet);
		$this->addBorder("B10", "B" . ($lastpenCell), $spreadsheet);
		$this->addBorder("C10", "C" . ($lastpenCell), $spreadsheet);
		$this->addBorder("D10", "D" . ($lastpenCell), $spreadsheet);
		$this->addBorder("E10", "E" . ($lastpenCell), $spreadsheet);
		$this->addBorder("F10", "F" . ($lastpenCell), $spreadsheet);
		$this->addBorder("G10", "G" . ($lastpenCell), $spreadsheet);
		$this->addBorder("H10", "H" . ($lastpenCell), $spreadsheet);
		$this->addBorder("I10", "I" . ($lastpenCell), $spreadsheet);
		$this->addBorder("J10", "J" . ($lastpenCell), $spreadsheet);
		$this->addBorder("K10", "K" . ($lastpenCell), $spreadsheet);
		$this->addBorder("L10", "L" . ($lastpenCell), $spreadsheet);
		$this->addBorder("M10", "M" . ($lastpenCell), $spreadsheet);
		$this->addBorder("N10", "N" . ($lastpenCell), $spreadsheet);
		$this->addBorder("O10", "O" . ($lastpenCell), $spreadsheet);
		$this->addBorder("P10", "P" . ($lastpenCell), $spreadsheet);
		$this->addBorder("Q10", "Q" . ($lastpenCell), $spreadsheet);
		$this->addBorder("R10", "R" . ($lastpenCell), $spreadsheet);
		$this->addBorder("S10", "S" . ($lastpenCell), $spreadsheet);
		$this->addBorder("T10", "T" . ($lastpenCell), $spreadsheet);

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$filename = $pl . "_Details_" . $area . ".xlsx";
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		ob_end_clean();
		exit($writer->save('php://output'));
	}
	function addBorder($borderStart, $borderEnd, $spreadsheet)
	{
		$spreadsheet
			->getActiveSheet()
			->getStyle($borderStart . ':' . $borderEnd)
			->getBorders()
			->getOutline()
			->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
			->setColor(new \PhpOffice\PhpSpreadsheet\Style\Color('000000'));
	}


	public function login()
	{

		$this->load->view('Login');
	}

	public function dashboard_admin()
	{

		$this->load->view('admin/dashboard_admin');
	}


	public function admin()
	{

		$this->load->view('dashboard_admin');
	}
	public function docreturn()
	{
		if ($this->session->userdata('role') != 3) {
			$datas = $this->M_packing->plDashboard()->result();
		} else {
			$where = array('p.list_group' => $this->session->userdata('corporate'));
			$datas = $this->M_packing->partnerDashboard($where)->result();
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$data['all'] = $datas;

		$this->load->view('docreturn');
	}
	public function packingLists()
	{
		$this->session->unset_userdata('lastPL');

		$where = array('no_pl' => "-", 'date_out !=' => NULL);
		$datas = $this->M_packing->plDashboard($where)->result();
		$where = array('no_pl !=' => "-", 'date_out !=' => NULL);
		$dt2 = $this->M_packing->plDashboard($where)->result();
		$data['all'] = array_merge($datas, $dt2);
		$this->callHead();
		$this->load->view('packingList', $data);
	}
	public function sjn()
	{
		if ($this->session->userdata('role') != 3) {
			$where = array('no_pl' => "-");
			$datas = $this->M_packing->getSJN($where);
			// $where = array('no_pl !=' => "-");
			// $dt2 = $this->M_packing->getSJN($where);
		} else {
			$where = array('list_group' => $this->session->userdata('corporate'), 'no_pl' => "-");
			$datas = $this->M_packing->getSJNGroup($where);
			$where = array('list_group' => $this->session->userdata('corporate'), 'no_pl !=' => "-");
			$dt2 = $this->M_packing->getSJNGroup($where);
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$data['all'] = $datas;
		$this->callHead();
		$this->load->view('sjn', $data);
	}
	public function printSjn()
	{
		if ($this->session->userdata('role') != 3) {
			$datas = $this->M_packing->plDashboard()->result();
		} else {
			$where = array('p.list_group' => $this->session->userdata('corporate'));
			$datas = $this->M_packing->partnerDashboard($where)->result();
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$data['all'] = $datas;

		$this->load->view('printSjn');
	}
	public function archive()
	{
		if ($this->session->userdata('role') != 3) {
			$where = array('no_pl' => "-");
			$datas = $this->M_packing->getSJN($where);
			$where = array('no_pl !=' => "-");
			$dt2 = $this->M_packing->getSJN($where);
		} else {
			$where = array('list_group' => $this->session->userdata('corporate'), 'no_pl' => "-");
			$datas = $this->M_packing->getSJNGroup($where);
			$where = array('list_group' => $this->session->userdata('corporate'), 'no_pl !=' => "-");
			$dt2 = $this->M_packing->getSJNGroup($where);
		}
		// echo "<pre>";
		// var_dump($datas);
		// echo "</pre>";
		// exit;
		$this->load->view('header_page');
		$data['all'] = array_merge($datas, $dt2);

		$this->load->view('archive');
	}
	public function printinvoice()
	{
		echo "aw";
		$poid = explode(',', $this->input->post('poids'));

		$data['invoice_name'] = $this->input->post('nama');
		$data['invoice_date'] = $this->input->post('invoice_date');
		$data['no_invoice'] = $this->input->post('invoice_number');
		$data['rekening_type'] = $this->input->post('rekening_type');
		$data['cost_type'] = $this->input->post('cost_type');
		$data['no_pot'] = NULL;
		$data['payment_date'] = NULL;

		// foreach ($poid as $key => $value) {
		// $data['id_po'] = $po;
		$this->M_packing->addInvoice($data, $poid);
		// }
		redirect(base_url("Packing/printInv/" . base64url_encode($data['no_invoice'])));
		// exit;

		// exit;

		// $this->load->view('nativeinvoice', $datas);
	}
	function printInv($invNums)
	{
		$invNum = base64url_decode($invNums);
		$datas['inv'] = $this->M_packing->printInvoice($invNum)->result_array();

		$isPO = true;
		$isSjn = true;
		$totalHarga = 0;
		$hargaBefore = 0;
		$ppn = 0;
		$qty = 0;
		$dt = [];
		$materialStr = "";
		$no = 0;
		foreach ($datas['inv'] as $keys => $values) {
			$no++;
			foreach ($values as $key => $value) {
				# code...
				if ($key == "no_po")
					$dt['no_po'][$value] = 0;
				if ($key == 'no_sji')
					$dt['no_sji'][$value] = 0;
				if ($key == 'satuan')
					$dt['satuan'][$value] = 0;
				if ($key == 'area')
					$dt['area'][$value] = 0;

			}
			$plus = " + ";
			if ($no < count($datas['inv']))
				$materialStr .= $values['material'] . $plus;
			else
				$materialStr .= $values['material'];
			$qty += $values['qty'];
			$hargaBefore += $values['totalcost'];
		}

		$dt['qty'] = $qty;
		$dt['materialStr'] = $materialStr;
		if (count($dt['no_po']) > 1) {
			$dt['no_po'] = false;
		}
		if (count($dt['no_sji']) > 1) {
			$dt['no_sji'] = false;
		}
		if (count($dt['satuan']) > 1) {
			$dt['satuan'] = false;
			$dt['qty'] = 1;
		}
		if (count($dt['area']) > 1) {
			$dt['area'] = false;
		}
		if (strlen($materialStr) > 200) {
			$dt['materialStr'] = "Data Material Akan Dilampirkan";
		}
		$totalHarga = $hargaBefore;
		if ($datas['inv'][0]['cost_type'] == "Belum PPN") {
			$ppn = round($totalHarga * 1.1 / 100);
			$totalHarga = $totalHarga + $ppn;
		} else {
			$ppn = round($totalHarga * 1.1 / 100);
			$hargaBefore = $totalHarga - $ppn;
		}
		$dt['hargaBefore'] = number_format($hargaBefore, 0, ',', '.');
		$dt['total_harga'] = number_format($totalHarga, 0, ',', '.');
		$dt['ppn'] = number_format($ppn, 0, ',', '.');
		$dt['terbilang'] = angkaKeTeks($totalHarga) . "rupiah";
		$datas['addon'] = $dt;
		// $this->load->view('nativeInvoice', $datas);
		echo "<pre>";
		var_dump($datas);
		echo "</pre>";
		exit;
		$this->load->library('pdf');
		$this->pdf->printInvoices('nativeInvoice', $datas, $datas['inv'][0]['no_invoice']);
	}
}
