<?php
class Logout extends CI_Controller
{
    function __construct()
    {

        parent::__construct();
		$this->load->model('m_user');
		if(!$this->session->userdata('isLogin'))
			redirect(base_url());
    }
	function index()
    {
		if($this->m_user->updateLogin($this->session->userdata('id'), false))
       		$this->session->sess_destroy();
		
        redirect(base_url());

    }
}

