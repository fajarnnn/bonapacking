<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/userguide3/general/urls.html
     */
    function __construct()
    {
        parent::__construct();
        $this->load->Model('M_orders');
    }

    public function index()
    {
        $this->load->view('orders');
    }
    public function order()
    {
        $dt = $this->input->post();
        // print_r($_FILES);
        if (array_key_exists('name',$dt)) {
            $phone = $dt['hp'];
            $name = $dt['name'];
            $address = $dt['address'];
            $jasa = $dt['jasa'];
            // if($_FILES['prove'])
            $ordersPath = "";
            
            if($_FILES['order']['error']['0'] != 4){
                $j = 0;
                $FILESORDERS = $this->splitImage('order');
                foreach ($FILESORDERS as $key => $value) {
                    $j++;
                    $_FILES['image'] = $value;
                    $uploadFile_order = $this->uploadImage($phone, 'image');
                    if(!array_key_exists('file_name', $uploadFile_order)){
                        print_r('error uploading file');
                    }else{
                        if($j == 1){
                            $ordersPath .= $uploadFile_order['file_name'];
                        }else{
                            $ordersPath .= "|".$uploadFile_order['file_name'];
                        }
                    }
                }
                
            }
            $FILESPROVE = $this->splitImage('prove');
            $imagePath = "";
            $i = 0;
            foreach ($FILESPROVE as $key => $value) {
                $i++;
                $_FILES['image'] = $value;
                $uploadFile = $this->uploadImage($phone, 'image');
                if (array_key_exists('file_name', $uploadFile)) {
                    if($i == 1){
                        $imagePath .= $uploadFile['file_name'];
                    }else{
                        $imagePath .= "|".$uploadFile['file_name'];
                    }
                }
            }
            if (array_key_exists('file_name', $uploadFile)) {
                // $imagePath = $uploadFile['full_path'];
                $data = array(
                    'nama' => $name,
                    'alamat' => $address,
                    'hp' => str_replace("-", "", $phone),
                    'pesanan' => $ordersPath,
                    'bukti' => $imagePath,
                    'jasa' => $jasa,
                    'tanggal' => date('Y-m-d H:i:s')
                );
                if($this->M_orders->saveOrder($data)){
                    $this->load->view('orderfailed');
                }else{
                    $this->load->view('orderconfirmed');
                }
            }
        }
    }
    public function splitImage($field){
        foreach ($_FILES[$field] as $key => $value) {
            foreach ($value as $keys => $values) {
                $FILES[$keys][$key] = $values;
            }
        }
        return $FILES;
    }
    public function uploadImage($phone, $path)
    {
        $configUpload['upload_path'] = 'uploads/'; #the folder placed in the root of project
        $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg'; #allowed types description
        $configUpload['max_size'] = '0'; #max size
        $configUpload['max_width'] = '0'; #max width
        $configUpload['max_height'] = '0'; #max height
        $configUpload['encrypt_name'] = true; #encrypt name of the uploaded file
        $this->load->library('upload', $configUpload); #init the upload class
        if (!$this->upload->do_upload($path)) {
            $uploadedDetails = $this->upload->display_errors();
        } else {
            $uploadedDetails = $this->upload->data();
        }
        // print_r($uploadedDetails);
        return $uploadedDetails;
    }
}