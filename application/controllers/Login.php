<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct()
	{

		parent::__construct();
		$this->load->model('m_user');
		if ($this->session->userdata('isLogin'))
			redirect('packing');
	}

	public function index()
	{
		$this->load->view('login');
		// echo md5(md5(md5(md5("admin"))));
	}
	public function doLogin()
	{


		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
		);
		$q = $this->m_user->login("users", $where);
		$cek = $q->num_rows();
		$isLogin = isset($q->row()->isLogin) ? $q->row()->isLogin : false;
		$role = isset($q->row()->role_id) ? $q->row()->role_id : null;
		$nama = isset($q->row()->nama) ? $q->row()->nama : null;
		$corp = isset($q->row()->corporate) ? $q->row()->corporate : null;
		// var_dump($this->session->userdata('isLogin'));
		// var_dump($isLogin);
		// exit;
		if ($isLogin && $this->session->userdata('isLogin')) {
			$this->session->set_flashdata('msg', 'User Sudah Login di perangkat lain!');
			$this->load->view('login');
		} else
		if ($cek > 0) {
			$id = $q->row()->id;
			$data_session = array(
				'id' => $id,
				'username' => $username,
				'nama' => $nama,
				'isLogin' => true,
				'role' => $role,
				'corporate'=> $corp
			);
			$this->m_user->updateLogin($id, true);
			$this->session->set_userdata($data_session);
			redirect(base_url("packing"));
		} else {
			// echo "Username dan password salah !";
			$this->session->set_flashdata('msg', 'Username atau Password Salah!');
			$this->load->view('login');
		}
	}

}

/* End of file  Login.php */
