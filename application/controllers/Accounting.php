<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Accounting extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('isLogin'))
			redirect(base_url());
		// $this->load->Model('M_packing');
		$this->load->Model('M_accounting');
		$this->load->library('Form_validation');
	}
	public function index()
	{

	}
	public function printinvoice()
	{
		$poid = explode(',', $this->input->post('poids'));

		$data['invoice_name'] = $this->input->post('nama');
		$data['invoice_date'] = $this->input->post('invoice_date');
		$data['no_invoice'] = $this->input->post('invoice_number');
		$data['rekening_type'] = $this->input->post('rekening_type');
		$data['cost_type'] = $this->input->post('cost_type');
		$data['discount'] = $this->input->post('diskon');
		$data['down_payment'] = $this->convertCurrency($this->input->post('dp'));
		$data['additional_cost'] = $this->convertCurrency($this->input->post('nominallain'));
		$data['ppn'] = $this->convertCurrency($this->input->post('jppn'));
		$data['in_kubikasi'] = $this->input->post('pemKub') !== "" ? $this->input->post('pemKub') : "0";
		$data['tujuan'] = $this->input->post('tujuan');
		$data['no_pot'] = NULL;
		$data['payment_date'] = NULL;

		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// exit();	

		if ($this->M_accounting->addInvoice($data, $poid)) {
			if ($data['in_kubikasi'] != "0") {
				$where = array(
					'no_invoice' => $data['no_invoice']
				);

				$packlist = $this->M_accounting->genPdf($where)->result();
				$inkb = $packlist[(count($packlist) - 1)]->in_kubikasi;
				$arr = json_decode(json_encode($packlist), true);
				$aw = array_search(1, array_column($arr, 'changed'));
				if ($inkb != "0") {
					$end = $packlist[(count($packlist) - 1)];
					$sum = array_reduce($packlist, function ($carry, $item) {
						return $carry + $item->kubikasi;
					});
					$inkb = $packlist[(count($packlist) - 1)]->in_kubikasi;
					$diff = $inkb - $sum;
					$chgkub = floatval($packlist[(count($packlist) - 1)]->kubikasi) + $diff;
					$packlist[(count($packlist) - 1)]->kubikasi = $chgkub;
					$end = $packlist[(count($packlist) - 1)];
					$packlist[(count($packlist) - 1)]->totalcost = floatval($end->kubikasi) * floatval($end->cost);
					$end = $packlist[(count($packlist) - 1)];
					$idmat = $end->idmat;
					$matdata['totalcost'] = $end->totalcost;
					$matdata['kubikasi'] = $end->kubikasi;
					$matdata['changed'] = 1;
					// echo "<pre>";
					// var_dump($matdata);
					// echo "</pre>";
					// exit();	
					$this->M_accounting->updMat($matdata, $idmat);
				}
			}
		}
		redirect(base_url("FileGenerator/printInv/" . base64url_encode($data['no_invoice'])));
	}
	public function getPPN()
	{
		$poid = explode(',', $this->input->post('poids'));
		$res = $this->M_accounting->getTotalHarga($poid);
		$tot = isset($res->result()[0]->tot) ? $res->result()[0]->tot : 0;
		$kub = isset($res->result()[0]->kub) ? $res->result()[0]->kub : 0;
		$data['total_harga'] = $tot;
		$data['kub'] = $kub;
		// echo $tot;exit;
		$ppn = round(intval($tot) * 1.1 / 100);
		$data['PPN'] = $ppn;
		echo json_encode($data);

	}
	function printInv($a)
	{
		$invNum = base64url_decode($a);
		// echo $invNum;
		// exit;
		$datas['inv'] = $this->M_accounting->printInvoice($invNum)->result_array();


		// exit;
		$isPO = true;
		$isSjn = true;
		$totalHarga = 0;
		$totalBerat = 0;
		$totalkubik = 0;
		$hargaBefore = 0;
		$total_cost = 0;
		$cost = 0;
		$ppn = 0;
		$qty = 0;
		$dt = [];
		$materialStr = "";
		$no = 0;
		$discountNom = 0;
		$addCost = 0;
		$discount = 0;
		$downPayment = 0;
		$subcost = 0;
		$matdata = [];
		foreach ($datas['inv'] as $keys => $values) {
			$no++;
			foreach ($values as $key => $value) {
				# code...
				if ($key == "no_po")
					$dt['no_po'][$value] = 0;
				if ($key == 'no_sji')
					$dt['no_sji'][$value] = 0;
				if ($key == 'satuan')
					$dt['satuan'][$value] = 0;
				if ($key == 'area')
					$dt['area'][$value] = 0;

			}
			$plus = " + ";
			if ($no < count($datas['inv']))
				$materialStr .= $values['material'] . $plus;
			else
				$materialStr .= $values['material'];
			$qty += $values['qty'];
			$totalBerat += $values['berat'] * $values['qty'];
			$totalkubik += $values['kubikasi'];
			$hargaBefore += $values['totalcost'];
			$addCost = $values['additional_cost'];
			$discount = $values['discount'];
			$downPayment = $values['down_payment'];
			$cost = $values['cost'];
			$ppnDB = $values['ppn'];
			if ($values['costbill'] > 0) {
				$sumton = $values['cost'] * ($values['tonase'] / 1000);
				$sumkub = round($values['cost'] * ($values['kubikasi']));
				// echo $sumkub."-".$values['totalcost'];
				if ($sumkub == $values['totalcost']) {
					$matdata['cb']['3'] = 0;
				} else if ($sumton == $values['totalcost'] && $values['tonase'] != null) {
					$matdata['cb']['1'] = 0;
				} else {
					$matdata['cb'][$values['costbill']] = 0;
				}
				// $matdata['cb'][$values['cost']] = 0;
			} else {
				// echo ($values['cost'] * ($values['tonase']/1000));
				$berat = is_numeric($values['tonase']) ? floatval($values['tonase']) : 0;
				$kubik = is_numeric($values['kubikasi']) ? floatval($values['kubikasi']) : 0;
				$ton = $berat / 1000;

				if ($ton > $kubik) {
					$matdata['cb']['1'] = 0;
				} else {
					$matdata['cb']['3'] = 0;
				}
				$matdata['cost'][$values['cost']] = 0;
			}

		}
		if (count($matdata['cb']) == 1) {
			foreach ($matdata['cb'] as $key => $value) {
				if ($key == 1) {
					$dt['qty'] = $totalBerat . " kg";
				} else if ($key == 3) {
					$dt['qty'] = $totalkubik . " m3";
				} else if ($key == 2) {
					$dt['qty'] = $totalBerat . " ton";
				} else if ($key == 4) {
					$dt['qty'] = $qty . " Unit";
				} else if ($key == 5) {
					$dt['qty'] = $qty . " Lot";
				}
			}
		} else {
			$cost = 0;
			$dt['qty'] = 1;
		}
		$dt['materialStr'] = $materialStr;
		if (count($dt['no_po']) > 1) {
			$dt['no_po'] = false;
		}
		if (count($dt['no_sji']) > 1) {
			$dt['no_sji'] = false;
		}
		if (count($dt['satuan']) > 1) {
			$dt['satuan'] = false;
			$dt['qty'] = 1;
		}
		if (count($dt['area']) > 1) {
			$dt['area'] = false;
		}
		if (strlen($materialStr) > 200) {
			$dt['materialStr'] = "Data Material Akan Dilampirkan";
		}
		$totalHarga = $hargaBefore;
		// echo "<br>HARGA AWAL:".$totalHarga;
		if (isset($discount)) {
			// echo "<br>DISCOUNT:".$discount;
			if (strpos($discount, "%") > 0) {
				$totdis = $totalHarga * (intval(str_replace("%", "", $discount)) / 100);
				$totalHarga -= $totdis;
				$discountNom = $totdis;
			} else if ($discount > 0) {
				$totalHarga -= $discount;
				$discountNom = $discount;
			}
		}
		// echo "<br>AFTER DISCOUNT:".$totalHarga;
		if ($datas['inv'][0]['cost_type'] == "Belum PPN") {
			$ppn = round($totalHarga * 1.1 / 100);
			if (isset($ppnDB)) {
				if ($ppnDB > 0)
					$ppn = $ppnDB;
			}
			$totalHarga = $totalHarga + $ppn;
		} else {
			$ppn = round($totalHarga * 1.1 / 100);
			if (isset($ppnDB)) {
				if ($ppnDB > 0)
					$ppn = $ppnDB;
			}
			$hargaBefore = $totalHarga - $ppn;
		}
		if (isset($ppnDB)) {
			// echo "<br>PPN DB:".$ppn;
		}
		// echo "<br>AFTER PPN:".$totalHarga;
		if (isset($downPayment)) {
			// echo "<br>DP:".$downPayment;
			if ($downPayment > 0) {
				$totalHarga -= $downPayment;
			}
		}
		// echo "<br>AFTER DB:".$totalHarga;
		if (isset($addCost)) {
			// echo "<br>BIAYA LAIN:".$addCost;
			if ($addCost > 0) {
				$totalHarga += $addCost;
			}
		}
		// echo "<br>AFTER BIAYA LAIN:".$totalHarga;
		// exit;
		$dt['cost'] = number_format($cost, 0, ',', '.');
		// $dt['cost'] = $total_cost;
		$dt['hargaBefore'] = number_format($hargaBefore, 0, ',', '.');
		$dt['total_harga'] = number_format($totalHarga, 0, ',', '.');
		$dt['ppn'] = number_format($ppn, 0, ',', '.');
		$dt['discountNom'] = number_format($discountNom, 0, ',', '.');
		$dt['discount'] = $discount;
		$dt['downPayment'] = number_format($downPayment, 0, ',', '.');
		$dt['addCost'] = number_format($addCost, 0, ',', '.');
		$dt['terbilang'] = angkaKeTeks($totalHarga) . " rupiah";
		$datas['addon'] = $dt;
		// echo '<pre>';
		// var_dump($matdata);
		// var_dump($datas);
		// echo '</pre>';
		$this->load->view('nativeInvoice', $datas);
		// $this->load->library('pdf');
		// $this->pdf->printInvoices('nativeInvoice', $datas, $datas['inv'][0]['no_invoice']);
	}
	function convertCurrency($curr)
	{
		return preg_replace('/[^0-9]/', '', $curr);
	}
}

/* End of file  Accounting.php */

