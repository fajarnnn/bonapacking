<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displaayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('isLogin'))
			redirect(base_url());
		$this->load->Model('M_admin');
		$this->load->Model('M_user');
		$this->load->Model('M_packing');
		$this->load->library('Form_validation');
	}
	public function index()
	{
		$group = array('no_pl', 'area');
		$datas = $this->M_admin->init('packing_list', $group)->result();

		$data['all'] = $datas;
		$this->load->view('admin/dashboard', $data);
	}
	public function login()
	{

		$this->load->view('Login');
	}

	public function user()
	{
		$data['users'] = $this->M_user->init()->result();
		$data['corporate'] = $this->M_packing->getCorporate()->result();
		$this->load->view('header_page', $data);
		$this->load->view('admin/user', $data);
	}
	function add_user()
	{
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$password = md5($password);
		$corporate = "PT. Bona Nusantara";
		$isLogin = false;
		$role_id = $this->input->post('role');
		if ($role_id == 3)
			$corporate = $this->input->post('corporate');
		$email = $this->input->post('email');
		$data = array(
			'username' => $username,
			'password' => $password,
			'corporate' => $corporate,
			'isLogin' => $isLogin,
			'role_id' => $role_id,
			'email' => $email,
			'nama' => $nama
		);
		if (!$this->M_user->add_user($data))
			$this->session->set_flashdata('addUserMsg', "tambah user gagal! Terjadi Kesalahan pada sistem");
		// echo "<pre>";
		// var_dump($data);
		// echo "</pre>";;
		// exit();
		redirect('admin/user');

	}
	function edit_user()
	{
		$id = $this->input->post('editid');
		$nama = $this->input->post('editnama');
		$username = $this->input->post('editusername');
		$password = $this->input->post('editpassword');
		
		// $isLogin = false;
		$role_id = $this->input->post('editrole');
		$email = $this->input->post('editemail');
		if ($password == "" || !isset($password)) {
			$data = array(
				'username' => $username,
				'role_id' => $role_id,
				'email' => $email,
				'nama' => $nama
			);
		} else {
			$password = md5($password);
			$data = array(
				'username' => $username,
				'password' => $password,
				'role_id' => $role_id,
				'email' => $email,
				'nama' => $nama
			);
		}
		// echo "<pre>";
		// var_dump($id);
		// echo "</pre>";;
		// exit();
		if (!$this->M_user->updateUser($id, $data))
			$this->session->set_flashdata('addUserMsg', "tambah user gagal! Terjadi Kesalahan pada sistem");
		redirect('admin/user');

	}
	function delete($id)
	{
		$role = $this->session->userdata('role');
		$iduser = $this->session->userdata('id');
		if ($role == 1) {
			if ($id != $iduser) {
				if ($this->M_user->delete_user($id))
					;
			}
		}
		redirect(base_url('admin/user'));
	}
}
