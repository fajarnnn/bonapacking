<?php if (!defined('BASEPATH'))
	exit('No direct script access allowed');

function angkaKeTeks($angka)
{
	$angka = intval($angka);
	// echo $angka;
	$ribuan = ["", "ribu", "juta", "miliar", "triliun"];

	if ($angka == 0) {
		return strtoupper("nol");
	}

	$hasil = "";
	$i = 0;
	$angkas = intval($angka);
	while ($angka > 0) {
		$sisa = $angka % 1000;
		if ($sisa != 0) {
			if ($i == 1) {
				$teksSisa = konversiRibuan($sisa, $i);
			} else {
				$teksSisa = konversiRatusan($sisa);
			}
			if($i == 1 && $angkas < 2000)
				$k = 0;
			else
				$k=$i;
			$hasil = $teksSisa . " " . $ribuan[$k] . " " . $hasil;
		}
		$angka = (int) ($angka / 1000);
		$i++;
	}

	return trim(strtoupper($hasil));
}

function konversiRatusan($angka)
{
	$satuan = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan"];
	$belasan = ["sepuluh", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas"];
	$puluhan = ["", "sepuluh", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh"];

	$hasil = "";

	if ($angka >= 100) {
		$ratus = (int) ($angka / 100);
		$angka %= 100;
		if ($ratus == 1) {
			$hasil .= "seratus ";
		} else {
			$hasil .= $satuan[$ratus] . " ratus ";
		}
	}

	if ($angka >= 20) {
		$puluh = (int) ($angka / 10);
		$angka %= 10;
		$hasil .= $puluhan[$puluh];
		if ($angka > 0) {
			$hasil .= " " . $satuan[$angka];
		}
	} elseif ($angka >= 10) {
		$hasil .= $belasan[$angka - 10];
	} else {
		$hasil .= $satuan[$angka];
	}

	return trim($hasil);
}

function konversiRibuan($angka, $level)
{
	if ($angka == 0)
		return "";

	if ($level == 1 && $angka == 1) {
		return "seribu";
	} else {
		return konversiRatusan($angka);
	}
}

function konversiJutaan($angka, $level)
{
	if ($angka == 0)
		return "";

	if ($level == 2 && $angka == 1) {
		return "satu juta";
	} else {
		return konversiRatusan($angka);
	}
}

function localIndoToDate($string)
{
	// "date_default_timezone_set" may be required by your server
	$string = indotowordl($string);
	if (strlen($string) > 0) {
		date_default_timezone_set('Asia/Jakarta');
		// make a DateTime object 
		// the "now" parameter is for get the current date, 
		// but that work with a date recived from a database 
		// ex. replace "now" by '2022-04-04 05:05:05'
		$dateTimeObj = new DateTime($string, new DateTimeZone('Asia/Jakarta'));

		// format the date according to your preferences
		// the 3 params are [ DateTime object, ICU date scheme, string locale ]
		$dateFormatted =
			IntlDateFormatter::formatObject(
				$dateTimeObj,
				'dd MMMM YYYY',
				'id'
			);

		return $dateFormatted;
	} else {
		return $string;
	}
}
function localIndoToDateFormat($string, $format)
{
	// "date_default_timezone_set" may be required by your server
	$string = indotowordl($string);
	if (strlen($string) > 0) {
		date_default_timezone_set('Asia/Jakarta');
		// make a DateTime object 
		// the "now" parameter is for get the current date, 
		// but that work with a date recived from a database 
		// ex. replace "now" by '2022-04-04 05:05:05'
		$dateTimeObj = new DateTime($string, new DateTimeZone('Asia/Jakarta'));

		// format the date according to your preferences
		// the 3 params are [ DateTime object, ICU date scheme, string locale ]
		$dateFormatted =
			IntlDateFormatter::formatObject(
				$dateTimeObj,
				$format,
				'id'
			);

		return $dateFormatted;
	} else {
		return $string;
	}
}

function indotowordl($string)
{
	$string = str_replace("Januari", "January", $string);
	$string = str_replace("Februari", "February", $string);
	$string = str_replace("Maret", "March", $string);
	$string = str_replace("Mei", "May", $string);
	$string = str_replace("Juni", "June", $string);
	$string = str_replace("Juli", "July", $string);
	$string = str_replace("Agustus", "August", $string);
	$string = str_replace("Oktober", "October", $string);
	$string = str_replace("Desember", "December", $string);

	return $string;
}
function toIndoWord($string)
{
	$string = str_replace("January","Januari", $string);
	$string = str_replace("February","Februari", $string);
	$string = str_replace("March","Maret", $string);
	$string = str_replace("May","Mei", $string);
	$string = str_replace("June","Juni", $string);
	$string = str_replace("July","Juli", $string);
	$string = str_replace("August","Agustus", $string);
	$string = str_replace("October","Oktober", $string);
	$string = str_replace("December","Desember", $string);

	return $string;
}
