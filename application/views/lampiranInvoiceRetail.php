<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
	<style>
		@page {
			size: Letter;
			margin: 1rem, 1rem, 3rem, 1rem;
			padding: 1rem;
		}
	</style>
</head>

<body>
	<div class="grid-container">
		<table style="border:none; border-spacing: 0px;width: 100%;">
			<tr>
				<td style="width: 60%; vertical-align:top">
					<h2 style="margin: 0px;">PT.BONA NUSANTARA RAYA SAKTI
					</h2>
					<h3 style="margin: 0px;">JL. GARUDA NO. 80F - KEMAYORAN JAKARTA PUSAT</h3>
					<h3 style="margin: 0px;">TELP: 021-4202660</h3>

				</td>
				<td style="width:40%; text-align:center;vertical-align: top;">
					<h2 style="margin: 0px;">INVOICE
					</h2>
					<h3 style="margin: 0px;"><?= $invoice_number ?>
					</h3>
				</td>
			</tr>
			<tr>
				<TD></TD>
			</tr>
		</table>
		<table style="border:none; border-spacing: 0px;width: 100%; margin-top: 1rem;">
			<tr>
				<td style="width: 60%; vertical-align:top">
					<h2 style="margin: 0px;"><?= $tujuanInvoice == "penerima" ? $corporate : $supplier ?>
					</h2>
					<h3 style="margin: 0px;">TUJUAN: <?= $tujuan ?></h3>
					<h3 style="margin: 0px;">JAKARTA, <?= toIndoWord(localIndoToDate($invoice_date)) ?></h3>

				</td>
			</tr>
			<tr>
				<TD></TD>
			</tr>
		</table>
		<div class="table">
			<table>
				<thead>
					<tr>
						<th> NO <BR>URUT</th>
						<th> BIAYA PENGIRIMAN UNTUK BARANG</th>
						<th> JUMLAH <BR>COLLY</th>
						<th> JENIS <BR>COLLY</th>
						<th style="padding-left: 5px;padding-right: 5px;"> VOLUME / <BR>TONASE </th>
						<th style="padding-left: 5px;padding-right: 5px;"> TARIF <BR>RP</th>
						<th> JUMLAH <BR>RP</th>
					</tr>
				</thead>

				<body>
					<?php

					$no = 1;
					foreach ($data as $pl => $corps):
						if ($pl != "-"):
							?>
							<tr>
								<td style="text-align: center;"></td>
								<td><strong>
										<p style='text-decoration: underline;'><?= "PL.NO : ".$pl ?></p>
									</strong></td>
								<td style="text-align: center;"></td>
								<td style="text-align: center;"></td>
								<td style="text-align: center;"></td>
								<td style="text-align: right;"></td>
								<td style="text-align: right;"></td>
							</tr>

							<?php
						endif;
						foreach ($corps as $corp => $posjis): ?>
							<tr>
								<td style="text-align: center;"></td>
								<td><strong>
										<p style='text-decoration: underline;'><?= $corp ?></p>
									</strong></td>
								<td style="text-align: center;"></td>
								<td style="text-align: center;"></td>
								<td style="text-align: center;"></td>
								<td style="text-align: right;"></td>
								<td style="text-align: right;"></td>
							</tr>
							<?php
							$pocnt = 0;
							foreach ($posjis as $posji => $matdata):
								$stripqty = 0;
								$stripvol = 0.0;
								$stripjml = 0;
								$samColl = [];
								$totalkub['isprint'] = false;
								$totalkub['val'] = 0;
								$totalcost['isprint'] = false;
								$totalcost['val'] = 0;


								uasort($matdata, function ($a, $b) {
									// echo $a['qty']."<br>";
									// echo $b['qty']."<br>";
					
									$specialInA = $a['qty'] != '0';
									$specialInB = $b['material'] != "-" || $b['material'] != "";
									// echo $specialInA;
									// echo $specialInB;
									if ($specialInA) {
										return 1;
									} else if ($specialInB) {
										return 1;
									} else {
										return 0;
									}
								});
								foreach ($matdata as $keymat => $mat):
									// echo $mat['volum'];
									if (($mat['material'] == "-" || $mat['material'] == "") && $mat['qty'] != "0") {
										$stripqty = $mat['qty'];
										$stripvol = $mat['volum'];
										$stripjml = $mat['totalcost'];
										continue;
									}
									$stringQty = $mat['qty'];
									if(is_numeric($mat['qty'])){
										$stringQty = $mat['qty'] != '0' ? $mat['qty'] + $stripqty : "";
									}
									?>
									<tr>
										<td style="text-align: center;"><?= $no ?></td>
										<td><?= $mat['material'] ?></td>
										<td style="text-align: right; padding-right: 5px;"><?=$stringQty ?></td>
										<td style="text-align: center;" ><?= $mat['satuan'] ?></td>
										<td style="text-align: center;" ><?= $mat['volum'] ?></td>
										<td style="text-align: right; padding-right: 5px;"><?= $mat['cost']  ?></td>
										<td style="text-align: right; padding-right: 5px;"><?= number_format($mat['totalcost'],'0',',','.') ?></td>

									</tr>
									<?php
									$stripqty = 0;
									$stripvol = 0.0;
									$stripjml = 0.0;
									$no++;
								endforeach; ?>
								<tr>
									<td style="text-align: center;"></td>
									<td style="padding-left: 10px;"><?= "PO.NO : ".str_replace("____", "<br>DO.NO : ", $posji) ?></td>
									<td style="text-align: center;"></td>
									<td style="text-align: center;"></td>
									<td style="text-align: center;"></td>
									<td style="text-align: right;"></td>
									<td style="text-align: right;"></td>
								</tr>
							<?php endforeach;
						endforeach;
					endforeach; ?>
					<tr>
						<td></td>
						<td colspan="1" style="border-right: 1px solid;">
							<?php if ($rekening_type == "PPN"): ?>
								<p style="font-family: 'Calibri'; margin-top:0.2rem">
									NAMA&nbsp;&nbsp;: PT.BONA
									NUSANTARA RAYA SAKTI
									<br>
									BANK
									&nbsp;&nbsp;:
									BRI<br>
									AC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
									<strong>0434-01-001066-303</strong><br>

								</p>

							<?php else: ?>
								<p style="font-family: 'Calibri'; margin-top:0.2rem">
									NAMA&nbsp;&nbsp;: RICO FAHLEVI
									HASIBUAN
									<br>
									BANK
									&nbsp;&nbsp;:
									BCA<br>
									AC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>6.840.299.161</strong><br>

								</p>
							<?php endif; ?>

						</td>

						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td rowspan="<?= ($rekening_type == 'PPN') ? ($cost_type == "Sudah PPN") ? '3' : '2' : "1" ?>"
							style="font-weight: bold;border: 1px solid;">
							<div>
								CATATAN : <br>
								DIHARAPKAN MENCANTUMKAN KETERANGAN NOMOR<BR>
								KWITANSI SAAT MELAKUKAN PEMBAYARAN
							</div>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align: center;border: 1px solid;">JUMLAH</td>
						<td style="text-align: right;border: 1px solid;padding-right: 5px;"><?= $total ?></td>
					</tr>
					<?php if ($rekening_type == 'PPN' && $cost_type == "Sudah PPN"): ?>
						<tr>
							<td></td>
							<td rowspan="<?= ($rekening_type == 'PPN') ? '2' : '1' ?>" style="border-rigth: 1px solid;">
							</td>
							<td></td>
							<td></td>
							<td style="text-align: center;border: 1px solid;">DPP</td>
							<td style="text-align: right;border: 1px solid;padding-right: 5px;"><?= $dpp ?></td>
						</tr>
					<?php endif;
					if ($rekening_type == 'PPN'): ?>
						<tr>
							<td></td>
							<?php if ($cost_type == "Belum PPN"): ?>
								<td></td>
							<?php endif; ?>
							<td></td>
							<td colspan="1" style="text-align: center;border: 1px solid;">PPN</td>
							<td style="text-align: center;">1,1%</td>
							<td style="text-align: right;padding-right: 5px;"><?= $ppn ?></td>

						</tr>
					<?php endif; ?>
					<tr>
						<td colspan="2" style="text-align: center;border: 1px solid;"></td>
						<td colspan="1" style="text-align: center;border: 1px solid"><?= $totqty ?>
						</td>
						<td colspan="1" style="text-align: center;border: 1px solid">COLLY</td>
						<td colspan="2" style="text-align: center;font-weight: bold; border: 1px solid">TOTAL</td>
						<td colspan="1" style="text-align: right;font-weight: bold;border: 1px solid;padding-right: 5px;"><?= $allTotal ?>
						</td>
					</tr>
				</body>
			</table>
		</div>
		<table style="width: 100%;">
			<tr>
				<td style="width:50%; text-align:left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami</td>

			</tr>
			<tr>
				<td style="width:50%; text-align:left;padding-top: 50px;">
					(Rico Fahlevi Hasibuan)
				</td>

			</tr>
		</table>
	</div>
</body>

</html>
<style>
	p {
		margin: 0;
	}

	/* .table table tbody tr td */
	.table>table {
		/* border: 10px; */
		border-collapse: collapse;
		width: 100%;
		border: 1px solid;
	}

	td {
		padding-left: 5px;
	}

	.table table tbody tr td {
		/* border: 10px; */
		border-right: 1px solid;
		/* page-break-inside: avoid !important; */
		/* page-break-after: auto; */
	}

	.table table thead tr th {
		/* border: 10px; */
		border: 1px solid;
	}



	.grid-container {
		/* display: grid; */
		font-family: 'Calibri';
		font-style: normal;
		font-size: 14px;
		/* grid-template-areas:
			'header header header header header header'
			'main main main main main main'
			'thead thead thead thead thead thead'
			'table table table table table table'
			'footer footer footer footer footer footer'; */
		/* gap: 5px; */
		/* background-color: #2196F3; */
		padding: 10px 10px 10px 10px;
	}

	.grid-container>div {
		/* background-color: rgba(255, 255, 255, 0.8); */
		/* text-align: center; */
		/* padding: 5px 0; */
	}
</style>
