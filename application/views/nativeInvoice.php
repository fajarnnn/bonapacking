

<!DOCTYPE html>
<html lang="en">
<html>
<head>
	<style>
		@page {
			size: 8.26in 5.83in;
			margin: 0;
			padding: 0;
		}

		body {
			font-size: 12px;
			font-family: sans-serif;
			margin: 1rem;
			letter-spacing: 0.5px;
			font-weight: 550;
		}

		p {
			font-size: 1em;
		}

		.img-head {
			/* margin-top: 3rem; */
			width: 100%;
			vertical-align: middle;
		}

		.img-head .imgs {
			width: 100%;
		}

		h3 {
			padding: 0;
			margin: 0;
			font-size: 16px;
		}

		p {
			padding: 0;
			margin: 0;
		}

		table {
			border-collapse: collapse;
		}

		table.kwitansi td {
			border: 2px solid black;
			border-bottom: 2px solid black;
			padding-left: 0.2rem;
			padding-top: 0.1rem;
		}

		table.kwitansi>* {
			font-size: 0.8em !important;
			font-family: sans-serif;
		}

		table.detail td {
			border: 2px solid black;
			text-align: right;
			padding-left: 0.3rem;
			padding-right: 0.3rem;
		}
		table.total {
			margin-top: 0.5rem;
		}
		table.total td {
			border: 2px solid black;
			text-align: right;
			padding-left: 0.3rem;
			padding-right: 0.3rem;
		}
		table.detail th {
			border: 2px solid black;
			text-align: center;
			/* font-weight: 500; */
		}

		.kwitansi-text {
			text-align: center;
		}

		.second-text {
			border-bottom: 3px solid black;
			border-top: 3px solid black;
			padding-top: 0.2rem;
			padding-bottom: 0.2rem;
			width: 70%;
		}

		.foot-number {
			border: 2px solid black;
			padding-left: 0.3rem;
			padding-right: 0.3rem;
		}

		.bona-ref {
			vertical-align: top;
			border: 2px solid black;
			padding-left: 1rem;
			padding-top: 1rem;
		}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<thead>
		
			<tr>
				<td style="width: 20%;">
					<div class="img-head">
						<img class="imgs" src="<?= base_url("assets/img/bona.png") ?>">
					</div>
				</td>

				<td style="vertical-align: top; padding-left: 1rem;">
					<h3><Strong>PT. BONA NUSANTARA RAYA SAKTI</Strong></h3>
					<p>
						Jl. Garuda No. 80 F, Kemayoran</br>
						Kota Administrasi Jakarta Pusat</br>
						021 - 4202660 / 4255068
					</p>
				</td>
				<td rowspan="2" style="width: 38%;">
					<div class="kwitansi-text">
						<h3><Strong>KWITANSI</Strong></h3>
					</div>
					<table class="kwitansi" style="width: 100%;">
						<tr>
							<td>
								<p>Tanggal</p>
								<strong><?=localIndoToDate($inv[0]['invoice_date'])?></strong>
							</td>
							<td>
								<p>Nomor</p>
								<strong><?=$inv[0]['no_invoice']?></strong>
							</td>
						</tr>
						<tr>
							<td>
								<p>Mata Uang</p>
								<p><strong>Indonesian Rupiah</strong></p>
							</td>
							<td>
								<p>Area</p>
								<P><strong><?=implode(",", array_keys($addon['areas']))?></strong></P>
							</td>
						</tr>
						<tr>
							<td>
								<p>PO NO</p>
								<strong><?= !$addon['no_po'] ? "-" : $inv[0]['no_po'] ?></strong>
							</td>
							<td>
								<p>SJN NO</p>
								<p><Strong><?= !$addon['no_sji'] ? "-" : $inv[0]['no_sji'] ?></Strong></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<div class="second-text">
						<p>Kepada :</p>
					</div>
					<h3 style="margin-top:10px"><strong><?=   $inv[0]['tujuan'] == "penerima" ?  $inv[0]['corporate'] : $inv[0]['supplier']?></h3>
				</td>
				<!-- <td>a</td> -->
			</tr>
		</thead>
		<tr>
			<td colspan="3" style="padding-top: 1rem;">
				<table class="detail" style="width: 100%">
					<thead>
						<th style="width:50%">Nama Barang</th>
						<th style="width:10%">Quantity</th>
						<th style="width:15%">@Harga</th>
						<th style="width:10%">Diskon</th>
						<th style="width:15%">Total Harga</th>
					</thead>
					<tbody>
						<td style="width:50%; text-align:left"><strong><?=$inv[0]['invoice_name']?></td>
						<td style="width:10%"><?= $addon['qty'] ?></td>
						<td style="width:10%"><?= intval($addon['cost']) > 0 ? $addon['cost'] :  $addon['hargaBefore']?></td>
						<td style="width:10%"><?=$addon['discount']?></td>
						<td style="width:20%"><?=$addon['hargaBefore']?></td>
					</tbody>
				</table>
				<table style="width: 100%; margin-top:0.3rem;margin-bottom:0.3rem">
					<tfoot>
						<tr>
							<td class="foot-text" style="width: 10%;">Terbilang : </td>
							<td class="foot-number"><?=strtolower($addon['terbilang'])?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="bona-ref" rowspan="2">
				<p>Keterangan :<br>
					<?= !$addon['materialStr'] ? "<br>" : $addon['materialStr']."</br>" ?>
					<?= !$addon['no_po'] ? "" : $inv[0]['no_po'] ?> <br>
					<?= !$addon['no_sji'] ? "" : $inv[0]['no_sji'] ?>
				</p>
			</td>
			<td style="padding-left: 0.5rem;">
				<table style="width: 100%" class="total">
					<tr>
						<td style="width:50%">Sub Total</td>
						<td style="width:50%" class="right"><?=$addon['hargaBefore']?></td>
					</tr>
					<tr>
						<td style="width:50%">Down Payment</td>
						<td style="width:50%" class="right"><?=$addon['downPayment']?></td>
					</tr>
					<tr>
						<td style="width:50%">Diskon</td>
						<td style="width:50%" class="right"><?=$addon['discountNom']?></td>
					</tr>
					<tr>
						<td style="width:50%">PPN (1.1%)</td>
						<td style="width:50%" class="right"><?=$addon['ppn']?></td>
					</tr>
					<tr>
						<td style="width:50%">Biaya Lain-Lain</td>
						<td style="width:50%" class="right"><?=$addon['addCost']?></td>
					</tr>
					<tr >
						<td style="width:50%"><strong>Total</strong></td>
						<td style="width:50%" class="right"><strong><?=$addon['total_harga']?></strong></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align: center">
				<h3>Disetujui Oleh</h3>
			</td>
		</tr>
		<?php 
		if($inv[0]['rekening_type'] == "PPN"):
		?>
		<tr>
			<td colspan="2">
				<p style="font-family: 'Calibri'; margin-top:0.2rem">
HARAP PEMBAYARAN DI TRANSFER KE REKENING BERIKUT INI : <br>
BANK &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: BRI<br>
CABANG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: KC.JAKARTA CEMPAKA MAS<br>
NO. REKENING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>0434-01-001066-303</strong><br>
ATAS NAMA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: PT.BONA NUSANTARA RAYA SAKTI
			</p>
			</td>
			<td style="text-align: center; vertical-align:bottom;padding-bottom: 1rem;">
				<p>______________________________________</p>
				<p>Rico Fahlevi Hasibuan</p>
			</td>
		</tr>
		<?php else: ?>
		<tr>
			<td colspan="2">
				<p style="font-family: 'Calibri'; margin-top:0.2rem">
HARAP PEMBAYARAN DI TRANSFER KE REKENING BERIKUT INI : <br>
BANK &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: BCA<br>
CABANG&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: KEM TOWER<br>
NO. REKENING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>6.840.299.161</strong><br>
ATAS NAMA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: RICO FAHLEVI HASIBUAN
			</p>
			</td>
			<td style="text-align: center; vertical-align:bottom;padding-bottom: 1rem;">
				<p>______________________________________</p>
				<p>Rico Fahlevi Hasibuan</p>
			</td>
		</tr>
		<?php endif;?>
	</table>
</body>
</html>
