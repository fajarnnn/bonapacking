<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>


				<div class="card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="example3" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PL</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PO</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No SJN
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Group</th>

									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Region</th>

									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Date Out
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Doc Return
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Unloading Date
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 0;
								foreach ($docret as $key => $value):
									# code...
									$no++;
									$dateIn = date_create($value->date_out);
									$dateNow = date_create(date("Y-m-d"));
									$diff = date_diff($dateIn, $dateNow);
									$df = $diff->format("%a");
									$trs = "";
									$tds = "";
									if (intval($df) >= 30) {
										$trs = "background-color: #e25f59;";
										$tds = "color:white";
									}
									?>
									<tr class="text-center" style="<?=$trs?>">

										<td style="<?=$tds?>"><?= $no ?></td>
										<td style="<?=$tds?>"><?= $value->no_pl ?></td>
										<td style="<?=$tds?>"><?= $value->no_po ?></td>
										<td style="<?=$tds?>"><?= $value->no_sji ?></td>
										<td style="<?=$tds?>"><?= $value->list_group ?></td>
										<td style="<?=$tds?>"><?= $value->area ?></td>
										<td style="<?=$tds?>"><?= localIndoToDate($value->date_out) ?></td>
										<td style="<?=$tds?>"><?= localIndoToDate($value->doc_return) ?></td>
										<td style="<?=$tds?>"><?= localIndoToDate($value->unloading_date) ?></td>
										<td style="<?=$tds?>">


											<button class="btn bg-gradient-warning mb-0"
												onclick='editDoc(<?= json_encode(json_encode($value)) ?>)'>EDIT
											</button>
											<button class="btn bg-gradient-dark mb-0"
												onclick='getPO(<?= json_encode($value); ?>)'>Detail
											</button>
											<button class="btn bg-gradient-danger mb-0"
												onclick='rejectPO(<?= json_encode($value); ?>)'>Reject
											</button>
											<a class="btn bg-gradient-info mb-0"
														href="<?= base_url() ?>FileGenerator/generatePDF/<?= $value->no_pl . "/" . rawurlencode($value->area) ?>"
														id="prt">Generate
													PDF</a>
											<!-- <?= json_encode($value) ?> -->
										</td>

									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>
</main>


<!-- ------ Modal ------------>
<div class="modal fade" id="modalDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Add Document Return Informations</h5>
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="docForm" method="post" enctype="multipart/form-data">
				<div class="modal-body">
					<section class="section dashboard">
						<div class="col-md-12 position-relative">
							<div class="row">
								<input name="id_po" type="hidden" class="form-control" id="id_po" placeholder="No PO"
									readonly>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">No PO</label>
									<input name="no_po" type="text" class="form-control" id="no_po" placeholder="No PO"
										readonly>
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">No SJN</label>
									<input name="no_sji" type="text" class="form-control" id="no_sji"
										placeholder="No SJN" readonly>
								</div>
							</div>
						</div>

						<div class="col-md-12 position-relative">
							<div class="row">
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Doc Return</label>
									<input name="doc_return" type="date" class="form-control" id="doc_return"
										placeholder="Invoice Date" value="">
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Unloading Date</label>
									<input name="unloading_date" type="date" class="form-control" id="unloading_date"
										placeholder="Unloading Date" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12 position-relative">
							<div class="row">
								<div class="col-md-12 position-relative">
									<div class="upload__box">
										<div class="upload__btn-box">
											<label class="upload__btn">
												<p>Upload images</p>
												<input type="file" multiple="" data-max_length="20"
													class="upload__inputfile" name='userfile[]'>
											</label>
										</div>

										<div class="upload__img-wrap">

										</div>
									</div>
								</div>
							</div>
						</div>



						<div class="col-md-0 position-relative mt-4">
							<button class="btn btn-primary" onclick="updatedoc(event)">SUBMIT</button>
						</div>
				</div>
			</form>
			</section>
		</div>
	</div>
</div>
<div class="modal" id="myModal">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">PO & Receveiver Informations</h4>
				<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid py-4" id="poDetails">
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<!-- Modal for editing data -->





<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.min.js"></script>
<!-- <script src="<?= base_url() ?>/assets/js/moment.js"></script> -->
<script src="<?= base_url() ?>/assets/js/moment-with-locales.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>

<script>
	var base_url = "<?php echo base_url(); ?>";
	var area = "";
</script>
<script>
	jQuery(document).ready(function () {
		ImgUpload();
	});

	function ImgUpload() {
		var imgWrap = "";
		var imgArray = [];

		$('.upload__inputfile').each(function () {
			$(this).on('change', function (e) {
				imgWrap = $(this).closest('.upload__box').find('.upload__img-wrap');
				var maxLength = $(this).attr('data-max_length');

				var files = e.target.files;
				var filesArr = Array.prototype.slice.call(files);
				var iterator = 0;
				// alert(filesArr);
				filesArr.forEach(function (f, index) {

					if (!f.type.match('image.*') && !f.type.match('video.*')) {
						return;
					}
					// alert(f.type);
					if (imgArray.length > maxLength) {
						return false
					} else {
						var len = 0;
						for (var i = 0; i < imgArray.length; i++) {
							if (imgArray[i] !== undefined) {
								len++;
							}
						}
						if (len > maxLength) {
							return false;
						} else {
							imgArray.push(f);

							var reader = new FileReader();
							reader.onload = function (e) {
								if (f.type.match('video.*')) {

									var html =
										"<div class='upload__img-box'><div class='img-bg'><video id='myvideo' width='100%' data-number='" +
										$(".upload__img - close").length + "' data-file='" + f
											.name + "' controls><source src='" +
										e.target.result +
										"' type='video/mp4'></video><div class='upload__img-close'>x</div></div></div>";
								} else {
									var html =
										"<div class='upload__img-box'><div style='background-image: url(" +
										e.target.result + ")' data-number='" + $(
											".upload__img-close").length + "' data-file='" + f
											.name +
										"' class='img-bg'><div class='upload__img-close'>x</div></div></div>";
								}
								imgWrap.append(html);
								iterator++;
							}
							reader.readAsDataURL(f);
						}
					}
				});
			});
		});

		$('body').on('click', ".upload__img-close", function (e) {
			var file = $(this).parent().data("file");
			console.log(file);
			let result = file.includes("exist");
			if (result == true) {
				Swal.fire({
					title: "Apakah anda akan menghapus file ini?",
					// showDenyButton: true,
					showCancelButton: true,
					confirmButtonText: "Submit",
					cancelButtonText: "Tunggu"
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						$.ajax({
							type: "POST",
							url: base_url + "Packing/deleteimage",
							data: {
								id: file
							},
							cache: false,
							success: function (response) {
								if (response == 'success') {
									Swal.fire("Deleted!", "", "success").then(function () {
										$("#" + file).remove();
									});

								} else {
									Swal.fire("Delete Failed!", "", "error").then(function () {

									});
								}

							}
						});
					} else {
						Swal.fire("Silahkan Periksa data kembali", "", "info");
					}
				});
			} else {
				for (var i = 0; i < imgArray.length; i++) {
					if (imgArray[i].name === file) {
						imgArray.splice(i, 1);
						break;
					}
				}
				$(this).parent().parent().remove();
			}
		});
	}
	new DataTable('#example3');
	$(document).ready(function () {
		// $(".js-basic-multiple").select2({
		// 	dropdownParent: $('#exampleModalCenter .modal-content')

		// });
	});
	var idpo = 0;

	function editDoc(a) {
		// console.log(a);
		var jsn = JSON.parse(a);
		console.log(jsn.id);
		idpo = jsn.id;

		$('#id_po').val(idpo);
		$('#no_sji').val(jsn.no_sji);
		$.ajax({
			type: "POST",
			url: base_url + "Packing/getImages",
			data: {
				id: idpo
			},
			cache: false,
			success: function (response) {
				// alert(response);
				$('.upload__img-wrap').html(response);
			}
		});
		$('#no_po').val(jsn.no_po);
		$('#no_sji').val(jsn.no_sji);
		document.cookie = "doc_ret=" + jsn.doc_return;
		// var mydate = new Date(jsn.doc_return)
		// Mengatur locale untuk Bahasa Indonesia
		var dt = Date.parse(jsn.doc_return);
		// alert(dt);
		var docdate = null;
		if (isNaN(dt)) {
			moment.locale('id');
			// Mengonversi string tanggal Indonesia menjadi objek Moment.js
			let tanggalObj = moment(jsn.doc_return, "DD MMMM YYYY");

			// Mengubah locale ke Inggris dan memformat tanggal
			let tanggalLokalInggris = tanggalObj.locale('en-gb').format('L');

			// alert(tanggalLokalInggris);
			docdate = moment(tanggalLokalInggris).format('YYYY-MM-DD');
		} else {
			docdate = moment(jsn.doc_return).format('YYYY-MM-DD');
		}
		$("#doc_return").val(docdate);
		$('#modalDoc').modal('show');
		// getPL(a, area);

	}

	function getPO(a) {
		var output = $('#poDetails');


		$.ajax({
			type: "POST",
			url: base_url + "/Packing/poDetails/",
			data: {
				po: a,
			},
			dataType: "text",
			cache: false,
			success: function (response) {
				$(output).html(response);
				console.log(response);
				$('#myModal').modal('show');
			}
		});
	}

	function updatedoc(e) {
		e.preventDefault();
		var docret = $('#doc_return').val();
		var unloading_date = $('#unloading_date').val();
		if (docret.length < 1) {
			alert("Mohon isi doc return!");
		} else {
			var form_data = new FormData($('#docForm')[0]);
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: base_url + "Packing/updateDocreturn",
						data: form_data,
						processData: false,
						contentType: false,
						cache: false,
						success: function (response) {
							console.log(response);
							if (response.includes("success")) {
								Swal.fire("Saved!", "", "success").then(function () {
									location.reload();
								});
							} else {
								Swal.fire("Gagal Update Data!", "", "error").then(function () {
									location.reload();
								});
							}
						}
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	}

	function rejectPO(a) {
		var output = $('#poDetails');
		Swal.fire({
			title: "Apakah anda akan Mereject data ini?",
			// showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak"
		}).then((result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				$.ajax({
					type: "POST",
					url: base_url + "/Packing/rejectPL",
					data: {
						po: a,
					},
					dataType: "text",
					cache: false,
					success: function (response) {
						console.log(response);
						if (response == "success") {
							Swal.fire("Saved!", "", "success").then(function () {
								location.reload();
							});
						} else {
							Swal.fire("Gagal Update Data!", "", "error").then(function () {
								location.reload();
							});
						}
					}
				});
			} else {
				Swal.fire("Silahkan Periksa data kembali", "", "info");
			}
		});


	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>
