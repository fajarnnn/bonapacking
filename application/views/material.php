<div class="row mb-3" id="matgroup<?= $nomat ?>">
    <div class="col-2 mb-md-0 mb-4">
        <input name="material[]" type="text" class="form-control" placeholder="Material">
    </div>
    <div class="col-2 mb-md-0 mb-4">
        <div class="row">
            <div class="col-4 p-1">
                <input name="panjang[]" type="text" class="form-control" id="p<?=$nomat?>" placeholder="P"
                    onchange="calculate(<?=$nomat?>)">
            </div>
            <div class="col-4 p-1">
                <input name="lebar[]" type="text" class="form-control" id="l<?=$nomat?>" placeholder="L"
                    onchange="calculate(<?=$nomat?>)">
            </div>
            <div class="col-4 p-1">
                <input name="tinggi[]" type="text" class="form-control" id="t<?=$nomat?>" placeholder="T"
                    onchange="calculate(<?=$nomat?>)">
            </div>
        </div>
    </div>
    <div class="col-md-2 mb-md-0 mb-4">
        <div class="row">
            <div class="col-7 p-1">
                <input name="kubikasi[]" type="text" class="form-control" id="k<?=$nomat?>" placeholder="kubikasi m3"
                    readonly>
            </div>
            <div class="col-5 p-1">
                <input name="berat[]" type="text" class="form-control" id="b<?=$nomat?>" placeholder="Kg"
                    onchange="calculate(<?=$nomat?>)">
            </div>
        </div>
    </div>
    <div class="col-md-2 mb-md-0 mb-4">
        <div class="row">
            <div class="col-5 p-1">
                <input name="qty[]" type="text" class="form-control" id="q<?=$nomat?>" placeholder="Qty"
                    onchange="calculate(<?=$nomat?>)">
            </div>
            <div class="col-7 p-1">
                <input name="satuan[]" type="text" class="form-control" id="validationTooltip04" placeholder="satuan">
            </div>
        </div>
    </div>
    <div class="col-md-2 mb-md-0 mb-4">
        <div class="row">
            <div class="col-3 p-1">
                <input name="ton[]" type="text" class="form-control" id="ton<?=$nomat?>" placeholder="Tonase" value=""
                    onchange="calculateTagihan()" readonly>
            </div>
            <div class="col-5 p-1">
                <input name="cost[]" type="text" class="form-control" id="cost<?=$nomat?>" placeholder="Cost" value=""
                    onchange="calculate(<?=$nomat?>)" onfocus="this.value=''">
            </div>
            <div class="col-4 p-1">
                <select class="form-select" aria-label="Default select example" name='costbill[]'
                    id='costbill<?=$nomat?>' onchange="calculate(<?=$nomat?>)" style="float: left;">
                    <option value="0">Default</option>
                    <option value="1">Ton</option>
                    <option value="2">Kg</option>
                    <option value="3">Kubik</option>
                    <option value="4">Unit</option>
                    <option value="5">Lot</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="totcost[]" type="text" class="form-control" placeholder="Total Cost" id="totcost<?=$nomat?>" onchange="calculateMan(<?=$nomat?>)"
            readonly>
    </div>
    <div class="col-md-1">
        <a class="btn btn-danger mb-0" href="javascript:removeMat(<?=$nomat?>);"><i class="fas fa-minus"
                aria-hidden="true"></i>&nbsp;&nbsp;</a>
    </div>
</div>
