<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/assets/img/bona.png">
    <title>
        Bona
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url() ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url() ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
</head>

<body class="g-sidenav-show  bg-gray-100">
    <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 "
        id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
                aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="<?= base_url() ?> ">
                <img src="<?= base_url() ?>/assets/img/bona.png" class="navbar-brand-img h-100" alt="main_logo">

            </a>
        </div>
        <hr class="horizontal dark mt-0">
        <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link  active" href="<?= base_url() ?>">

                        <div
                            class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>shop </title>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1716.000000, -439.000000)" fill="#FFFFFF"
                                        fill-rule="nonzero">
                                        <g transform="translate(1716.000000, 291.000000)">
                                            <g transform="translate(0.000000, 148.000000)">
                                                <path class="color-background opacity-6"
                                                    d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z">
                                                </path>
                                                <path class="color-background"
                                                    d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1">Dashboard</span>
                    </a>
                </li>





            </ul>
        </div>

    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->

        <form action="<?= base_url('packing/editDate/') ?>" method="post" enctype="multipart/form-data">
            <!-- End Navbar -->
            <div class="container-fluid py-4">
                <div class="col-md-12">
                    <div class="card mt-4">
                        <div class="card-body p-3">
                            <div class="row">
                                <div class="col-md-6 mb-md-0 mb-4">
                                    <label for="validationTooltip01" class="form-label">No PL</label>
                                    <input name="no_pl" type="text" class="form-control" id="validationTooltip04"
                                        placeholder="No PL" value="<?= $details[0]->no_pl ?>">

                                </div>
                                <div class="col-md-6">
                                    <label for="validationTooltip01" class="form-label">Area</label>
                                    <input name="area" type="text" class="form-control" id="validationTooltip04"
                                        placeholder="Area" value="<?= $details[0]->area ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <div class="card">
                            <div class="card-header pb-0 px-3">
                                <h6 class="mb-0">PO Information</h6>
                            </div>
                            <div class="card-body pt-4 p-3">
                                <ul class="list-group">
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="d-flex flex-column">

                                            <input name="no_po" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="No PO"
                                                value="<?= $details[0]->no_po ?>">
                                            <br>
                                            <input name="supplier" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Supplier"
                                                value="<?= $details[0]->supplier ?>">


                                            <br>
                                            <input name="date_in" type="date" class="form-control"
                                                id="validationTooltip04" placeholder="Date In"
                                                value="<?= $details[0]->date_in ?>">
                                            <br>
                                            <input name="date_out" type="date" class="form-control"
                                                id="validationTooltip04" placeholder="Date Out"
                                                value="<?= $details[0]->date_out ?>">

                                        </div>
                                        <div class="ms-auto">
                                            <input name="no_sji" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="No SJN"
                                                value="<?= $details[0]->no_sji ?>">
                                            <br>


                                            <input name="no_pot" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="No POT"
                                                value="<?= $details[0]->no_pot ?>">
                                            <br>
                                            <input name="remark" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="remark"
                                                value="<?= $details[0]->remark ?>">
                                            <br>
                                            <input name="invoice_date" type="date" class="form-control"
                                                id="validationTooltip04" placeholder="Invoice Date"
                                                value="<?= $details[0]->invoice_date ?>">
                                            <br>
                                            <input name="no_invoice" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Invoice Number"
                                                value="<?= $details[0]->no_invoice ?>">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-4">
                        <div class="card">
                            <div class="card-header pb-0 px-3">
                                <h6 class="mb-0">Reciever & Shipping Information</h6>
                            </div>
                            <div class="card-body pt-4 p-3">
                                <ul class="list-group">
                                    <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                        <div class="d-flex flex-column">

                                            <input name="corporate" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Corporate Name"
                                                value="<?= $details[0]->corporate ?>">
                                            <br>
                                            <input name="address" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Address"
                                                value="<?= $details[0]->address ?>">
                                            <br>
                                            <input name="street" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Street"
                                                value="<?= $details[0]->street ?>">
                                            <br>
                                            <label for="validationTooltip01" class="form-label">PIC</label>
                                            <input name="pic" type="text" class="form-control" id="validationTooltip04"
                                                placeholder="Name"
                                                value="<?= isset(explode('/', $details[0]->pic)[1]) ? explode('/', $details[0]->pic)[1] : "-" ?>">
                                            <br>
                                            <input name="Phone" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Phone"
                                                value="<?= isset(explode('/', $details[0]->pic)[0]) ? str_replace('TLP:', '', trim(explode('/', $details[0]->pic)[0])) : "" ?>">
                                            <br>
                                            <input name="plant" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Plant"
                                                value="<?= $details[0]->plant ?>">
                                            <br>
                                            <input name="doc_return" type="date" class="form-control"
                                                id="validationTooltip04" placeholder="Document Return"
                                                value="<?= $details[0]->doc_return ?>">


                                        </div>
                                        <div class="ms-auto">
                                            <select class="form-select" aria-label="Default select example"
                                                name='shipping'>

                                                <option value="" disabled>Shipping Method</option>
                                                <?php if (isset($details[0]->driver)) { ?>
                                                <option value="Truck" selected>Truck</option>
                                                <option value="Vessel">Vessel</option>
                                                <?php } else { ?>
                                                <option value="Truck">Truck</option>
                                                <option value="Vessel" selected>Vessel</option>
                                                <?php } ?>
                                            </select>
                                            <br>

                                            <input name="vessel_name" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Name"
                                                value="<?= isset($details[0]->driver) ? $details[0]->driver : $details[0]->vessel_name ?>">
                                            <br>
                                            <input name="plate" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Plate"
                                                value="<?= isset($details[0]->plate) ? $details[0]->plate :  $details[0]->vessel_no_segel  ?>">
                                            <br>
                                            <input name="driverPhone" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Phone Number"
                                                value="<?= isset($details[0]->phone) ? $details[0]->phone :  $details[0]->vessel_no_cont ?>">
                                            <br>
                                            <input name="list_group" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="Group"
                                                value="<?= $details[0]->list_group ?>">
                                            <br>
                                            <input name="no_voyage" type="text" class="form-control"
                                                id="validationTooltip04" placeholder="No Voyage"
                                                value="<?= isset($details[0]->vessel_no_voyage) ? $details[0]->vessel_no_voyage :  '-' ?>">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="card mt-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Material Informations</h6>
                            </div>

                        </div>
                    </div>
                    <div class="card-body p-3 mb-3" id="matGrup">
                        <div class="row mb-3">
                            <div class="col-md-2 mb-md-0 mb-4">
                                <label>Material</label>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <label>Panjang</label>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <label>Lebar</label>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <label>Tinggi</label>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <label>Kubikasi</label>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <label>Berat</label>
                            </div>
                            <div class="col-md-2 mb-md-0 mb-4">
                                <label>Satuan</label>
                            </div>
                            <div class="col-md-2 mb-md-0 mb-4">
                                <label>Cost</label>
                            </div>
                        </div>
                        <?php $no = 0;
                        foreach ($details as $key => $value) {
                            # code<?=base_url()
                            $no++;
                        ?>
                        <div class="row mb-3">
                            <div class="col-md-2 mb-md-0 mb-4">
                                <input name="material[]" type="text" class="form-control" id="validationTooltip04"
                                    placeholder="Material" value="<?= $value->material ?>">
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <input name="panjang[]" type="text" class="form-control" id="p<?=$no?>"
                                    placeholder="P" value="<?= $value->panjang ?>" onchange="calculate(<?=$no?>)">
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <input name="lebar[]" type="text" class="form-control" id="l<?=$no?>"
                                    placeholder="L" value="<?= $value->lebar ?>" onchange="calculate(<?=$no?>)">
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <input name="tinggi[]" type="text" class="form-control" id="t<?=$no?>"
                                    placeholder="T" value="<?= $value->tinggi ?>" onchange="calculate(<?=$no?>)">
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <input name="kubikasi[]" type="text" class="form-control"id="k<?=$no?>"
                                    placeholder="kubikasi m3" value="<?= $value->kubikasi ?>" readonly>
                            </div>
                            <div class="col-md-1 mb-md-0 mb-4">
                                <input name="berat[]" type="text" class="form-control" id="validationTooltip04"
                                    placeholder="Kg" value="<?= $value->berat ?>">
                            </div>
                            <div class="col-md-2 mb-md-0 mb-4">
                                <input name="satuan[]" type="text" class="form-control" id="validationTooltip04"
                                    placeholder="satuan" value="<?= $value->satuan ?>">
                            </div>
                            <div class="col-md-2 mb-md-0 mb-4">
                                <input name="cost_kubik[]" type="text" class="form-control" id="validationTooltip04"
                                    placeholder="m3" value="<?= $value->cost_kubik ?>">
                            </div>
                            <?php
                                if ($no == 1) {




                                ?>

                            <div class="col-md-1 text-end">
                                <a class="btn bg-gradient-dark mb-0" href="javascript:addMat();"><i class="fas fa-plus"
                                        aria-hidden="true"></i>&nbsp;&nbsp;</a>
                            </div>
                            <?php } ?>


                        </div>
                        <?php } ?>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-12 text-center py-4">
                    <input class="btn bg-gradient-success mb-0" type="submit" name="Add Data"></input>
                </div>
            </div>
        </form>



        <footer class="footer pt-3  ">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6 mb-lg-0 mb-4">
                        <div class="copyright text-center text-sm text-muted text-lg-start">
                            © <script>
                            document.write(new Date().getFullYear())
                            </script>,
                            made with <i class="fa fa-heart"></i> by
                            <a href="https://www.breathid.tech" class="font-weight-bold">Breath Tech
                                Ind</a>
                            for a better applications.
                        </div>
                    </div>

                </div>
            </div>
        </footer>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>
<script>
	calculate(1);
	mat = 1;
function addMat(params) {
	mat +=1;
    var output = $('#matGrup');
    var base_url = "<?php echo base_url(); ?>";
    $.ajax({
        type: "POST",
        url: base_url + "Packing/loadMaterial",
        data: {noMat:mat},
        cache: false,
        success: function(response) {
            $(output).last().append(response);
        }
    });
}
function calculate(no) {
		var p = $('#p'+no).val();
		var l = $('#l'+no).val();
		var t = $('#t'+no).val();
		var k = $('#k'+no);
		
		var kubikasi = p * l * t / 1000000;
		k.val(kubikasi);
		
	}
</script>

</html>
