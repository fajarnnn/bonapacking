<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
    <title>Document</title>
</head>

<body>
    <div class="grid-container">
        <div class="header">
            <h2>DAFTAR PENGIRIMAN BARANG</h2>
        </div>
        <div class="left-item">
            <table style="border:none; border-spacing: 0px;width: 100%;">
                <tr>
                    <td style="width: 60%;">
                        <!-- <?php var_dump($pl_detail['driver']) ?> -->
                        <h4><?= isset($pl_detail["vehicle"]) ? $pl_detail["vehicle"] : "-" ?></h4>
                        <h4><?= isset($pl_detail["area"]) ? $pl_detail["area"] : "-" ?></h4>
                        <h4>Jakarta, 04 Juni 24</h4>

                    </td>
                    <td style="width:40%; text-align:center;vertical-align: top;padding-top: 20px;">
                        <img src="<?= base_url() ?>assets/img/print.png">
                        <h4 style="padding-top: 10px;">NO.PL : <?= isset($pl_detail["pl"]) ? $pl_detail["pl"] : "-" ?>
                        </h4>
                    </td>
                </tr>
                <!-- <tr style="line-height: 0px;">
					<td style="font-weight: bold;width:10%">Tujuan</td>
					<td style="width: 50%;"><pre> : Jl. Kemuning Pasir Kuda, Jakarta</pre></td>
				</tr>
				<tr style="line-height: 0px;">
					<td style="font-weight: bold;width:10%">Tanggal Masuk</td>
					<td style="width: 50%;"><pre> : 12 Maret 2024</pre></td>
				</tr>
				<tr style="line-height: 0px;">
					<td style="font-weight: bold;width:10%">Tanggal Keluar</td>
					<td style="width: 50%;"><pre> : 16 Maret 2024</pre></td>
				</tr>
				<tr style="line-height: 0px;">
					<td style="font-weight: bold;">Grup</td>
					<td><pre> : Lamongan</pre></td>
				</tr> -->
            </table>
        </div>
        <div class="thead">
            <h4>Supir: <?= isset($pl_detail["driver"]) ? $pl_detail["driver"] : "-" ?>
                <?= isset($pl_detail["phone"]) ? $pl_detail["phone"] : "-" ?></h4>
        </div>
        <div class="table">
            <table>
                <thead>
                    <tr>
                        <th> No</th>
                        <th> Penerima</th>
                        <th> Barang</th>
                        <th> No PO</th>
                        <th> Jumlah </th>
                        <th> Satuan</th>
                        <th> Keterangan</th>

                    </tr>
                </thead>
                <tbody>
                    <?php $qtot = 0;
                    $no = 0;
                    foreach ($details as $key => $value) {
                        # code...
                        $no++;
                    ?>
                    <tr>
                        <td style="vertical-align: middle;text-align: center;"><?= $no ?></td>
                        <td style="vertical-align: top;padding-left: 5px;">
                            <p><?= $key ?></p>
                        </td>
                        <td style="padding-left: 5px;vertical-align: top;">
                            <?php foreach ($value as $key2 => $value2) {
                                    # code...
                                    foreach ($value2 as $key3 => $value3) {
                                        # code...


                                ?>
                            <p><?= isset($value3->material) ? $value3->material : "-" ?></p>

                            <?php }
                                } ?>
                        </td>
                        <td style="text-align: center;vertical-align: top;">
                            <?php foreach ($value as $key2 => $value2) {
                                    # code...
                                    $kk = "";
                                    foreach ($value2 as $key3 => $value3) {
                                        # code...

                                        if ($kk != $key2) {
                                            echo '<p>' . $key2 . '</p>';
                                            $kk = $key2;
                                        } else {
                                            echo '<p><br/></p>';
                                        }

                                ?>

                            <?php }
                                } ?>
                        </td>
                        <td style="text-align: center; vertical-align: top;;">
                            <?php foreach ($value as $key2 => $value2) {
                                    # code...
                                    foreach ($value2 as $key3 => $value3) {
                                        # code...
                                        $qtot += $value3->qty;

                                ?>
                            <p><?= isset($value3->qty) ? $value3->qty : "-" ?></p>

                            <?php }
                                } ?>
                        </td>
                        <td style="text-align: center; vertical-align:top">
                            <?php foreach ($value as $key2 => $value2) {
                                    # code...
                                    foreach ($value2 as $key3 => $value3) {
                                        # code...


                                ?>
                            <p><?= isset($value3->satuan) ? $value3->satuan : "-" ?></p>

                            <?php }
                                } ?>
                        </td>
                        <td style="text-align: center;"><?= isset($value->remark) ? $value->remark : "-" ?></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="4" style="text-align: center;font-weight: bold;">TOTAL</td>
                        <td colspan="1" style="text-align: center;"><?= $qtot ?></td>
                        <td colspan="1" style="text-align: center;">Colly</td>
                        <td colspan="1" style="text-align: center;"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="item5">
            <div class="footer-item">
                <div class="penerima">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width:50%; text-align:center">Yang Menerima</td>
                            <td style="width:50%; text-align:center">Yang Menyerahkan</td>
                        </tr>
                        <tr>
                            <td style="width:50%; text-align:center;padding-top: 50px;">
                                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                            </td>
                            <td style="width:50%; text-align:center;padding-top: 50px;">
                                (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
<style>
.thead {
    grid-area: thead;
    text-align: center;
}

.penerima {
    grid-area: terima;
}

.pengirim {
    grid-area: kirim;
}

.header {
    grid-area: header;
    text-align: center;
}

.left-item {
    grid-area: main;
    text-align: left;
}

.image {
    grid-area: right;
    text-align: center;
}

.table {
    grid-area: table;
}

p {
    margin: 5px 0px 5px 0px;
}

/* .table table tbody tr td */
.table>table {
    /* border: 10px; */
    border-collapse: collapse;
    width: 100%;
    border: 1px solid;
}

.table table tbody tr td {
    /* border: 10px; */
    border: 1px solid;
}

.table table thead tr th {
    /* border: 10px; */
    border: 1px solid;
}

.item5 {
    grid-area: footer;
}

.footer-item {
    display: grid;
    grid-template-areas:
        'terima terima terima terima'
    ;
    gap: 10px;
    /* background-color: #2196F3; */
    padding: 20px;
}

.grid-container {
    display: grid;
    font-family: 'Calibri';
    font-style: normal;
    font-size: 11px;
    grid-template-areas:
        'header header header header header header'
        'main main main main main main'
        'thead thead thead thead thead thead'
        'table table table table table table'
        'footer footer footer footer footer footer';
    gap: 5px;
    /* background-color: #2196F3; */
    padding: 10px 10px 10px 10px;
}

.grid-container>div {
    /* background-color: rgba(255, 255, 255, 0.8); */
    /* text-align: center; */
    padding: 5px 0;
}
</style>