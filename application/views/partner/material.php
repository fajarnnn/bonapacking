<div class="row mb-3">
    <div class="col-md-3 mb-md-0 mb-4">
        <input name="material[]" type="text" class="form-control" id="mat" placeholder="Material" > 
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="panjang[]" type="text" class="form-control" id="p<?=$nomat?>" placeholder="P" onchange="calculate(<?=$nomat?>)">
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="lebar[]" type="text" class="form-control" id="l<?=$nomat?>" placeholder="L" onchange="calculate(<?=$nomat?>)">
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="tinggi[]" type="text" class="form-control" id="t<?=$nomat?>" placeholder="T" onchange="calculate(<?=$nomat?>)">
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="kubikasi[]" type="text" class="form-control" id="k<?=$nomat?>" placeholder="kubikasi m3" readonly>
    </div>
    <div class="col-md-1 mb-md-0 mb-4">
        <input name="berat[]" type="text" class="form-control" id="b<?=$nomat?>" placeholder="Kg" onchange="calculateTagihan()">
    </div>
    <div class="col-md-2 mb-md-0 mb-4">
        <input name="satuan[]" type="text" class="form-control" id="validationTooltip04" placeholder="satuan">
    </div>

</div>
