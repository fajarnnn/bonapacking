<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/assets/img/bona.png">
    <title>
        Bona
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url() ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/2.0.8/css/dataTables.uikit.css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/css/uikit.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url() ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
</head>

<body class="g-sidenav-show  bg-gray-100">
    <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="<?= base_url() ?> ">
                <img src="<?= base_url() ?>/assets/img/bona.png" class="navbar-brand-img h-100" alt="main_logo">

            </a>
        </div>
        <hr class="horizontal dark mt-0">
        <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link  active" href="<?= base_url('partner') ?>">

                        <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>shop </title>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1716.000000, -439.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                        <g transform="translate(1716.000000, 291.000000)">
                                            <g transform="translate(0.000000, 148.000000)">
                                                <path class="color-background opacity-6" d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z">
                                                </path>
                                                <path class="color-background" d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1">Dashboard</span>
                    </a>
                </li>





            </ul>
        </div>

    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->

        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">

                        <div class="col-xl-6">
                            <div class="row">


                            </div>
                        </div>
                        <div class="col-md-12 mb-lg-0 mb-4">
                            <div class="card mt-4">
                                <div class="card-header pb-0 p-3">

                                </div>
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-md-6 mb-md-0 mb-4">
                                            <h6 class="mb-0">No Packing List</h6>
                                            <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">


                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->no_pl) ? $details[0]->no_pl : "-" ?>
                                                </h6>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="mb-0">Area</h6>
                                            <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">

                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->area) ? $details[0]->area : "-" ?>

                                                    </h5>
                                                </h6>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row my-4">
                <div class="col-lg">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-3">
                                <table id="example3" class="table table-striped table align-items-center mb-0" style="width:100%">
                                    <thead>
                                        <tr class="text-center">
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No PO</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                No SJI</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Penerima</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Detail
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($details as $key => $value) {
                                            # code<?=base_url()
                                            $nopo = isset($value->no_po) ? $value->no_po : "-";
                                            $nosji = isset($value->no_sji) ? $value->no_sji : "-";
                                            // $id = isset($value->id) ? $value->id : "-";
                                            $rawpo = rawurlencode(str_replace('/', '_', $nopo));
                                            $rawsji = rawurlencode(str_replace('/', '_', $nosji));
                                            $no++;
                                        ?>
                                            <tr class="text-center">
                                                <td><?= $no ?></td>
                                                <td><?= isset($value->no_po) ? $value->no_po : "-" ?></td>
                                                <td><?= isset($value->no_sji) ? $value->no_sji : "-" ?></td>
                                                <td><?= isset($value->corporate) ? $value->corporate : "-" ?></td>
                                                <td>

                                                    <span class="btn bg-gradient-dark mb-0" data-bs-toggle="modal" data-bs-target="#myModal">Detail</span>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer pt-3  ">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6 mb-lg-0 mb-4">
                        <div class="copyright text-center text-sm text-muted text-lg-start">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>,
                            made with <i class="fa fa-heart"></i> by
                            <a href="https://www.breathid.tech" class="font-weight-bold">Breath
                                Tech
                                Ind</a>
                            for a better applications.
                        </div>
                    </div>

                </div>
            </div>
        </footer>
        </div>
    </main>

    <!---------------- Modal Start --------------->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">PO & Receveiver Informations</h4>
                    <button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid py-4">
                        <div class="row">
                            <div class="col-md-6 mt-4">
                                <div class="card">
                                    <div class="card-header pb-0 px-3">
                                        <h6 class="mb-0">PO Information</h6>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <ul class="list-group">
                                            <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                                <div class="d-flex flex-column">
                                                    <h6 class="mb-3 text-sm">
                                                        <?= isset($details[0]->no_po) ? $details[0]->no_po : "-" ?></h6>
                                                    <span class="mb-3 text-xs">Supplier
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->supplier) ? $details[0]->supplier : "-" ?></span></span>



                                                    <span class="mb-3 text-xs">Date
                                                        In&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_in) ? $details[0]->date_in : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Date
                                                        Out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_out) ? $details[0]->date_out : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Unload
                                                        Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_out) ? $details[0]->date_out : "-" ?></span></span>

                                                </div>

                                                <div class="ms-auto">
                                                    <h6 class="mb-3 text-sm">
                                                        <?= isset($details[0]->no_sji) ? $details[0]->no_sji : "-" ?>
                                                    </h6>

                                                    <span class="mb-3 text-xs">Tagihan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">RP 22.010/m3
                                                            xxx/Kg
                                                        </span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">No
                                                        POT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->no_pot) ? $details[0]->no_pot : "-" ?></span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->remark) ? $details[0]->remark : "-" ?></span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Inv
                                                        Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->invoice_date) ? $details[0]->invoice_date : "-" ?></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Inv
                                                        No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->no_invoice) ? $details[0]->no_invoice : "-" ?></span>

                                                </div>



                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mt-4">
                                <div class="card h-100">
                                    <div class="card-header pb-0 px-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="mb-0">Receiver & Shipping Informations</h6>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <ul class="list-group">
                                            <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                                <div class="d-flex flex-column">
                                                    <h6 class="mb-3 text-sm">4506441542</h6>
                                                    <span class="mb-3 text-xs">Penerima
                                                        &nbsp;&nbsp;: <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->receiver) ? $details[0]->receiver : "-" ?>
                                                            </p></span></span>
                                                    <span class="mb-3 text-xs">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        : <span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->address) ? $details[0]->address : "-" ?>
                                                        </span></span>

                                                    <span class="mb-3 text-xs">Jalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        : <span class="text-dark ms-sm-2 font-weight-bold">
                                                            <p class="text-sm">
                                                                <?= isset($details[0]->street) ? $details[0]->street : "-" ?>
                                                        </span></span>
                                                    <span class="mb-3 text-xs">PIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark ms-sm-2 font-weight-bold">
                                                            <p class="text-sm">
                                                                <?= isset($details[0]->pic) ? $details[0]->pic : "-" ?>
                                                            </p> |
                                                            085268845886
                                                        </span></span>
                                                    <span class="mb-3 text-xs">Plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->plant) ? $details[0]->plant : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Doc Return&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->doc_return) ? $details[0]->doc_return : "-" ?></span></span>
                                                </div>
                                                <div class="ms-auto">
                                                    <h6 class="mb-3 text-sm">SJ.NO.MSI/SH/0/0/0/0</h6>
                                                    <span class="text-xs">Transport
                                                        Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">Truck</span></span>

                                                    <br>
                                                    <span class="mb-2 text-xs">Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->list_group) ? $details[0]->list_group : "-" ?></span></span>

                                                </div>



                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <div class="card">
                                    <div class="card-header pb-0 px-3">
                                        <h6 class="mb-0">Material Information</h6>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <div class="table-responsive p-3">
                                            <table id="example3" class="table table-striped table align-items-center mb-0" style="width:100%">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            No</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Material</th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Panjang
                                                        </th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Lebar</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Tinggi</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Berat</th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Kubikasi
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Satuan
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Cost m3
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Cost Kg
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr class="text-center">





                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        </div>




                    </div>
                </div>
            </div>

        </div>
    </div>



    <!-------------- Modal End ------------>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.uikit.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>

    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
    <script>
        new DataTable('#example3');

        function getPO(a, b) {
            var output = $('#poDetails');

            var base_url = "<?php echo base_url(); ?>";
            $.ajax({
                type: "POST",
                url: base_url + "/Packing/poDetails/",
                data: {
                    no_po: a,
                    nosji: b
                },
                dataType: "text",
                cache: false,
                success: function(response) {
                    $(output).html(response);
                    // alert(response);
                }
            });
        }
        $('#prt').on('click', function() {
            var dt = "<?= $details[0]->no_pl ?>";
            if (dt.length < 1) {
                alert('No PL Kosong');
                return false;
            } else {
                return true;
            }
        });
    </script>
</body>

</html>