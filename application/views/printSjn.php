<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
</head>
<?php $dt = $sjn[0] ?>
<style>
	@page { size: 10cm 20cm landscape; }
</style>
<body>
	<div class="grid-container">
		<div class="header">
			<table style="border:none; border-spacing: 0px; width: 100%; margin: 0 auto;">

				<tr>
					<?php if (!$isPrint): ?>
						<td style="width: 33%; text-align: left;" class="left-item">
							<h2 style="margin: 0px;">PENGIRIM</h2>
						</td>

						<td style="width: 33%; text-align: center;" class="center-item">
							<img src="<?= base_url() ?>assets/img/print.png">
						</td>
						<td style="width: 33%; text-align: left;" class="right-item">
							<h2 style="margin: 0px;">PENERIMA</h2>
						</td>
					<?php else: ?>
						<td></td>
						<td></td>
						<td></td>
					<?php endif; ?>
				</tr>
				<tr>
					<td style="width: 33%; text-align: left; margin-left: 50px;" class="left-item">
						<h2 style="margin: 0px;"><?= $dt->supplier ?></h2>

					</td>

					<?php if (!$isPrint): ?>
						<td style="width: 33%; text-align: center;" class="right-item">
							<h2 style="margin: 0px;">PT BONA NUSANTARA RAYA SAKTI</h2>
							<h3 style="margin: 0px;">JL. Garuda No 80 F, Kemayoran, Jakarta Pusat, Indonesia</h3>
							<h3 style="margin: 0px;">No TLP : 021 - 4202660 /4255068 Fax : 021-4202948</h3>

						</td>
					<?php else:
						echo "<td></td>";
					endif; ?>
					<td style="width: 40%; text-align: left;" class="left-item">
						<h2 style="margin: 0px;"><?= $dt->corporate ?></h2>
						<h3 style="margin: 0px;"><?= $dt->street ?></h3>
						<h3 style="margin: 0px;"><?= $dt->area ?></h3>
						<h3 style="margin: 0px;"><?= $dt->pic ?></h3>
					</td>
				</tr>
			</table>
		</div>

		<!-- <h2 class="left-item">PT BONA NUSANTARA RAYA SAKTI</h2>
					<img src="<?= base_url() ?>assets/img/print.png">
					<h2>PT BONA NUSANTARA RAYA SAKTI</h2>
					<h3>JL. Garuda No 80 F, Kemayoran, Jakarta Pusat, Indonesia</h3>
					<h3>No TLP : 021 - 4202660 /4255068 Fax : 021-4202948</h3> -->


		<!-- <div class="thead">
			<h2>PT BONA NUSANTARA RAYA SAKTI</h2>
			<h3>JL. Garuda No 80 F, Kemayoran, Jakarta Pusat, Indonesia</h3>
			<h3>No TLP : 021 - 4202660 /4255068 Fax : 021-4202948</h3>


		</div> -->
		<div class="table">
			<table style="width: 100%; margin-top:50px">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>

					</tr>
				</thead>
				<tbody>
					<?php $totqty = 0;
					$no = 1;
					$qty = "";
					$material = "";
					$number = "";
					$remark = "";
					$satuan = "";
					$nopo = "";
					foreach ($sjn as $key => $value) {
						$qty .= $value->qty . "<br/>";

						$material .= $value->material . "<br/>";
						$remark .= $value->remark . "<br/>";
						$satuan .= $value->satuan . "<br/>";
						$totqty += $value->qty;
						if ($no > 1 && $nopo != $value->no_po)
							$material .= $nopo . "<br/>" . $value->no_sji;

						$nopo = $value->no_po;

						if ($no == count($sjn))
							$material .= $nopo . "<br/>" . $value->no_sji;
						$no++;
					} ?>
					<tr>
						<td style="text-align: center; width:10%"><?= $qty ?></td>

						</td>
						<td style="text-align: center;width:10%">
							<?= $satuan ?>
						</td>
						<td style="width:40%"><?= $material ?>
						</td>
						<td style="width:40%"><?= $remark ?></td>
					</tr>
					<tr style="margin-top:50px">
						<td colspan="1" style="text-align: center;font-weight: bold;width:10%"><?= $totqty ?></td>
						<td colspan="1" style="text-align: center;font-weight: bold;width:10%">COLLY</td>
						<td colspan="1" style="font-weight: bold;;width:40%">
							<?= angkaKeTeks($totqty) ?>
						</td>
						<td colspan="1" style="width:40%"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="item5">
			<div class="footer-item">
				<div class="penerima">

					<table style="width: 100%;">
						<tr>
							<?php if (!$isPrint): ?>
								<td style="width:40%; text-align:left">
									<h3>Barang Diterima dalam Kondisi Baik</h3>
									<h3>Segala kurasakan </h3>
								</td>
							<?php else:
								echo "<td></td>";
							endif; ?>
							<?php
							setlocale(LC_TIME, 'ID_id');
							$dtnow = time();
							$sjnTime = strftime('%d %B %Y', $dtnow);
							?>
							<td style="width:40%; text-align:left">Jakarta,
								<?= $sjnTime ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
						<tr>
							<?php if (!$isPrint): ?>
								<td style="width:40%; text-align:left">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PENERIMA
								</td>
								<td style="width:40%; text-align:right">
									Hormat
									Kami&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
							<?php else:
								echo "<td></td><td></td>";
							endif; ?>
						</tr>
						<tr>
							<?php if (!$isPrint): ?>
								<td style="width:40%; text-align:left;padding-top: 50px;">
									(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
								</td>
							<?php else:
								echo "<td></td>";
							endif; ?>
							<td style="width:40%; text-align:left;padding-top: 50px;">
								(Rico
								Fahlevi)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

</body>

</html>
<style>
	.thead {
		grid-area: thead;
		text-align: center;
	}

	.penerima {
		grid-area: terima;
	}

	.pengirim {
		grid-area: kirim;
	}

	.header {
		grid-area: header;
		text-align: center;
	}

	.left-item {
		grid-area: main;
		text-align: left;
	}

	.image {
		grid-area: right;
		text-align: center;
	}

	.table {
		grid-area: table;
	}

	p {
		margin: 5px 0px 5px 0px;
	}

	/* .table table tbody tr td */
	.table>table {
		/* border: 10px; */
		border-collapse: collapse;
		width: 100%;
		/* border: 1px solid; */
	}

	.table table tbody tr td {
		/* border: 10px; */
		/* border: 1px solid; */
	}

	.table table thead tr th {
		/* border: 10px; */
		/* border: 1px solid; */
	}

	.item5 {
		grid-area: footer;
	}

	.footer-item {
		display: grid;
		grid-template-areas:
			'terima terima terima terima'
		;
		gap: 10px;
		/* background-color: #2196F3; */
		padding: 20px;
	}

	.grid-container {
		display: grid;
		font-family: 'Calibri';
		font-style: normal;
		font-size: 12px;
		margin-right: 50px;
		letter-spacing: 1px;
		grid-template-areas:
			'header header header header header header'
			'main main main main main main'
			'thead thead thead thead thead thead'
			'table table table table table table'
			'footer footer footer footer footer footer';
		gap: 5px;
		/* background-color: #2196F3; */
		padding: 10px 10px 10px 10px;
	}

	.grid-container>div {
		/* background-color: rgba(255, 255, 255, 0.8); */
		/* text-align: center; */
		padding: 5px 0;
	}
</style>
