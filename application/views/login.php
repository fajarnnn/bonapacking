<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="./assets/img/bona.png">
	<title>
		Bona
	</title>
	<link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<!-- CSS Files -->
	<link id="pagestyle" href="<?= base_url() ?>assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
	<!-- Nepcha Analytics (nepcha.com) -->
	<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->	
</head>

<body class="">
	<main class="main-content  mt-0">
		<section>
			<div class="page-header min-vh-75">
				<div class="container">
					<div class="row">
						<div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
							<div class="card card-plain mt-8">
								<div class="card-header pb-0 text-left bg-transparent">
									<h3 class="font-weight-bolder text-info text-gradient">Bona Packing List
										Application</h3>
									<p class="mb-1">Enter your Username and password to sign in</p>
								</div>
								<div class="card-body m-0 pt-0 pb-1">
									<div class="alert alert-danger text-white font-weight-bold m-0 p-2" role="alert" id="alert">
										
									</div>
								</div>
								<div class="card-body pt-0">
									<form role="form" method="post" action="<?= base_url('login/dologin') ?>">
										<label>User</label>
										<div class="mb-3">
											<input type="text" name="username" class="form-control" placeholder="User"
												aria-label="Email" aria-describedby="email-addon">
										</div>
										<label>Password</label>
										<div class="mb-3">
											<input type="password" name="password" class="form-control"
												placeholder="Password" aria-label="Password"
												aria-describedby="password-addon">
										</div>
										<div class="form-check form-switch">
											<input class="form-check-input" type="checkbox" id="rememberMe" checked="">
											<label class="form-check-label" for="rememberMe">Remember me</label>
										</div>
										<div class="text-center">
											<button type="submit" class="btn bg-gradient-info w-100 mt-4 mb-0">Sign
												in</button>
										</div>
									</form>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n6">
								<div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6"
									style="background-image:url('<?= base_url() ?>/assets/img/login2.jpg')"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<!-- -------- START FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->

	<div class="row">
		<div class="col-8 mx-auto text-center mt-6">
			<p class="mb-0 text-secondary">
				Copyright ©
				<script>
					document.write(new Date().getFullYear())
				</script> Breath ID Technology
			</p>
		</div>
	</div>
	</div>
	</footer>
	<!-- -------- END FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
	<!--   Core JS Files   -->
	<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
	<!-- <script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script> -->
	<!-- <script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script> -->
	<!-- Github buttons -->
	<!-- <script async defer src="https://buttons.github.io/buttons.js"></script> -->
	<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
	<!-- <script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script> -->
</body>

</html>
<script>
	// assumes you're using jQuery
	$('#alert').hide();
	$(document).ready(function () {
		<?php if ($this->session->flashdata('msg')) { ?>
			$('#alert').html('<?php echo $this->session->flashdata('msg'); ?>').show();
		<?php }else {?>
			$('#alert').hide();
			<?php }?>
	});
</script>
