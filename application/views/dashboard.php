<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>
				<?php $roleid = $this->session->userdata('role');
				if ($roleid != 3):
					?>
					<div class="col-lg-12 px-3 text-end">
						<a type="button" href="<?= base_url('Packing/data_form') ?>"
							class="btn bg-gradient-success ms-auto col-lg-2 justify-content-end py-2 px-2">
							Add Packing List</a>
					</div>
				<?php endif; ?>

				<div class="card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="example3" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PL</th>

									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Region</th>

									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Truck Driver / Vessel Name</th>
									<?php if ($roleid != 3): ?>
										<th
											class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
											Driver</th>
									<?php endif; ?>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Detail
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
								setlocale(LC_TIME, 'ID_id');
								setlocale(LC_ALL, 'id_ID');
								$no = 0;
								foreach ($all as $key => $value) {
									$dateIn = date_create($value->created_at);
									$dateNow = date_create(date("Y-m-d"));
									$diff = date_diff($dateIn, $dateNow);
									$df = $diff->format("%a");
									$trs = "";
									$tds = "";
									if (intval($df) >= 30) {
										$trs = "background-color: #e25f59;";
										$tds = "color:white";
									}
									$no++;
									$shipping = strtoupper($value->method);
									$nopl = isset($value->no_pl) ? $value->no_pl : "-";
									$area = isset($value->area) ? $value->area : "-";
									$dt = base64Url_encode($nopl . "_BONAN_" . $area);
									try {
										$dateOut = strtotime($value->date_out);
										$SdateOut = strftime('%d %B %Y', $dateOut);
										if (strpos($SdateOut, '1970')) {
											$SdateOut = '-';
										}
									} catch (Exception $e) {
										$SdateOut = '-';
									}
									?>
									<tr class="text-center" style="<?= $trs ?>">

										<td style="<?=$tds?>"><?= $no ?></td>
										<td style="<?=$tds?>"><?= $value->no_pl ?></td>

										<td style="<?=$tds?>"><?= $value->area ?></td>
										<td style="<?=$tds?>"><?= $shipping ?></td>
										<?php if ($roleid != 3): ?>
											<td style="<?=$tds?>"><?= $value->name ?></td>
										<?php endif; ?>

										<td style="<?=$tds?>">
											<a href="<?= base_url('Packing/detail/' . $dt) ?>">
												<span class="btn bg-gradient-dark mb-0">Detail</span> </a>
											<?php if ($value->no_pl != "-" && $roleid != 3): ?>
												<button class="btn bg-gradient-warning mb-0"
													onclick="editPL('<?= $value->no_pl ?>', '<?= $value->area ?>')">EDIT
												</button>
											<?php endif; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
				</div>
				<?php if ($this->session->userdata('role') != 3): ?>
					<div class="col-lg-12 px-3 text-end">
						<a type="button" href="<?= base_url('FileGenerator/generateAllExcel') ?>"
							class="btn bg-gradient-warning ms-auto col-lg-2 justify-content-end py-2 px-2">
							Download All Data</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
</main>


<!-------- Modal ------------>
<!-- <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<section class="section dashboard">

						<form class="row g-3 needs-validation  pt-0 justify-content-center" novalidate action="submit"
							method="post" enctype="multipart/form-data">



							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">No PL</label>
								<input name="no_pl" type="text" class="form-control" id="validationTooltip04"
									placeholder="No PL">

							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Shipping Method</label>
								<select class="form-select" aria-label="Default select example" name='doc_type'>
									<option value="" disabled selected>Choose One</option>
									<option value="pdf">Truck</option>
									<option value="exce">Vessel</option>

								</select>
							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Name</label>
								<input name="name" type="text" class="form-control" id="validationTooltip04"
									placeholder="Driver Or Vessel">

							</div>
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Phone</label>
								<input name="phone" type="text" class="form-control" id="validationTooltip04"
									placeholder="Phone">

							</div>






							<div class="d-flex justify-content-end">

								<div class="col-md-0 position-relative">
									<input type="submit" name="submit" class="btn btn-primary">
								</div>
							</div>
						</form>
					</section>
				</div>
			</div>
		</div>
	</div> -->


<!-- Modal for editing data -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<section class="section dashboard">
					<form class="row g-3 needs-validation pt-0 justify-content-center" novalidate action="submit"
						method="post" enctype="multipart/form-data">
						<!-- Hidden field for storing the no_pl value -->
						<input type="hidden" name="no_pl_old" value="<?= $value->no_pl ?>">

						<div class="col-md-12 position-relative">
							<label for="validationTooltip01" class="form-label">No PL</label>
							<input name="no_pl" type="text" class="form-control" id="validationTooltip04"
								value="<?= $value->no_pl ?>" required>
							<div class="invalid-tooltip">
								Please enter a valid No PL.
							</div>
						</div>



						<div class="col-md-12 position-relative">
							<label for="validationTooltip01" class="form-label">Shipping Method</label>
							<select class="form-select" aria-label="Default select example" name='shipping_method'>
								<option value="" disabled selected>Choose One</option>
								<option value="truck" <?= ($value->shipping_method == 'truck') ? 'elected' : '' ?>>
									Truck</option>
								<option value="vessel" <?= ($value->shipping_method == 'vessel') ? 'elected' : '' ?>>
									Vessel</option>
							</select>
						</div>

						<div class="col-md-12 position-relative">
							<label for="validationTooltip01" class="form-label">Name</label>
							<input name="name" type="text" class="form-control" id="validationTooltip04"
								value="<?= $value->driver ?>" required>
							<div class="invalid-tooltip">
								Please enter a valid name.
							</div>
						</div>
						<div class="col-md-12 position-relative">
							<label for="validationTooltip01" class="form-label">Phone</label>
							<input name="phone" type="text" class="form-control" id="validationTooltip04"
								value="<?= $value->driver ?>" required>
							<div class="invalid-tooltip">
								Please enter a valid name.
							</div>
						</div>
					</form>
					<div class="d-flex justify-content-end">
						<div class="col-md-0 position-relative">
							<input type="submit" name="submit" class="btn btn-primary">
						</div>
					</div>

				</section>
			</div>
		</div>
	</div>
</div>


<div class="modal" id="insertPLModal">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit PL</h4>
				<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
			</div>
			<div class="modal-body">

				<div class="container-fluid py-4" id="pllist">

					<div class="row">

						<div class="col-6">
							<div class="col-md-12">
								<label>NO PL</label>
							</div>
							<div class="col-12 mb-1">
								<input name="bulkPL" type="text" class="form-control" id="bulkpl" placeholder="NO PL"
									value="" readonly>
							</div>
							<div class="col-md-12">
								<label>Shipping Method</label>
							</div>
							<div class="col-12 mb-1">

								<select class="form-select" aria-label="Default select example" name='shipping'
									id="shipping">
									<option value="" disabled selected>Shipping Method</option>
									<option value="Truck">Truck</option>
									<option value="Vessel">Vessel</option>

								</select>
							</div>
						</div>
						<div class="col-6">
							<div class="col-md-12">
								<label>Nama Pengangkut</label>
							</div>
							<div class="col-12 mb-1">
								<input name="shippingName" type="text" class="form-control" id="shippingName"
									placeholder="Driver / Vessel Name" value="">
								<input name="pl_id" type="hidden" class="form-control" id="pl_id">
							</div>
							<div class="col-md-12">
								<label>Nomor Kendaraan</label>
							</div>
							<div class="col-12 mb-1">

								<input name="noKendaraan" type="text" class="form-control" id="noKendaraan"
									placeholder="Plate / No Container" value="">
							</div>
							<div class="col-md-12">
								<label>Nomor Pengangkut</label>
							</div>
							<div class="col-12 mb-1">
								<input type="text" name="noPengangkut" rows="10" class="form-control" id="noPengangkut"
									placeholder="No Segel / Driver Phone"></input>
							</div>
							<div class="col-md-12">
								<label>Nomor Voyage</label>
							</div>
							<div class="col-12 mb-1">
								<input type="text" name="noVoyage" rows="10" class="form-control" id="noVoyage"
									placeholder="NO Voyage"></input>
							</div>
							<div class="col-12 mb-1">
								<label for="validationTooltip01" class="form-label">Date Out</label>
								<input name="date_out" type="date" class="form-control" id="date_out"
									placeholder="Date Out" value="">
							</div>
							<!-- <div class="col-12 mb-1">
								<input type="checkbox" id="isLot" name="isLot" value="false">
								<label for="validationTooltip01" class="form-label">Is LOT</label>

							</div> -->
							<div class="col-12 mb-1" style="display: none;" id="divTagihan">
								<label for="validationTooltip01" class="form-label">Tagihan</label>
								<input name="tagihan" type="text" class="form-control" id="tagihan"
									placeholder="Tagihan" value="">
							</div>

						</div>

						<div class="modal-footer mt-3">
							<button type="button" class="btn btn-success" data-bs-dismiss="modal"
								onclick="singlePl()">Submit</button>
							<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>

<script>
	var base_url = "<?php echo base_url(); ?>";
	var area = "";
	var lot = false;
	$("#isLot").change(function () {
		if (this.checked) {
			$("#divTagihan").css('display', 'block');
			lot = true;
		} else {
			$("#divTagihan").css('display', 'none');
			lot = false;
		}
	});

	function getPL(pl) {
		$.ajax({
			type: "POST",
			url: base_url + "Packing/checkPL",
			data: {
				no_pl: pl,
				area
			},
			cache: false,
			success: function (response) {
				if (response != "Not Exist") {
					console.log(response);
					// var json = jQuery.parseJSON(response);
					var dt = JSON.parse(response);
					$('#shipping').val(dt.method);
					$('#pl_id').val(dt.pl_id);
					$('#shippingName').val(dt.name);
					$('#noKendaraan').val(dt.no_vehicle);
					$('#noPengangkut').val(dt.no_shipping);
					$('#noVoyage').val(dt.no_voyage);
					var mydate = new Date(dt.date_out);
					if (dt.isLot == 0)
						lot = false;
					else
						lot = true;
					$('#isLot').prop('checked', lot);
					if (lot)
						$("#divTagihan").css('display', 'block');
					else
						$("#divTagihan").css('display', 'none');
					var date = moment(mydate).format('YYYY-MM-DD')
					if (!td.date_out.includes('1970'))
						$('#date_out').val(date);
					$('#tagihan').val(dt.tagihan);
				}
			}
		});
	}
	$('#bulkpl').change(function () {
		var no_pl = $(this).val();

		// alert(no_pl+"|"+area);
		getPL(no_pl, area)

	});

	function singlePl() {
		var shipping = $('#shipping').val();
		var noPL = $('#bulkpl').val();
		var shippingName = $('#shippingName').val();
		var noKendaraan = $('#noKendaraan').val();
		var noPengangkut = $('#noPengangkut').val();
		var noVoyage = $('#noVoyage').val();
		var date_out = $('#date_out').val();
		var date_out = $('#date_out').val();
		var tagihan = $('#tagihan').val();
		var pl_id = $('#pl_id').val();
		var data = {
			noPL: noPL,
			area: area,
			shipping: shipping,
			noKendaraan: noKendaraan,
			noPengangkut: noPengangkut,
			noVoyage: noVoyage,
			lot: lot,
			date_out: date_out,
			shippingName: shippingName,
			tagihan: tagihan,
			pl_id: pl_id,
		};
		// console.log(data);

		if (noPL.length < 1) {
			alert("Mohon isi NO PL d!");
		} else if (shipping == null) {
			alert("Mohon Pilih shipping method!");
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: base_url + "Packing/updateSinglePL",
						data: data,
						cache: false,
						success: function (response) {
							console.log(response);
							if (response == "Success") {
								Swal.fire("Saved!", "", "success").then(function () {
									location.reload();
								});
							} else {
								Swal.fire("Gagal Update Data!", "", "error").then(function () {
									location.reload();
								});
							}
						}
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	}
</script>
<script>
	new DataTable('#example3');
	$(document).ready(function () {
		// $(".js-basic-multiple").select2({
		// 	dropdownParent: $('#exampleModalCenter .modal-content')
		// });
	});

	function editPL(a, b) {
		$('#insertPLModal').modal('show');
		$('#bulkpl').val(a);
		area = b;
		getPL(a, area);
	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>
