<?php
setlocale(LC_ALL, 'id_IN');
$sdatein = "";
$sdateout = "";
$sunload = "";
$sdocreturn = "";
$sinvDate = "";
if (isset($details->date_in)) {
	// $formatter = new IntlDateFormatter("id_IN", IntlDateFormatter::SHORT, IntlDateFormatter::NONE);
	// $unixtime=$formatter->parse($details[0]->date_in);
	$datein = new DateTime($details->date_in);
	// $datein=new DateTime();
	// $datein->setTimestamp($unixtime);
	$sdatein = date_format($datein, "Y-m-d");
}
if (isset($pl[0]->date_out)) {
	$dateout = new DateTime($pl[0]->date_out);
	$sdateout = date_format($dateout, "Y-m-d");
}
if (isset($details->doc_return)) {
	$docreturn = new DateTime($details->doc_return);
	$sdocreturn = date_format($docreturn, "Y-m-d");
}
// var_dump($details);
if (isset($details->unloading_date) && $details->unloading_date != '0' && $details->unloading_date != '-' && $details->unloading_date != '') {
	$unload = new DateTime($details->unloading_date);
	$sunload = date_format($unload, "Y-m-d");
}
if (isset($pl[0]->invoice_date) && $pl[0]->invoice_date != '0' && $pl[0]->invoice_date != '-' && $pl[0]->invoice_date != '') {
	$unload = new DateTime($pl[0]->invoice_date);
	$sinvDate = date_format($unload, "Y-m-d");
}
// var_dump($stime);
?>
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<form action="<?= base_url('packing/data_edit') ?>" method="post" enctype="multipart/form-data" id="add-form">
		<!-- End Navbar -->
		<div class="container-fluid py-4">
			<div class="col-md-12">
				<div class="card mt-4">
					<div class="card-body p-3">
						<div class="row">
							<div class="col-md-6 mb-md-0 mb-4">
								<?php $dt = $pl[0] ?>
								<label for="validationTooltip01" class="form-label">No PL</label>
								<input name="no_pl" type="text" class="form-control" id="no_pl" placeholder="No PL"
									value="<?= isset($dt->no_pl) ? $dt->no_pl : "-" ?>">

							</div>
							<div class="col-md-6">
								<label for="validationTooltip01" class="form-label">Area</label>
								<input name="area" type="text" class="form-control" id="area" placeholder="Area"
									value="<?= isset($dt->area) ? $dt->area : "-" ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 mt-4">
					<div class="card">
						<div class="card-header pb-0 px-3">
							<h6 class="mb-0">PO Information</h6>
						</div>
						<div class="card-body pt-4 p-3">
							<ul class="list-group">
								<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
									<div class="d-flex flex-column">
										<input name="idpo" type="hidden" class="form-control" placeholder="idpo"
											value="<?= $details->id_po ?>">
										<input name="lastpl" type="hidden" class="form-control" placeholder="idpo"
											value="<?= $dt->pl_id ?>">
										<input name="no_po" type="text" class="form-control" id="validationTooltip04"
											placeholder="No PO"
											value="<?= isset($details->no_po) ? $details->no_po : "-" ?>">
										<br>
										<input name="no_sji" type="text" class="form-control" id="validationTooltip04"
											placeholder="No SJI / SJN"
											value="<?= isset($details->no_sji) ? $details->no_sji : "-" ?>">
										<br>
										<input name="supplier" type="text" class="form-control" id="validationTooltip04"
											placeholder="Supplier"
											value="<?= isset($details->supplier) ? $details->supplier : "" ?>">


										<br>
										<label for="validationTooltip01" class="form-label">Date In</label>
										<input name="date_in" type="date" class="form-control" id="validationTooltip04"
											placeholder="Date In" value="<?= $sdatein ?>">
										<!-- <br>
											<label for="validationTooltip01" class="form-label">Date Out</label>
											<input name="date_out" type="date" class="form-control"
												id="validationTooltip04" placeholder="Date Out" value="<?= $sdateout ?>"
												readonly>

											<br>
											<label for="validationTooltip01" class="form-label">Unload Document</label>
											<input name="unload_date" type="date" class="form-control" id="unload_date"
												placeholder="Unload Date" value="<?= $sunload ?>">
											<br>
											<label for="doc_return" class="form-label">Doc Return</label>
											<input name="doc_return" type="date" class="form-control" id="doc_return"
												placeholder="Document Return" value="<?= $sdocreturn ?>"> -->




									</div>
									<div class="ms-auto">

										<label for="validationTooltip01" class="form-label">Bill</label>

										<br>

										<input name="tagihan" type="text" class="form-control" id="tagihan"
											placeholder="tagihan"
											value="<?= isset($dt->tagihan) ? $dt->tagihan : "0" ?>" readonly>
										<br>

										<input name="remark" type="text" class="form-control" id="validationTooltip04"
											placeholder="remark"
											value="<?= isset($details->remark) ? $details->remark : "" ?>">
										<!-- <br>
										<input type="checkbox" id="isLot" name="isLot" value="false">
										<label for="validationTooltip01" class="form-label">Is LOT</label> -->
										<br>
										<!-- <label for="validationTooltip01" class="form-label">Invoice Date</label>
											<input name="inv_date" type="date" class="form-control"
												id="validationTooltip04" placeholder="invoice date"
												value="<?= $sinvDate ?>">
											<br>
											<input name="inv_no" type="text" class="form-control"
												id="validationTooltip04" placeholder="Invoice Number"
												value="<?= $details->no_invoice ?>">
											<br>
											<input name="no_pot" type="text" class="form-control"
												id="validationTooltip04" placeholder="No POT"
												value="<?= $details->no_pot ?>">
											<br> -->
										<!-- <div class="image-preview-container">
												<div class="preview">
													<img id="preview-selected-image" width="20%" height="20%" />
												</div>
												<label for="file-upload">Upload Image</label>
												<input type="file" id="file-upload" accept="image/*"
													onchange="previewImage(event);" name="image" />
											</div> -->


									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6 mt-4">
					<div class="card">
						<div class="card-header pb-0 px-3">
							<h6 class="mb-0">Reciever & Shipping Information</h6>
						</div>
						<div class="card-body pt-4 p-3">
							<ul class="list-group">
								<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
									<div class="d-flex flex-column">
										<input name="corporate" type="text" class="form-control"
											id="validationTooltip04" placeholder="Corporate Name"
											value="<?= isset($details->corporate) ? $details->corporate : "" ?>">
										<br>
										<input name="address" type="text" class="form-control" id="validationTooltip04"
											placeholder="Address"
											value="<?= isset($details->address) ? $details->address : "" ?>">
										<br>
										<input name="street" type="text" class="form-control" id="validationTooltip04"
											placeholder="Street"
											value="<?= isset($details->street) ? $details->street : "" ?>">
										<br>
										<input name="plant" type="text" class="form-control" id="validationTooltip04"
											placeholder="Plant"
											value="<?= isset($details->plant) ? $details->plant : "" ?>">
										<br>
										<input name="pic" type="text" class="form-control" id="validationTooltip04"
											placeholder="Name"
											value="<?= isset($details->pic) ? trim(isset(explode("/", $details->pic)[0]) ? explode("/", $details->pic)[0] : "-") : "" ?>">
										<br>
										<input name="phone" type="text" class="form-control" id="validationTooltip04"
											placeholder="Phone"
											value="<?= isset($details->pic) ? trim(isset(explode("/", $details->pic)[1]) ? explode("/", $details->pic)[1] : "-") : "" ?>">




									</div>
									<div class="ms-auto">
										<select class="form-select" aria-label="Default select example" name='shipping'
											id='shipping'>
											<?php if ($dt->method == "Truck"):
											?>
												<option value="" disabled>Shipping Method</option>
												<option value="Truck" selected>Truck</option>
												<option value="Vessel">Vessel</option>
											<?php elseif ($dt->method == "Vessel"):
											?>
												<option value="" disabled>Shipping Method</option>
												<option value="Truck">Truck</option>
												<option value="Vessel" selected>Vessel</option>
											<?php else:
											?>
												<option value="" selected>Shipping Method</option>
												<option value="Truck">Truck</option>
												<option value="Vessel">Vessel</option>
											<?php endif; ?>
										</select>
										<br>

										<input name="vessel_name" type="text" class="form-control" id="vessel_name"
											placeholder="Driver Name / Vessel Name" value="<?= $dt->name ?>">
										<br>
										<input name="plate" type="text" class="form-control" id="plate"
											placeholder="Plate" value="<?= $dt->no_vehicle ?>">
										<br>
										<input name="driverPhone" type="text" class="form-control" id="driverPhone"
											placeholder="Phone Number" value="<?= $dt->no_shipping ?>">
										<br>
										<input name="list_group" type="text" class="form-control" id="list_group"
											placeholder="Group"
											value="<?= isset($details->list_group) ? $details->list_group : "" ?>">
										<br>
										<input name="no_voyage" type="text" class="form-control" id="no_voyage"
											placeholder="No Voyage"
											value="<?= isset($dt->no_voyage) ? $dt->no_voyage : "" ?>">

									</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
		</div>
		<div class="container-fluid">
			<div class="card mt-4">
				<div class="card-header pb-0 p-3">
					<div class="row">
						<div class="col-6 d-flex align-items-center">
							<h6 class="mb-0">Material Informations</h6>
						</div>

					</div>
				</div>
				<div class="card-body p-3" id="matGrup">

					<div class="row mb-3">
						<div class="col-md-2 mb-md-0 mb-4">
							<label>Material</label>
						</div>
						<div class="col-md-2 mb-md-0 mb-4">
							<div class="row">
								<div class="col-4 p-1">
									<label>Panjang</label>
								</div>
								<div class="col-4 p-1">
									<label>Lebar</label>
								</div>
								<div class="col-4 p-1">
									<label>Tinggi</label>
								</div>
							</div>
						</div>

						<div class="col-md-2 mb-md-0 mb-4">
							<div class="row">
								<div class="col-7 p-1">
									<label>Kubikasi</label>
								</div>
								<div class="col-5 p-1">
									<label>Berat</label>
								</div>
							</div>
						</div>
						<div class="col-md-2 mb-md-0 mb-4">
							<div class="row">
								<div class="col-5 p-1">
									<label>QTY</label>
								</div>
								<div class="col-7 p-1">
									<label>Satuan</label>
								</div>
							</div>
						</div>
						<div class="col-md-2 mb-md-0 mb-4">
							<div class="row">
								<div class="col-3 p-1">
									<label>Tonase</label>
								</div>
								<div class="col-5 p-1">
									<label>Cost</label>
								</div>
								<div class="col-4 p-1">
									<label>Cost Bill</label>
								</div>
							</div>
						</div>

						<div class="col-md-1 mb-md-0 mb-4">
							<label>Total Cost</label>
						</div>

					</div>
					<?php $no = 1;
					foreach ($material as $key => $value): ?>
						<input name="ids[]" type="hidden" class="form-control" placeholder="Material"
							value="<?= $value->id ?>">
						<div class="row mb-3" id='matgroup<?= $no ?>'>
							<div class="col-md-2 mb-md-0 mb-4">
								<input name="material[]" type="text" class="form-control" placeholder="Material"
									value="<?= $value->material ?>">
							</div>
							<div class="col-md-2 mb-md-0 mb-4">
								<div class="row">
									<div class="col-4 p-1">
										<input name="panjang[]" type="text" class="form-control" id="p<?= $no ?>"
											placeholder="P" onchange="calculate(<?= $no ?>)" value="<?= $value->panjang ?>">
									</div>
									<div class="col-4 p-1">
										<input name="lebar[]" type="text" class="form-control" id="l<?= $no ?>"
											placeholder="L" onchange="calculate(<?= $no ?>)" value="<?= $value->lebar ?>">
									</div>
									<div class="col-4 p-1">
										<input name="tinggi[]" type="text" class="form-control" id="t<?= $no ?>"
											placeholder="T" onchange="calculate(<?= $no ?>)" value="<?= $value->tinggi ?>">
									</div>
								</div>
							</div>
							<div class="col-md-2 mb-md-0 mb-4">
								<div class="row">
									<div class="col-7 p-1">
										<input name="kubikasi[]" type="text" class="form-control" id="k<?= $no ?>"
											placeholder="kubikasi m3" readonly>
									</div>

									<div class="col-5 p-1">
										<input name="berat[]" type="text" class="form-control" id="b<?= $no ?>"
											placeholder="Kg" onchange="calculateTagihan()" value="<?= $value->berat ?>">
									</div>
								</div>
							</div>
							<div class="col-md-2 mb-md-0 mb-4">
								<div class="row">
									<div class="col-7 p-1">
										<input name="satuan[]" type="text" class="form-control" id="validationTooltip04"
											placeholder="satuan" value="<?= $value->satuan ?>">
									</div>
									<div class="col-5 p-1">
										<input name="qty[]" type="text" class="form-control" id="q<?= $no ?>"
											placeholder="Qty" value="<?= $value->qty ?>" onchange="calculate(<?= $no ?>)">
									</div>
									<div class="col-7 p-1">
										<input name="satuan[]" type="text" class="form-control" id="validationTooltip04"
											placeholder="satuan" value="<?= $value->satuan ?>">
									</div>
									<div class="col-7 p-1">
										<input name="satuan[]" type="text" class="form-control" id="validationTooltip04"
											placeholder="satuan" value="<?= $value->satuan ?>">
									</div>
								</div>
							</div>



							<div class="col-md-2 mb-md-0 mb-4">
								<div class="row">
									<div class="col-3 p-1">
										<input name="ton[]" type="text" class="form-control" id="ton<?= $no ?>"
											placeholder="Tonase" value="" readonly>
									</div>
									<div class="col-5 p-1">
										<input name="cost[]" type="text" class="form-control" id="cost<?= $no ?>"
											placeholder="Cost" value="<?= $value->cost ?>" onchange="calculate(<?= $no ?>)"
											onfocus="this.value=''">
									</div>
									<div class="col-4 p-1">
										<select class="form-select" aria-label="Default select example" name='costbill[]'
											id='costbill<?= $no ?>' onchange="calculate(<?= $no ?>)" style="float: left;">
											<?php if ($value->costbill == 0): ?>
												<option value="0" selected>Default</option>
												<option value="1">Ton</option>
												<option value="2">Kg</option>
												<option value="3">Kubik</option>
												<option value="4">Unit</option>
												<option value="5">Lot</option>
											<?php elseif ($value->costbill == 1): ?>
												<option value="0">Default</option>
												<option value="1" selected>Ton</option>
												<option value="2">Kg</option>
												<option value="3">Kubik</option>
												<option value="4">Unit</option>
												<option value="5">Lot</option>
											<?php elseif ($value->costbill == 2): ?>
												<option value="0">Default</option>
												<option value="1">Ton</option>
												<option value="2" selected>Kg</option>
												<option value="3">Kubik</option>
												<option value="4">Unit</option>
												<option value="5">Lot</option>
											<?php elseif ($value->costbill == 3): ?>
												<option value="0">Default</option>
												<option value="1">Ton</option>
												<option value="2">Kg</option>
												<option value="3" selected>Kubik</option>
												<option value="4">Unit</option>
												<option value="5">Lot</option>
											<?php elseif ($value->costbill == 4): ?>
												<option value="0">Default</option>
												<option value="1">Ton</option>
												<option value="2">Kg</option>
												<option value="3">Kubik</option>
												<option value="4" selected>Unit</option>
												<option value="5">Lot</option>
											<?php else: ?>
												<option value="0">Default</option>
												<option value="1">Ton</option>
												<option value="2">Kg</option>
												<option value="3">Kubik</option>
												<option value="4">Unit</option>
												<option value="5" selected>Lot</option>
											<?php endif; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-1 mb-md-0 mb-4">
								<input name="totcost[]" type="text" class="form-control" placeholder="Total Cost"
									id="totcost<?= $no ?>" readonly>
							</div>
							<?php if ($no == 1): ?>
								<div class="col-md-1">
									<a class="btn bg-gradient-dark mb-0" href="javascript:addMat();"><i class="fas fa-plus"
											aria-hidden="true"></i>&nbsp;&nbsp;</a>
								</div>
							<?php else: ?>
								<div class="col-md-1">
									<a class="btn btn-danger mb-0"
										href="javascript:removeMatDB(<?= $no ?>,<?= $value->id ?>);"><i class="fas fa-minus"
											aria-hidden="true"></i>&nbsp;&nbsp;</a>
								</div>
							<?php endif;
							$no++; ?>
						</div>

					<?php endforeach; ?>
				</div>
			</div>


		</div>

		<div class="row">
			<div class="col-12 text-center py-4">
				<input class="btn bg-gradient-success mb-0" type="submit" name="Add Data" id='add'></input>
			</div>
		</div>
	</form>



	<footer class="footer pt-3  ">
		<div class="container-fluid">
			<div class="row align-items-center justify-content-lg-between">
				<div class="col-lg-6 mb-lg-0 mb-4">
					<div class="copyright text-center text-sm text-muted text-lg-start">
						©
						<script>
							document.write(new Date().getFullYear())
						</script>,
						made with <i class="fa fa-heart"></i> by
						<a href="https://www.breathid.tech" class="font-weight-bold">Breath Tech
							Ind</a>
						for a better applications.
					</div>
				</div>

			</div>
		</div>
	</footer>
	</div>
</main>
<div class="modal fade" id="waitingModal" tabindex="-1" role="dialog" aria-labelledby="waitingModalLabel"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title in" id="waitingModalLabel">Please Wait</h4>
				<h4 class="modal-title hide" id="waitingModalLabel">Complete</h4>
			</div>
			<div class="modal-body center-block">
				<div class="progress">
					<div class="progress-bar bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
						aria-valuemax="100">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default hide" data-dismiss="modal" id="btnClose">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>assets/js/util.js"></script>
<script src="<?= base_url() ?>assets/js/material.js"></script>
<script>
	var win = navigator.platform.indexOf('Win') > -1;
	if (win && document.querySelector('#sidenav-scrollbar')) {
		var options = {
			damping: '0.5'
		}
		Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>
<script>
	var mat = <?= count($material) ?>;
	var base_url = "<?php echo base_url(); ?>";

	$(document).ready(function() {
		var lotdt = <?= $pl[0]->isLot ?>;

		var lot = false;
		if (lotdt == 1) {
			lot = true;
			$('#isLot').prop('checked', lot);
			if (lot)
				$("#tagihan").attr("readonly", false);
			else
				$("#tagihan").attr("readonly", true);
			changeLot(lot);
		} else {
			lot = false;
			$('#isLot').prop('checked', lot);
			if (lot)
				$("#tagihan").attr("readonly", false);
			else
				$("#tagihan").attr("readonly", true);

			changeLot(lot);
		}
		var nos = parseInt('<?= count($material) ?>');
		// alert("lot:" + lot);
		// alert("mat:" + mat);
		mat = nos;
		var a = $('#tagihan').val();
		a = curToNum(a.toString());
		a = formatMoney(a);
		$('#tagihan').val(a);
		for (let i = 1; i <= nos; i++) {
			calculate(i);
			// console.log("NOS: "+nos);
		}

	});

	function removeMatDB(id, idmat) {

		Swal.fire({
			title: "Apakah anda akan menghapus data ini?",
			// showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak"
		}).then((result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				$.ajax({
					type: "POST",
					url: base_url + "Packing/deleteMaterial",
					data: {
						idmat: idmat
					},
					cache: false,
					success: function(response) {
						console.log(response);
						if (response == 'success') {
							removeMat(id);
						} else {
							Swal.fire("Gagal Delete Data!", "", "error");
						}
					}
				});
			} else {
				Swal.fire("Silahkan Periksa data kembali", "", "info");
			}
		});
	}

	// function formatMoney(number) {
	// 	return parseInt(number).toLocaleString('in-ID', {
	// 		style: "currency",
	// 		currency: "IDR",
	// 		minimumFractionDigits: 0,
	// 		maximumFractionDigits: 0
	// 	});

	// }

	function curToNum(currency) {
		var num = currency.replaceAll('.', '');

		return Number(num.replace(/[^0-9.-]+/g, ""));
	}


	function addMat(params) {
		mat = mat + 1;
		var output = $('#matGrup');


		$.ajax({
			type: "POST",
			url: base_url + "Packing/loadMaterial",
			data: {
				noMat: mat
			},
			cache: false,
			success: function(response) {
				$(output).last().append(response);

			}
		});
	}
	$('#add').on('click', function(e) {
		e.preventDefault();
		var form = $(this).parents('form');
		if ($('#area').val().length < 1) {
			alert('Area Kosong');
			return false;
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					Swal.fire("Saved!", "", "success").then(function() {
						form.submit();
						$('#waitingModal').modal('show');
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	});

	function formatMoney(number) {
		return parseInt(number).toLocaleString('in-ID', {
			style: "currency",
			currency: "IDR",
			minimumFractionDigits: 0,
			maximumFractionDigits: 0
		});

	}
	jQuery(document).ready(function() {
		ImgUpload();
	});

	function ImgUpload() {
		var imgWrap = "";
		var imgArray = [];

		$('.upload__inputfile').each(function() {
			$(this).on('change', function(e) {
				imgWrap = $(this).closest('.upload__box').find('.upload__img-wrap');
				var maxLength = $(this).attr('data-max_length');

				var files = e.target.files;
				var filesArr = Array.prototype.slice.call(files);
				var iterator = 0;
				filesArr.forEach(function(f, index) {

					if (!f.type.match('image.*')) {
						return;
					}

					if (imgArray.length > maxLength) {
						return false
					} else {
						var len = 0;
						for (var i = 0; i < imgArray.length; i++) {
							if (imgArray[i] !== undefined) {
								len++;
							}
						}
						if (len > maxLength) {
							return false;
						} else {
							imgArray.push(f);

							var reader = new FileReader();
							reader.onload = function(e) {
								var html =
									"<div class='upload__img-box'><div style='background-image: url(" +
									e.target.result + ")' data-number='" + $(
										".upload__img-close").length + "' data-file='" + f
									.name +
									"' class='img-bg'><div class='upload__img-close'>x</div></div></div>";
								imgWrap.append(html);
								iterator++;
							}
							reader.readAsDataURL(f);
						}
					}
				});
			});
		});

		$('body').on('click', ".upload__img-close", function(e) {
			var file = $(this).parent().data("file");
			// console.log(file);
			let result = file.includes("exist");
			if (result == true) {
				Swal.fire({
					title: "Apakah anda akan menghapus file ini?",
					// showDenyButton: true,
					showCancelButton: true,
					confirmButtonText: "Submit",
					cancelButtonText: "Tunggu"
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {
						$.ajax({
							type: "POST",
							url: base_url + "Packing/deleteimage",
							data: {
								id: file
							},
							cache: false,
							success: function(response) {
								if (response == 'success') {
									Swal.fire("Deleted!", "", "success").then(function() {
										$("#" + file).remove();
									});

								} else {
									Swal.fire("Delete Failed!", "", "error").then(function() {

									});
								}

							}
						});
					} else {
						Swal.fire("Silahkan Periksa data kembali", "", "info");
					}
				});
			} else {
				for (var i = 0; i < imgArray.length; i++) {
					if (imgArray[i].name === file) {
						imgArray.splice(i, 1);
						break;
					}
				}
				$(this).parent().parent().remove();
			}
		});
	}
	$('#no_pl').change(function() {
		var no_pl = $(this).val();
		var area = $('#area').val();
		// alert(no_pl+"|"+area);
		getPL(no_pl, area);

	});
	$('#area').change(function() {
		var no_pl = $('#no_pl').val();
		var area = $(this).val();
		// alert(no_pl+"|"+area);
		getPL(no_pl, area);

	});
	$("#tagihan").change(function() {
		var a = $(this).val();
		a = curToNum(a.toString());
		a = formatMoney(a);
		$(this).val(a);
	});

	$("#isLot").change(function() {
		if (this.checked) {
			$("#tagihan").attr("readonly", false);
			lot = true;
			$(this).val('true');
		} else {
			$("#tagihan").attr("readonly", true);
			lot = false;
			$(this).val('false');

		}
		changeLot(lot);
		calculateTagihan();
	});

	function getPL(pl, area) {
		$.ajax({
			type: "POST",
			url: base_url + "Packing/checkPL",
			data: {
				no_pl: pl,
				area
			},
			cache: false,
			success: function(response) {
				if (response != "Not Exist") {
					console.log(response);
					// var json = jQuery.parseJSON(response);
					var dt = JSON.parse(response);
					$('#shipping').val(dt.method);
					$('#pl_id').val(dt.pl_id);
					$('#vessel_name').val(dt.name);
					$('#plate_').val(dt.no_vehicle);
					$('#driver_phone').val(dt.no_shipping);
					$('#no_voyage').val(dt.no_voyage);
					var mydate = new Date(dt.date_out);
					if (dt.isLot == 0)
						lot = false;
					else
						lot = true;
					$('#isLot').prop('checked', lot);
					if (lot)
						$("#tagihan").attr("readonly", false);
					else
						$("#tagihan").attr("readonly", true);
					var date = moment(mydate).format('YYYY-MM-DD')
					$('#date_out').val(date);
					$('#tagihan').val(dt.tagihan);
					changeLot(lot);
				}
			}
		});
	}
	$('#waitingModal').on('shown.bs.modal', function() {

		var progress = setInterval(function() {
			var $bar = $('.bar');

			if ($bar.width() == 450) {

				// complete!

				// reset progree bar
				clearInterval(progress);
				// $('.progress').removeClass('active');
				// $bar.width(0);

				// // update modal 
				// $('#myModal .modal-body').html('Task complete. You can now close the modal.');
				// $('#myModal .hide,#myModal .in').toggleClass('hide in');

				// // re-enable modal allowing close
				// $('#myModal').data('reenable', true);
				// $('#myModal').modal('hide');
			} else {

				// perform processing logic (ajax) here
				$bar.width($bar.width() + 90);
			}

			$bar.text($bar.width() / 5 + "%");
		}, 600);

	});
</script>


</html>
