<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>

				<div class=" card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="example3" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PL
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PO
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No SJN</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No Invoice
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Date In</th>

									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Date Out</th>

									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Invoice Date
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Payment Date
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1;
								foreach ($arch as $key => $archive): 
								
								$pl = $archive->no_pl;
								$area = rawurlencode($archive->area);
								$sji = base64url_encode($archive->no_sji);
								$inv = base64url_encode($archive->no_invoice);
								?>
									<tr class="text-center">
										<td><?= $no ?></td>
										<td><?= $archive->no_pl ?></td>
										<td><?= $archive->no_po ?></td>
										<td><?= $archive->no_sji ?></td>
										<td><?= $archive->no_invoice ?></td>
										<td><?= $archive->date_in ?></td>
										<td><?= $archive->date_out ?></td>
										<td><?= $archive->invoice_date ?></td>
										<td><?= $archive->payment_date ?></td>
										<td>



											<button class="btn bg-gradient-dark mb-0">Detail
											</button>


											<button class="btn bg-gradient-success dropdown-toggle" type="button"
												data-bs-toggle="dropdown" aria-expanded="false" style="margin-bottom: 0px;">
												Download
											</button>
											<ul class=" dropdown-menu">
												<li><a class="dropdown-item"
														href="<?= base_url() ?>FileGenerator/generatePDF/<?= $pl . "/" . $area ?>"><i
															class="fa fa-list m-1" aria-hidden="true"></i>
														PL</a></li>
												<li><a class="dropdown-item" href="<?= base_url('FileGenerator/printSjn/' . $sji . '/print') ?>"><i class="fa fa-file-text m-1"
															aria-hidden="true"></i>
														SJN</a></li>
												<li><a class="dropdown-item" href="<?=base_url('FileGenerator/printInv/'.$inv)?>"><i class="fa fa-file-pdf m-1"
															aria-hidden="true"></i>
														Kwitansi</a></li>
												<li><a class="dropdown-item" href="<?=base_url('FileGenerator/generateLampiran/'.$inv)?>"><i class="fa fa-list-alt m-1"
															aria-hidden="true"></i>
														Invoice</a></li>
												<li><a class="dropdown-item" href="<?= base_url() ?>Packing/generateExcel/<?= $pl . "/" . $area ?>"><i class="fa fa-file-excel m-1"
															aria-hidden="true"></i>
														Excel Data</a></li>


											</ul>


										</td>

									</tr>
									<?php $no++; endforeach; ?>

							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>
	</div>
</main>


<div class="modal" id="myModal">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">PO & Receveiver Informations</h4>
				<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid py-4" id="poDetails">
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<!-- ------ Modal ------------>
<div class="modal fade" id="modalDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Add Invoice Informations</h5>
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<section class="section dashboard">
					<div class="col-md-12 position-relative">
						<div class="row">

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Nama</label>
								<input name="nama" type="text" class="form-control" id="nama" placeholder="Nama"
									readonly>
							</div>
						</div>
					</div>
					<div class="col-md-12 position-relative">
						<div class="row">
							<div class="col-md-6 position-relative">
								<label for="validationTooltip01" class="form-label">Invoice Date</label>
								<input name="invoice_date" type="text" class="form-control" id="invoice_date"
									placeholder="Invoice Date" readonly>
							</div>
							<div class="col-md-6 position-relative">
								<label for="validationTooltip01" class="form-label">Invoice Number</label>
								<input name="invoice_number" type="text" class="form-control" id="invoice_number"
									placeholder="Invoice Number" value="" readonly>
							</div>
						</div>
					</div>
					<div class="col-md-12 position-relative">
						<div class="col-md-6 position-relative">
							<label for="validationTooltip01" class="form-label">Payment Date</label>
							<input name="payment_date" type="date" class="form-control" id="payment_date"
								placeholder="Payment Date" value="">
						</div>
					</div>
					<div class="col-md-0 position-relative mt-4">
						<button class="btn btn-primary" onclick="updatedoc()">SUBMIT</button>
					</div>
			</div>
			</section>
		</div>
	</div>
</div>
</div>


<!-- Modal for editing data -->





<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>
<script src="<?= base_url() ?>assets/js/42d5adcbca.js" crossorigin="anonymous"></script>


<script>
	var base_url = "<?php echo base_url(); ?>";
	var area = "";
</script>
<script>
	new DataTable('#example3');
	$(document).ready(function () {
		// $(".js-basic-multiple").select2({
		// 	dropdownParent: $('#exampleModalCenter .modal-content')

		// });
	});
	var idpo = 0;

	function editDoc(a) {
		var jsn = JSON.parse(a);
		console.log(jsn);
		idpo = jsn.id;

		$('#invoice_number').val(jsn.no_invoice);
		$('#nama').val(jsn.invoice_name);

		// var dt = Date.parse(jsn.invoice_date);

		$('#invoice_date').val(jsn.invoice_date);
		// console.log(a);
		// var jsn = JSON.parse(a);
		// console.log(jsn.id);
		// idpo = jsn.id;
		// var mydate = new Date(jsn.date_in);
		// var date_in = moment(mydate).format('YYYY-MM-DD');
		// $('#no_po').val(jsn.no_po);
		// $('#no_sji').val(jsn.no_sji);
		$('#modalDoc').modal('show');
		// getPL(a, area);

	}

	function getPO(a) {
		var output = $('#poDetails');


		$.ajax({
			type: "POST",
			url: base_url + "/Packing/poDetails/",
			data: {
				po: a,
			},
			dataType: "text",
			cache: false,
			success: function (response) {
				$(output).html(response);
				// alert(response);
				$('#myModal').modal('show');
			}
		});
	}

	function rejectPO(a) {
		var output = $('#poDetails');
		Swal.fire({
			title: "Apakah anda akan Mereject data ini?",
			// showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak"
		}).then((result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				$.ajax({
					type: "POST",
					url: base_url + "/Packing/rejectInvoices",
					data: {
						po: a,
					},
					dataType: "text",
					cache: false,
					success: function (response) {
						console.log(response);
						if (response == "success") {
							Swal.fire("Saved!", "", "success").then(function () {
								location.reload();
							});
						} else {
							Swal.fire("Gagal Update Data!", "", "error").then(function () {
								location.reload();
							});
						}
					}
				});
			} else {
				Swal.fire("Silahkan Periksa data kembali", "", "info");
			}
		});


	}

	function updatedoc() {
		var paydate = $('#payment_date').val();
		var invoice_number = $('#invoice_number').val();
		if (paydate.length < 1) {
			alert("Mohon isi Invoice Date!");
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: base_url + "Packing/updatePayment",
						data: {
							id: idpo,
							payment_date: paydate,
							invoice_number: invoice_number
						},
						cache: false,
						success: function (response) {
							console.log(response);
							if (response == "success") {
								Swal.fire("Saved!", "", "success").then(function () {
									location.reload();
								});
							} else {
								Swal.fire("Gagal Update Data!", "", "error").then(function () {
									location.reload();
								});
							}
						}
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>
