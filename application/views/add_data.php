<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->

    <form action="<?= base_url('packing/data_add') ?>" method="post" enctype="multipart/form-data" id="add-form">
        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="col-md-12">
                <div class="card mt-4">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-md-6 mb-md-0 mb-4">
                                <label for="validationTooltip01" class="form-label">No PL</label>
                                <input name="no_pl" type="text" class="form-control" id="no_pl" placeholder="No PL"
                                    value="<?= $last ?>">

                            </div>
                            <div class="col-md-6">
                                <label for="validationTooltip01" class="form-label">Area</label>
                                <input name="area" type="text" class="form-control" id="area" placeholder="Area"
                                    value="<?= set_value('area') ?>" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 mt-4">
                    <div class="card">
                        <div class="card-header pb-0 px-3">
                            <h6 class="mb-0">PO Information</h6>
                        </div>
                        <div class="card-body pt-4 p-3">
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                    <div class="d-flex flex-column">

                                        <input name="no_po" type="text" class="form-control" id="no_po"
                                            placeholder="No PO" value="">
                                        <br>
                                        <input name="no_sji" type="text" class="form-control" id="no_sji"
                                            placeholder="No SJI / SJN" value="">
                                        <br>
                                        <input name="supplier" type="text" class="form-control" id="supplier"
                                            placeholder="Supplier" value="">


                                        <br>
                                        <label for="date_in" class="form-label">Date In</label>
                                        <input name="date_in" type="date" class="form-control" id="date_in"
                                            placeholder="Date In" value="<?= set_value('date_out') ?>">
                                        <br>
                                        <!-- <label for="date_out" class="form-label">Date Out</label>
											<input name="date_out" type="date" class="form-control"
												id="date_out" placeholder="Date Out"
												value=""> -->

                                        <!-- <br>
											<label for="validationTooltip01" class="form-label">Unload Document</label>
											<input name="unload_date" type="date" class="form-control" id="unload_date"
												placeholder="Unload Date" value="">
											<br>
											<label for="validationTooltip01" class="form-label">Doc Return</label>
											<input name="doc_return" type="date" class="form-control"
												id="validationTooltip04" placeholder="Document Return"
												value="<?= set_value('doc_return') ?>"> -->




                                    </div>
                                    <div class="ms-auto">

                                        <label for="validationTooltip01" class="form-label">Bill</label>

                                        <br>

                                        <input name="tagihan" type="text" class="form-control" id="tagihan"
                                            placeholder="tagihan" value="" readonly>
                                        <br>
                                        <input name="remark" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="remark" value="<?= set_value('remark') ?>">
                                        <br>
                                        <input name="list_group" type="text" class="form-control" id="list_group"
                                            placeholder="Group" value="">
                                        <br>
                                        <input type="checkbox" id="isLot" name="isLot" value="false"
                                            style="display: none;">

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="card">
                        <div class="card-header pb-0 px-3">
                            <h6 class="mb-0">Reciever & Shipping Information</h6>
                        </div>
                        <div class="card-body pt-4 p-3">
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                    <div class="d-flex flex-column">
                                        <input name="corporate" type="text" class="form-control"
                                            id="validationTooltip04" placeholder="Corporate Name" value="">
                                        <br>
                                        <input name="address" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="Address" value="">
                                        <br>
                                        <input name="street" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="Street" value="">
                                        <br>
                                        <input name="plant" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="Plant" value="">
                                        <br>
                                        <label for="validationTooltip01" class="form-label">PIC</label>
                                        <input name="pic" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="Name" value="">
                                        <br>
                                        <input name="phone" type="text" class="form-control" id="validationTooltip04"
                                            placeholder="Phone">




                                    </div>
                                    <!-- <div class="ms-auto">
											<select class="form-select" aria-label="Default select example"
												name='shipping' id='shipping'>
												<option value="" disabled selected>Shipping Method</option>
												<option value="Truck">Truck</option>
												<option value="Vessel">Vessel</option>

											</select>
											<br>

											<input name="vessel_name" type="text" class="form-control"
												id="vessel_name" placeholder="Driver Name / Vessel Name"
												value="">
											<br>
											<input name="plate" type="text" class="form-control"
												id="plate" placeholder="Plate / No Segel" value="">
											<br>
											<input name="driverPhone" type="text" class="form-control"
												id="driverPhone" placeholder="Phone Number / No Container"
												value="">
											<br>
											<input name="list_group" type="text" class="form-control"
												id="list_group" placeholder="Group" value="">
											<br>
											<input name="no_voyage" type="text" class="form-control"
												id="no_voyage" placeholder="No Voyage" value="">

										</div> -->
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="card mt-4">
                <div class="card-header pb-0 p-3">
                    <div class="row">
                        <div class="col-6 d-flex align-items-center">
                            <h6 class="mb-0">Material Informations</h6>
                        </div>

                    </div>
                </div>
                <div class="card-body p-3" id="matGrup">
                    <div class="row mb-3">
                        <div class="col-md-2 mb-md-0 mb-4">
                            <label>Material</label>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-4 p-1">
                                    <label>Panjang</label>
                                </div>
                                <div class="col-4 p-1">
                                    <label>Lebar</label>
                                </div>
                                <div class="col-4 p-1">
                                    <label>Tinggi</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-7 p-1">
                                    <label>Kubikasi</label>
                                </div>
                                <div class="col-5 p-1">
                                    <label>Berat</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-5 p-1">
                                    <label>QTY</label>
                                </div>
                                <div class="col-7 p-1">
                                    <label>Satuan</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-3 p-1">
                                    <label>Tonase</label>
                                </div>
                                <div class="col-5 p-1">
                                    <label>Cost</label>
                                </div>
                                <div class="col-4 p-1">
                                    <label>Cost Bill</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1 mb-md-0 mb-4">
                            <label>Total Cost</label>
                        </div>

                    </div>
                    <div class="row mb-3" id='matgroup1'>
                        <div class="col-2 mb-md-0 mb-4">
                            <input name="material[]" type="text" class="form-control" placeholder="Material">
                        </div>
                        <div class="col-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-4 p-1">
                                    <input name="panjang[]" type="text" class="form-control" id="p1" placeholder="P"
                                        onchange="calculate(1)">
                                </div>
                                <div class="col-4 p-1">
                                    <input name="lebar[]" type="text" class="form-control" id="l1" placeholder="L"
                                        onchange="calculate(1)">
                                </div>
                                <div class="col-4 p-1">
                                    <input name="tinggi[]" type="text" class="form-control" id="t1" placeholder="T"
                                        onchange="calculate(1)">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-7 p-1">
                                    <input name="kubikasi[]" type="text" class="form-control" id="k1"
                                        placeholder="kubikasi m3" readonly>
                                </div>
                                <div class="col-5 p-1">
                                    <input name="berat[]" type="text" class="form-control" id="b1" placeholder="Kg"
                                        onchange="calculate(1)">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-5 p-1">
                                    <input name="qty[]" type="text" class="form-control" id="q1" placeholder="Qty"
                                        onchange="calculate(1)">
                                </div>
                                <div class="col-7 p-1">
                                    <input name="satuan[]" type="text" class="form-control" id="validationTooltip04"
                                        placeholder="satuan">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 mb-md-0 mb-4">
                            <div class="row">
                                <div class="col-3 p-1">
                                    <input name="ton[]" type="text" class="form-control" id="ton1" placeholder="Tonase"
                                        value="<?= set_value('date_out') ?>" onchange="calculateTagihan()" readonly>
                                </div>
                                <div class="col-5 p-1">
                                    <input name="cost[]" type="text" class="form-control" id="cost1" placeholder="Cost"
                                        value="" onchange="calculate(1)" onfocus="this.value=''">
                                </div>
                                <div class="col-4 p-1">
                                    <select class="form-select" aria-label="Default select example" name='costbill[]'
                                        id='costbill1' onchange="calculate(1)" style="float: left;">
                                        <option value="0" selected>Default</option>
                                        <option value="1">Ton</option>
                                        <option value="2">Kg</option>
                                        <option value="3">Kubik</option>
                                        <option value="4">Unit</option>
                                        <option value="5">Lot</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 mb-md-0 mb-4">
                            <input name="totcost[]" type="text" class="form-control" placeholder="Total Cost"
                                id="totcost1" onchange="calculateMan(1)" readonly>
                        </div>
                        <div class="col-md-1">
                            <a class="btn bg-gradient-dark mb-0" href="javascript:addMat();"><i class="fas fa-plus"
                                    aria-hidden="true"></i>&nbsp;&nbsp;</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-12 text-center py-4">
                <input class="btn bg-gradient-success mb-0" type="submit" name="Add Data" id='add'></input>
            </div>
        </div>
    </form>



    <footer class="footer pt-3  ">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-lg-between">
                <div class="col-lg-6 mb-lg-0 mb-4">
                    <div class="copyright text-center text-sm text-muted text-lg-start">
                        ©
                        <script>
                        document.write(new Date().getFullYear())
                        </script>,
                        made with <i class="fa fa-heart"></i> by
                        <a href="https://www.breathid.tech" class="font-weight-bold">Breath Tech
                            Ind</a>
                        for a better applications.
                    </div>
                </div>

            </div>
        </div>
    </footer>
    </div>
</main>

<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>

<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<!-- Github buttons -->
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>
<script>
var mat = 1;
var base_url = "<?php echo base_url(); ?>";

function addMat(params) {
    mat = mat + 1;
    var output = $('#matGrup');
    var base_url = "<?php echo base_url(); ?>";
    $.ajax({
        type: "POST",
        url: base_url + "Packing/loadMaterial",
        data: {
            noMat: mat
        },
        cache: false,
        success: function(response) {
            $(output).last().append(response);

        }
    });
}

function curToNum(currency) {
    currency = "" + currency;
    if (currency.length > 0) {
        var num = currency.replaceAll(".", "");
        // alert(num)
        return Number(num.replace(/[^0-9.-]+/g, ""));
    } else {
        return 0;
    }
}

function getPL(pl, area) {
    $.ajax({
        type: "POST",
        url: base_url + "Packing/checkPL",
        data: {
            no_pl: pl,
            area
        },
        cache: false,
        success: function(response) {
            if (response != "Not Exist") {
                console.log(response);
                // var json = jQuery.parseJSON(response);
                var dt = JSON.parse(response);
                $('#shipping').val(dt.method);
                $('#pl_id').val(dt.pl_id);
                $('#vessel_name').val(dt.name);
                $('#plate_').val(dt.no_vehicle);
                $('#driver_phone').val(dt.no_shipping);
                $('#no_voyage').val(dt.no_voyage);
                var mydate = new Date(dt.date_out);
                if (dt.isLot == 0)
                    lot = false;
                else
                    lot = true;
                $('#isLot').prop('checked', lot);
                if (lot)
                    $("#tagihan").attr("readonly", false);
                else
                    $("#tagihan").attr("readonly", true);
                var date = moment(mydate).format('YYYY-MM-DD')
                $('#date_out').val(date);
                $('#tagihan').val(dt.tagihan);
                changeLot(lot);
            }
        }
    });
}
$('#no_pl').change(function() {
    var no_pl = $(this).val();
    var area = $('#area').val();
    // alert(no_pl+"|"+area);
    getPL(no_pl, area);

});
$('#area').change(function() {
    var no_pl = $('#no_pl').val();
    var area = $(this).val();
    // alert(no_pl+"|"+area);
    getPL(no_pl, area);

});
$("#tagihan").change(function() {
    var a = $(this).val();
    a = curToNum(a.toString());
    a = formatMoney(a);
    $(this).val(a);
});
var lot = false;
$("#isLot").change(function() {
    if (this.checked) {
        $("#tagihan").attr("readonly", false);
        lot = true;
        $(this).val('true');
    } else {
        $("#tagihan").attr("readonly", true);
        lot = false;
        $(this).val('false');
    }
    changeLot(lot);
    calculateTagihan();
});
$('#add').on('click', function(e) {
    e.preventDefault();
    var form = $(this).parents('form');
    if ($('#area').val().length < 1) {
        alert('Area Kosong');
        return false;
    } else {
        var po = $('#no_po').val();
        var base_url = "<?php echo base_url(); ?>";
        $.ajax({
            type: "POST",
            url: base_url + "Packing/checkPO",
            data: {
                noPo: po
            },
            cache: false,
            success: function(response) {
                if (response == "Exist") {
                    Swal.fire({
                        title: "Nomor PO sudah ada! Apakah anda akan Melanjutkan?",
                        // showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: "Submit",
                        cancelButtonText: "Tunggu"
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            Swal.fire("Saved!", "", "success").then(function() {
                                form.submit();
                            });
                        } else {
                            Swal.fire("Silahkan Periksa data kembali", "", "info");
                        }
                    });
                } else {
                    Swal.fire({
                        title: "Apakah anda akan menambahkan data?",
                        // showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: "Submit",
                        cancelButtonText: "Tunggu"
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            Swal.fire("Saved!", "", "success").then(function() {
                                form.submit();
                            });
                        } else {
                            Swal.fire("Silahkan Periksa data kembali", "", "info");
                        }
                    });
                }
            }
        });
    }
});
</script>
<script src="<?= base_url() ?>assets/js/util.js"></script>
<script src="<?= base_url() ?>assets/js/material.js"></script>

</html>
