<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->

    <!-- End Navbar -->
    <div class="row my-4">
        <div class="col-lg">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Packing List</h6>
                </div>

                <div class=" card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-3">
                        <table id="example3" class="table table-striped table align-items-center mb-0"
                            style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        No</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Invoice Date
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Invoice Number
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Invoice Name</th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        PO List
                                    </th>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Account Type</th>

                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Cost Type</th>

                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Doc Return
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;
								foreach ($docret as $key => $value):
									# code...
									$no++;
									$inv = base64url_encode($value->no_invoice);
									$dateIn = date_create($value->invoice_date);
									$dateNow = date_create(date("Y-m-d"));
									$diff = date_diff($dateIn, $dateNow);
									$df = $diff->format("%a");
									$trs = "";
									$tds = "";
									if (intval($df) >= 30) {
										$trs = "background-color: #e25f59;";
										$tds = "color:white";
									}
									?>
                                <tr class="text-center" style="<?= $trs ?>">

                                    <td  style="<?= $tds ?>"><?= $no ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->invoice_date ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->no_invoice ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->invoice_name ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->no_pos ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->rekening_type ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->cost_type ?></td>
                                    <td  style="<?= $tds ?>"><?= $value->doc_return ?></td>
                                    <td  style="<?= $tds ?>">


                                        <button class="btn bg-gradient-warning mb-0"
                                            onclick='editDoc(<?=  json_encode(json_encode($value)); ?>)'>Edit
                                        </button>
                                        <button class="btn bg-gradient-dark mb-0"
                                            onclick='getPO(<?= json_encode($value); ?>)'>Detail
                                        </button>
                                        <button class="btn bg-gradient-danger mb-0"
                                            onclick='rejectPO(<?= json_encode($value); ?>)'>Reject
                                        </button>
										<a class="btn bg-gradient-success mb-0"
                                           href="<?=base_url('FileGenerator/printInv/'.$inv)?>">Re-Print
                                        </a>
										<a class="btn bg-gradient-info mb-0"
                                           href="<?=base_url('FileGenerator/generateLampiran/'.$inv)?>">Print Lampiran
                                        </a>
                                        <!-- <?= json_encode($value) ?> -->
                                    </td>

                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>
</main>


<div class="modal" id="myModal">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">PO & Receveiver Informations</h4>
                <button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid py-4" id="poDetails">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- ------ Modal ------------>
<div class="modal fade" id="modalDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add Invoice Informations</h5>
                <!-- <button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button> -->
            </div>
            <div class="modal-body">
                <section class="section dashboard">
                    <div class="col-md-12 position-relative">
                        <div class="row">

                            <div class="col-md-12 position-relative">
                                <label for="validationTooltip01" class="form-label">Nama</label>
                                <input name="nama" type="text" class="form-control" id="nama" placeholder="Nama"
                                    readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 position-relative">
                        <div class="row">
                            <div class="col-md-6 position-relative">
                                <label for="validationTooltip01" class="form-label">Invoice Date</label>
                                <input name="invoice_date" type="text" class="form-control" id="invoice_date"
                                    placeholder="Invoice Date" readonly>
                            </div>
                            <div class="col-md-6 position-relative">
                                <label for="validationTooltip01" class="form-label">Invoice Number</label>
                                <input name="invoice_number" type="text" class="form-control" id="invoice_number"
                                    placeholder="Invoice Number" value="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 position-relative">
                        <div class="col-md-6 position-relative">
                            <label for="validationTooltip01" class="form-label">Payment Date</label>
                            <input name="payment_date" type="date" class="form-control" id="payment_date"
                                placeholder="Payment Date" value="">
                        </div>
                    </div>
                    <div class="col-md-0 position-relative mt-4">
                        <button class="btn btn-primary" onclick="updatedoc()">SUBMIT</button>
                    </div>
            </div>
            </section>
        </div>
    </div>
</div>
</div>


<!-- Modal for editing data -->





<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>

<script>
var base_url = "<?php echo base_url(); ?>";
var area = "";
</script>
<script>
new DataTable('#example3');
$(document).ready(function() {
    // $(".js-basic-multiple").select2({
    // 	dropdownParent: $('#exampleModalCenter .modal-content')

    // });
});
var idpo = 0;

function editDoc(a) {
    var jsn = JSON.parse(a);
    console.log(jsn);
    idpo = jsn.id;

    $('#invoice_number').val(jsn.no_invoice);
    $('#nama').val(jsn.invoice_name);

    // var dt = Date.parse(jsn.invoice_date);

    $('#invoice_date').val(jsn.invoice_date);
    // console.log(a);
    // var jsn = JSON.parse(a);
    // console.log(jsn.id);
    // idpo = jsn.id;
    // var mydate = new Date(jsn.date_in);
    // var date_in = moment(mydate).format('YYYY-MM-DD');
    // $('#no_po').val(jsn.no_po);
    // $('#no_sji').val(jsn.no_sji);
    $('#modalDoc').modal('show');
    // getPL(a, area);

}

function getPO(a) {
    var output = $('#poDetails');


    $.ajax({
        type: "POST",
        url: base_url + "/Packing/poDetails/",
        data: {
            po: a,
        },
        dataType: "text",
        cache: false,
        success: function(response) {
            $(output).html(response);
            // alert(response);
            $('#myModal').modal('show');
        }
    });
}

function rejectPO(a) {
    var output = $('#poDetails');
    Swal.fire({
        title: "Apakah anda akan Mereject data ini?",
        // showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak"
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                type: "POST",
                url: base_url + "/Packing/rejectInvoices",
                data: {
                    po: a,
                },
                dataType: "text",
                cache: false,
                success: function(response) {
                    console.log(response);
                    if (response == "success") {
                        Swal.fire("Saved!", "", "success").then(function() {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Gagal Update Data!", "", "error").then(function() {
                            location.reload();
                        });
                    }
                }
            });
        } else {
            Swal.fire("Silahkan Periksa data kembali", "", "info");
        }
    });


}

function updatedoc() {
    var paydate = $('#payment_date').val();
    var invoice_number = $('#invoice_number').val();
    if (paydate.length < 1) {
        alert("Mohon isi Invoice Date!");
    } else {
        Swal.fire({
            title: "Apakah anda akan menambahkan data?",
            // showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: "Submit",
            cancelButtonText: "Tunggu"
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: base_url + "Packing/updatePayment",
                    data: {
                        id: idpo,
                        payment_date: paydate,
                        invoice_number: invoice_number
                    },
                    cache: false,
                    success: function(response) {
                        console.log(response);
                        if (response == "success") {
                            Swal.fire("Saved!", "", "success").then(function() {
                                location.reload();
                            });
                        } else {
                            Swal.fire("Gagal Update Data!", "", "error").then(function() {
                                location.reload();
                            });
                        }
                    }
                });
            } else {
                Swal.fire("Silahkan Periksa data kembali", "", "info");
            }
        });
    }
}
</script>
<!-- Github buttons -->
<script src="<?= base_url() ?>assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>

