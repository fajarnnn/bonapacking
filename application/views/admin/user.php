<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>

				<div class="col-lg-12 px-3 text-end">
					<div class="btn bg-gradient-success ms-auto col-lg-2 justify-content-end py-2 px-2"
						data-bs-toggle="modal" data-bs-target="#exampleModal2">
						Add User</div>
				</div>
				<div class="card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="userTable" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Nama</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										User Name
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Email</th>


									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Password</th>
									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Role</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Detail
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($users as $key => $value): ?>
									<tr class="text-center">
										<td class="id" style="display: none;"><?= $value->id ?></td>
										<td class="no"><?= $no ?></td>
										<td class="name"><?= $value->nama ?></td>
										<td class="uname"><?= $value->username ?></td>
										<td class="email"><?= $value->email ?></td>
										<td class="pass"><?= $value->password ?></td>
										<td><?= $value->role ?></td>
										<td class="rl" style="display: none;"><?= $value->role_id ?></td>
										<td>
											<div class="col-6">
											<button id="editUser">
												<span class="btn bg-gradient-warning mb-0" data-bs-toggle="modal"
													data-bs-target="#exampleModal">Edit</span>
											</button>
											</div>
											<div class="col-6">

												<form action="<?= base_url('admin/delete/') ?>"
													onsubmit="confirmDelete(event, this)">
	
													<button class="btn bg-gradient-danger mb-0">
														Delete</button>
												</form>
											</div>


										</td>
									</tr>
									<?php $no++;
								endforeach; ?>
							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>
	</div>
</main>




<!-- Modal for editing data -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Add User</h5>
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<section class="section dashboard">
					<form class="row g-3 needs-validation pt-0 justify-content-center" novalidate
						action="<?= base_url('Admin/add_user') ?>" method="post" enctype="multipart/form-data"
						id="addForm" form="modal-form">
						<!-- Hidden field for storing the no_pl value -->
						<div class="row">
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Nama</label>
								<input name="nama" type="text" class="form-control" value="" required>
								<div class="invalid-tooltip">
									masukan nama dengan benar
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Email</label>
								<input name="email" type="email" class="form-control" value="" required>
								<div class="invalid-tooltip">
								</div>
							</div>
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Username</label>
								<input name="username" type="text" class="form-control" value="" required>
								<div class="invalid-tooltip">
									Mohon masukan username
								</div>
							</div>
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Password</label>
								<input name="password" type="password" class="form-control" value="">
								<div class="invalid-tooltip">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Role</label>
								<select class="form-select" aria-label="Default select example" name='role' required>
									<option value="" disabled selected>Choose One</option>
									<option value="1">
										Administrator</option>
									<option value="2">
										Officer</option>
									<option value="3">
										Partner</option>
									<option value="4">
										Accounting</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Corporate</label>
								<select class="form-select" aria-label="Default select example" name='corporate'>
									<option value="" disabled selected>Choose One</option>
									<?php foreach ($corporate as $key => $value): ?>
										<option value="<?= $value->list_group ?>">
											<?= $value->list_group ?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="d-flex justify-content-end mt-3">
							<div class="col-md-0 position-relative">
								<input type="submit" name="submit" class="btn btn-primary" id="add">
							</div>
						</div>
					</form>
					<div class="alert alert-danger text-white font-weight-bold m-0 p-2" role="alert" id="alert">
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
				<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<section class="section dashboard">
					<form class="row g-3 needs-validation pt-0 justify-content-center" novalidate
						action="<?= base_url('admin/edit_user') ?>" method="post" enctype="multipart/form-data"
						id="editForm">
						<!-- Hidden field for storing the no_pl value -->
						<input type="hidden" name="no_pl_old" value="">
						<div class="row">
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Nama</label>
								<input name="editnama" id="editnama" type="text" class="form-control" value="" required>
								<input name="editid" id="editid" type="hidden" class="form-control" value="" required>
								<div class="invalid-tooltip">
									Please enter a valid No PL.
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Email</label>
								<input name="editemail" id="editemail" type="email" class="form-control" value=""
									required>
								<div class="invalid-tooltip">
									Please enter a valid No PL.
								</div>
							</div>
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Username</label>
								<input name="editusername" id="editusername" type="text" class="form-control" value=""
									required>
								<div class="invalid-tooltip">
									Please enter a valid No PL.
								</div>
							</div>
							<div class="col-md-4 position-relative">
								<label for="validationTooltip01" class="form-label">Password</label>
								<input name="editpassword" id="editpassword" type="password" class="form-control"
									value="">
								<div class="invalid-tooltip">
									Please enter a valid No PL.
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Role</label>
								<select class="form-select" aria-label="Default select example" name='editrole'
									id='editrole'>
									<option value="">Choose One</option>
									<option value="1">
										Administrator</option>
									<option value="2">
										Officer</option>
									<option value="3">
										Partner</option>
									<option value="4">
										Accounting</option>
								</select>
							</div>
						</div>

					</form>
					<div class="d-flex justify-content-end mt-3">
						<div class="col-md-0 position-relative">
							<input type="submit" name="submit" class="btn btn-primary" id="edit">
						</div>
					</div>

				</section>
			</div>
		</div>
	</div>
</div>

<!--   Core JS Files   -->
<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js"></script>
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
<script src="https://cdn.datatables.net/2.0.8/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>
<script>
	$('#alert').hide();
	$(document).ready(function () {
		<?php if ($this->session->flashdata('addUserMsg')) { ?>
			$('#alert').html('<?php echo $this->session->flashdata('addUserMsg'); ?>').show();
		<?php } else { ?>
			$('#alert').hide();
		<?php } ?>
	});
</script>
<script>
	function confirmDelete(e, form) {
		e.preventDefault();
		Swal.fire({
			title: "Apakah Anda Akan Menghapus User ini?",
			showCancelButton: true,
			confirmButtonText: "YA",
			cancelButtonText: "TIDAK",
			icon: "warning"
		}).then((result) => {
			if (result.isConfirmed) {
				form.submit();
			} else {
				return false;
			}
		});
	}
	$('#add').on('click', function (e) {
		e.preventDefault();
		var form = $('#addForm');
		var isFormValid = document.getElementById('addForm').checkValidity();


		if (!isFormValid) {
			document.getElementById('addForm').reportValidity();
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					Swal.fire("Saved!", "", "success").then(function () {
						// console.log(form);
						$('#addForm')[0].dispatchEvent(new Event('submit'));
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}

	});
	$('#edit').on('click', function (e) {
		e.preventDefault();
		var form = $('#editForm');
		var isFormValid = document.getElementById('editForm').checkValidity();


		if (!isFormValid) {
			document.getElementById('editForm').reportValidity();
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					Swal.fire("Saved!", "", "success").then(function () {
						// console.log(form);
						$('#editForm')[0].dispatchEvent(new Event('submit'));
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}

	});
	var table = document.querySelectorAll('#userTable tr button');
	table.forEach(function (ele) {
		ele.addEventListener('click', function (e) {
			var rowid = $(this).closest('tr');
			var no = rowid.find(".no").text();
			var id = rowid.find(".id").text();
			var name = rowid.find(".name").text();
			var uname = rowid.find(".uname").text();
			var email = rowid.find(".email").text();
			var pass = rowid.find(".pass").text();
			var rl = rowid.find(".rl").text();
			$('#editnama').val(name);
			$('#editusername').val(uname);
			$('#editemail').val(email);
			// $('#editpassword').val(pass);
			$('#editid').val(id);
			$('#editrole').val(rl);
		})
	});

	function tableText(tableRow) {
		var myJSON = JSON.stringify(tableRow);
		console.log(myJSON);
	}
</script>

</html>
