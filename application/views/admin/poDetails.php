<ul class="list-group">
	<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
		<div class="d-flex flex-column">
			<h6 class="mb-3 text-sm"><?= isset($details[0]->no_po) ? $details[0]->no_po : "-" ?></h6>
			<span class="mb-3 text-xs">Penerima
				&nbsp;&nbsp;: <span
					class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->receiver) ? $details[0]->receiver : "-" ?>
					</p></span></span>
			<span class="mb-3 text-xs">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				: <span
					class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->address) ? $details[0]->address : "-" ?>
				</span></span>

			<span class="mb-3 text-xs">Jalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				: <span class="text-dark ms-sm-2 font-weight-bold">
					<p class="text-sm">
						<?= isset($details[0]->no_po) ? $details[0]->street : "-" ?>
				</span></span>
			<span
				class="mb-3 text-xs">PIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
				<span class="text-dark ms-sm-2 font-weight-bold">
					<p class="text-sm">
						<?= isset($details[0]->pic) ? $details[0]->pic : "-" ?>
					</p> |
					085268845886
				</span></span>
			<span class="mb-3 text-xs">Plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
				<span
					class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->plant) ? $details[0]->plant : "-" ?></span></span>
			<span class="mb-3 text-xs">Doc Return&nbsp;:<span
					class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->doc_return) ? $details[0]->doc_return : "-" ?></span></span>
		</div>
		<div class="ms-auto">
			<h6 class="mb-3 text-sm">SJ.NO.MSI/SH/0/0/0/0</h6>
			<span class="text-xs">Transport Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
					class="text-dark font-weight-bold ms-sm-2">Truck</span></span>
			<br>
			<span
				class="text-xs">Driver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
				<span
					class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->driver) ? $details[0]->driver : "-" ?>
				</span></span>
			<br>
			<span
				class="mb-2 text-xs">Plate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
				<span
					class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->plate) ? $details[0]->plate : "-" ?></span>
			<br>
			<span
				class="mb-2 text-xs">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
					class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->phone) ? $details[0]->phone : "-" ?></span></span>
			<br>
			<span
				class="mb-2 text-xs">Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
					class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->list_group) ? $details[0]->list_group : "-" ?></span></span>

		</div>



	</li>

</ul>
