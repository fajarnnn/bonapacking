<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?=base_url()?>assets/img/bona.png">
	<title>
		Bona
	</title>
	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<!-- Nucleo Icons -->

	<link href="<?=base_url()?>assets/css/nucleo-icons.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/css/nucleo-svg.css" rel="stylesheet" />
	<!-- Font Awesome Icons -->
	<!-- <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script> -->
	<link href="<?=base_url()?>assets/css/nucleo-svg.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/css/dataTables.uikit.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/css/uikit.min.css" rel="stylesheet" />
	<!-- CSS Files -->
	<link id="pagestyle" href="<?=base_url()?>assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
	<!-- Nepcha Analytics (nepcha.com) -->
	<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
	<script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
</head><body class="g-sidenav-show  bg-gray-100">

	<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		<!-- Navbar -->

		<!-- End Navbar -->
		<div class="container-fluid py-4">
			<div class="row mt-4">
				<div class="col-lg-12 mb-lg-0 mb-4">
					<div class="card">
						<div class="card-body p-3">
							<div class="row">
								<div class="col-lg-12">
									<div class="d-flex flex-column h-100">
										<h5 class=" font-weight-bolder text-center">DAFTAR PENGIRIMAN BARANG</h5>

									</div>
								</div>
								<div class="col-lg-6">
									<div class="d-flex flex-column h-100">
										<p class="mb-1 pt-2">Truck / Vessel : abcde</p>
										<p class="mb-1 pt-2">Tujuan : Universitas Jambi Mendalo Barat Kec.Jambi</p>
										<p class="mb-1 pt-2">Jakarta 29 February 20204</p>
										<p class="mb-1 pt-2">Group : Salim</p>



									</div>
								</div>
								<div class="col-lg-5 ms-auto text-center mt-5 mt-lg-0">


									<div class="position-relative d-flex align-items-center justify-content-center h-100">
										<img class="w-50 h-10 position-top z-index-2 pt-4" src="<?=base_url()?>assets/img/bona.png" alt="rocket">


									</div>
								</div>
								<div class="col-lg-5 ms-auto text-center mt-5 mt-lg-0">


									<div class="position-relative d-flex align-items-center justify-content-center h-100">
										<p class="mb-1 pt-2">No PO : 45664149865165</p>


									</div>
								</div>
								<div class="table-responsive p-3">
									<p class="text-center">SUPIL : Johny 08525896547</p>

									<table class="table table-striped table align-items-center mb-0" style="width:100%">
										<thead>
											<tr class="text-center">
												<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-10">
													No</th>
												<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-10">
													Penerima</th>
												<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-10">
													Barang</th>
												<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-10 ps-2">
													No PO</th>
												<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-10">
													Jumlah
												</th>
												<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-10">
													Satuan</th>
												<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-10 ps-5">
													Keterangan</th>

											</tr>
										</thead>
										<tbody>

											<tr class="text-center">
												<td class="text-sm">1</td>
												<td class="text-sm">
													<p class="text-sm">Ahmad</p>
													<p class="text-sm">0582364789</p>
												</td>
												<td class="text-sm">
													<p class="text-sm">Dinalbolt</p>
													<p class="text-sm">Paku</p>
													<p class="text-sm">Obeng</p>
													<p class="text-sm">Baut</p>
												</td>
												<td class="text-sm">6549812348</td>
												<td class="text-sm">
													<p class="text-sm">4</p>
													<p class="text-sm">6</p>
													<p class="text-sm">7</p>
													<p class="text-sm">8</p>
												</td>
												<td class="text-sm">
													<p class="text-sm">PCS</p>
													<p class="text-sm">PCS</p>
													<p class="text-sm">PCS</p>
													<p class="text-sm">PCS</p>
												</td>
												<td class="text-sm">-</td>
											</tr>
											<tr>
												<td colspan="4" class="text-center font-weight-bold">TOTAL</td>
												<td colspan="1" class="text-center font-weight-bold">7</td>
												<td colspan="1" class="text-center font-weight-bold">Colly</td>
											</tr>
										</tbody>
									</table>

								</div>
							</div>
							<div class="row">

								<div class="col-lg-6">
									<div class="d-flex flex-column h-100">
										<p class="mb-1 pt-5 text-center">Yang Menerima</p>
										<br>
										<br>
										<br>
										<br>
										<br>
										<p class="text-center">(&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;)</p>



									</div>
								</div>
								<div class="col-lg-6">
									<div class="d-flex flex-column h-100">
										<p class="mb-1 pt-5 text-center">Yang Menyerahkan</p>
										<br>
										<br>
										<br>
										<br>
										<br>
										<p class="text-center">(&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
											&nbsp; &nbsp;)</p>





									</div>



								</div>
							</div>
						</div>
					</div>
				</div>

			</div>








		</div>
	</main>


	<!--   Core JS Files   -->
	<script src="<?=base_url()?>assets/js/jquery-3.7.1.js"></script>
	<script src="<?=base_url()?>assets/js/uikit.min.js"></script>
	<script src="<?=base_url()?>assets/js/dataTables.js"></script>
	<script src="<?=base_url()?>assets/js/dataTables.uikit.js"></script>
	<script src="<?=base_url()?>assets/js/core/popper.min.js"></script>
	<script src="<?=base_url()?>assets/js/core/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/plugins/perfect-scrollbar.min.js"></script>
	<script src="<?=base_url()?>assets/js/plugins/smooth-scrollbar.min.js"></script>
	<script src="<?=base_url()?>assets/js/plugins/chartjs.min.js"></script>


	<script>
		new DataTable('#example3');
	</script>
	<!-- Github buttons -->
	<script async defer src="https://buttons.github.io/buttons.js"></script>
	<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
	<script src="<?=base_url()?>assets/js/soft-ui-dashboard.min.js"></script>
</body></html>
