    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->

        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">

                        <div class="col-xl-6">
                            <div class="row">


                            </div>
                        </div>
                        <div class="col-md-12 mb-lg-0 mb-4">
                            <div class="card mt-4">
                                <div class="card-header pb-0 p-3">
                                    <div class="row">

                                        <div class="col-12 text-end">
                                            <!-- <?php var_dump($details) ?> -->
                                            <a class="btn bg-gradient-danger mb-0 mx-3" href="<?= base_url() ?>Admin/seacrhPO/<?= $details[0]->no_pl . "/" . rawurlencode($details[0]->area) ?>" id="prt">Generate
                                                PDF</a><a class="btn bg-gradient-success mb-0 mx-3" href="<?= base_url() ?>Admin/generateExcel/<?= (isset($details[0]->no_pl) ? $details[0]->no_pl : "-") . "/" . $details[0]->area ?>" id="prt">Generate
                                                Excel</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-md-6 mb-md-0 mb-4">
                                            <h6 class="mb-0">No Packing List</h6>
                                            <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">


                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->no_pl) ? $details[0]->no_pl : "-" ?>
                                                </h6>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="mb-0">Area</h6>
                                            <div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">

                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->area) ? $details[0]->area : "-" ?>

                                                    </h5>
                                                </h6>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row my-4">
                <div class="col-lg">
                    <div class="card mb-4">
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-3">
                                <table id="example3" class="table table-striped table align-items-center mb-0" style="width:100%">
                                    <thead>
                                        <tr class="text-center">
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                No PO</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                No SJI</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                Penerima</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                Detail
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($details as $key => $value) {
                                            # code<?=base_url()
                                            $nopo = isset($value->no_po) ? $value->no_po : "-";
                                            $nosji = isset($value->no_sji) ? $value->no_sji : "-";
                                            // $id = isset($value->id) ? $value->id : "-";
                                            $rawpo = rawurlencode(str_replace('/', '_', $nopo));
                                            $rawsji = rawurlencode(str_replace('/', '_', $nosji));
                                            $no++;
                                        ?>
                                            <tr class="text-center">
                                                <td><?= $no ?></td>
                                                <td><?= isset($value->no_po) ? $value->no_po : "-" ?></td>
                                                <td><?= isset($value->no_sji) ? $value->no_sji : "-" ?></td>
                                                <td><?= isset($value->corporate) ? $value->corporate : "-" ?></td>
                                                <td>

                                                    <span class="btn bg-gradient-dark mb-0" data-bs-toggle="modal" data-bs-target="#myModal">Detail</span>
                                                    |
                                                    <a href="<?= base_url() ?>Admin/editPO/<?= $rawpo ?>/<?= $rawsji ?>"><span class="btn bg-gradient-warning mb-0">Edit</span>|
                                                        <span class="btn bg-gradient-danger mb-0">delete</span></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer pt-3  ">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6 mb-lg-0 mb-4">
                        <div class="copyright text-center text-sm text-muted text-lg-start">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>,
                            made with <i class="fa fa-heart"></i> by
                            <a href="https://www.breathid.tech" class="font-weight-bold">Breath
                                Tech
                                Ind</a>
                            for a better applications.
                        </div>
                    </div>

                </div>
            </div>
        </footer>
        </div>
    </main>

    <!---------------- Modal Start --------------->
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">PO & Receveiver Informations</h4>
                    <button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid py-4">
                        <div class="row">
                            <div class="col-md-6 mt-4">
                                <div class="card">
                                    <div class="card-header pb-0 px-3">
                                        <h6 class="mb-0">PO Information</h6>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <ul class="list-group">
                                            <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                                <div class="d-flex flex-column">
                                                    <h6 class="mb-3 text-sm">
                                                        <?= isset($details[0]->no_po) ? $details[0]->no_po : "-" ?></h6>
                                                    <span class="mb-3 text-xs">Supplier
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->supplier) ? $details[0]->supplier : "-" ?></span></span>



                                                    <span class="mb-3 text-xs">Date
                                                        In&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_in) ? $details[0]->date_in : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Date
                                                        Out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_out) ? $details[0]->date_out : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Unload
                                                        Document&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_out) ? $details[0]->date_out : "-" ?></span></span>

                                                </div>

                                                <div class="ms-auto">
                                                    <h6 class="mb-3 text-sm">
                                                        <?= isset($details[0]->no_sji) ? $details[0]->no_sji : "-" ?>
                                                    </h6>

                                                    <span class="mb-3 text-xs">Tagihan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">RP 22.010/m3
                                                            xxx/Kg
                                                        </span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">No
                                                        POT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->no_pot) ? $details[0]->no_pot : "-" ?></span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->remark) ? $details[0]->remark : "-" ?></span></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Inv
                                                        Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->invoice_date) ? $details[0]->invoice_date : "-" ?></span>
                                                    <br>
                                                    <span class="mb-3 text-xs">Inv
                                                        No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->no_invoice) ? $details[0]->no_invoice : "-" ?></span>

                                                </div>



                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mt-4">
                                <div class="card h-100">
                                    <div class="card-header pb-0 px-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="mb-0">Receiver & Shipping Informations</h6>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <ul class="list-group">
                                            <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                                <div class="d-flex flex-column">
                                                    <h6 class="mb-3 text-sm">4506441542</h6>
                                                    <span class="mb-3 text-xs">Penerima
                                                        &nbsp;&nbsp;: <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->receiver) ? $details[0]->receiver : "-" ?>
                                                            </p></span></span>
                                                    <span class="mb-3 text-xs">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        : <span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->address) ? $details[0]->address : "-" ?>
                                                        </span></span>

                                                    <span class="mb-3 text-xs">Jalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        : <span class="text-dark ms-sm-2 font-weight-bold">
                                                            <p class="text-sm">
                                                                <?= isset($details[0]->street) ? $details[0]->street : "-" ?>
                                                        </span></span>
                                                    <span class="mb-3 text-xs">PIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark ms-sm-2 font-weight-bold">
                                                            <p class="text-sm">
                                                                <?= isset($details[0]->pic) ? $details[0]->pic : "-" ?>
                                                            </p> |
                                                            085268845886
                                                        </span></span>
                                                    <span class="mb-3 text-xs">Plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->plant) ? $details[0]->plant : "-" ?></span></span>
                                                    <span class="mb-3 text-xs">Doc Return&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->doc_return) ? $details[0]->doc_return : "-" ?></span></span>
                                                </div>
                                                <div class="ms-auto">
                                                    <h6 class="mb-3 text-sm">SJ.NO.MSI/SH/0/0/0/0</h6>
                                                    <span class="text-xs">Transport
                                                        Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">Truck</span></span>
                                                    <br>
                                                    <span class="text-xs">Driver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->driver) ? $details[0]->driver : "-" ?>
                                                        </span></span>
                                                    <br>
                                                    <span class="mb-2 text-xs">Plate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                                        <span class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->plate) ? $details[0]->plate : "-" ?></span>
                                                    <br>
                                                    <span class="mb-2 text-xs">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->phone) ? $details[0]->phone : "-" ?></span></span>
                                                    <br>
                                                    <span class="mb-2 text-xs">Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->list_group) ? $details[0]->list_group : "-" ?></span></span>

                                                </div>



                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <div class="card">
                                    <div class="card-header pb-0 px-3">
                                        <h6 class="mb-0">Material Information</h6>
                                    </div>
                                    <div class="card-body pt-4 p-3">
                                        <div class="table-responsive p-3">
                                            <table id="example3" class="table table-striped table align-items-center mb-0" style="width:100%">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            No</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Material</th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Panjang
                                                        </th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Lebar</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Tinggi</th>
                                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                            Berat</th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Kubikasi
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Satuan
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Cost m3
                                                        </th>
                                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                                            Cost Kg
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <tr class="text-center">





                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                        </div>




                    </div>
                </div>
            </div>

        </div>
    </div>



    <!-------------- Modal End ------------>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.uikit.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>

    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
    <script>
        new DataTable('#example3');

        function getPO(a, b) {
            var output = $('#poDetails');

            var base_url = "<?php echo base_url(); ?>";
            $.ajax({
                type: "POST",
                url: base_url + "/Admin/poDetails/",
                data: {
                    no_po: a,
                    nosji: b
                },
                dataType: "text",
                cache: false,
                success: function(response) {
                    $(output).html(response);
                    // alert(response);
                }
            });
        }
        $('#prt').on('click', function() {
            var dt = "<?= $details[0]->no_pl ?>";
            if (dt.length < 1) {
                alert('No PL Kosong');
                return false;
            } else {
                return true;
            }
        });
    </script>
</body>

</html>
