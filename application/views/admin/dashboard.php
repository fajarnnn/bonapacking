<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="./assets/img/bona.png">
    <title>
        Bona
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url() ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/2.0.8/css/dataTables.uikit.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/css/uikit.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url() ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script>
</head>

<body class="g-sidenav-show  bg-gray-100">
    <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 "
        id="sidenav-main">
        <div class="sidenav-header">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
                aria-hidden="true" id="iconSidenav"></i>
            <a class="navbar-brand m-0" href="<?= base_url() ?> ">
                <img src="<?= base_url() ?>/assets/img/bona.png" class="navbar-brand-img h-100" alt="main_logo">

            </a>
        </div>
        <hr class="horizontal dark mt-0">
        <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link  active" href="<?= base_url('admin') ?>">

                        <div
                            class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>shop </title>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1716.000000, -439.000000)" fill="#FFFFFF"
                                        fill-rule="nonzero">
                                        <g transform="translate(1716.000000, 291.000000)">
                                            <g transform="translate(0.000000, 148.000000)">
                                                <path class="color-background opacity-6"
                                                    d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z">
                                                </path>
                                                <path class="color-background"
                                                    d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1">Dashboard</span>
                    </a>
                </li>





            </ul>
            <ul class="navbar-nav mt-3">
                <li class="nav-item">
                    <a class="nav-link  active" href="<?= base_url('admin/user') ?>">

                        <div
                            class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>shop </title>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1716.000000, -439.000000)" fill="#FFFFFF"
                                        fill-rule="nonzero">
                                        <g transform="translate(1716.000000, 291.000000)">
                                            <g transform="translate(0.000000, 148.000000)">
                                                <path class="color-background opacity-6"
                                                    d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z">
                                                </path>
                                                <path class="color-background"
                                                    d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z">
                                                </path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1">User</span>
                    </a>
                </li>





            </ul>
        </div>

    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->

        <!-- End Navbar -->
        <div class="row my-4">
            <div class="col-lg">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Packing List</h6>
                    </div>

                    <div class="col-lg-12 px-3 text-end">
                        <a type="button" href="<?= base_url('Admin/data_form') ?>"
                            class="btn bg-gradient-success ms-auto col-lg-2 justify-content-end py-2 px-2">
                            Add Packing List</a>
                    </div>
                    <div class="card-body px-0 pt-0 pb-2">
                        <div class="table-responsive p-3">
                            <table id="example3" class="table table-striped table align-items-center mb-0"
                                style="width:100%">
                                <thead>
                                    <tr class="text-center">
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            No</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            No PL</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Date Out
                                        </th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Region</th>


                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Corporate</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Group</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Truck Driver / Vessel Name</th>
                                        <th
                                            class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                            Driver</th>
                                        <th
                                            class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                            Detail
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    setlocale(LC_TIME, 'ID_id');
                                    setlocale(LC_ALL, 'id_ID');
                                    $no = 0;
                                    $shipping = "TRUCK";
                                    $nameship = "";
                                    foreach ($all as $key => $value) {
                                        # code..
                                        $no++;
                                        if (isset($value->vessel_name) || $value->vessel_name != '-') {
                                            $shipping = "VESSEL";
                                            $nameship = $value->vessel_name;
                                        } else {
                                            $nameship = $value->driver;
                                        }

                                        try {
                                            $dateIn = strtotime($value->date_in);
                                            $SdateIn = strftime('%d %B %Y', $dateIn);
                                            if (strpos($SdateIn, '1970')) {
                                                $SdateIn = '-';
                                            }
                                        } catch (Exception $e) {
                                            $SdateIn = '-';
                                        }
                                        try {
                                            $dateOut = strtotime($value->date_out);
                                            $SdateOut = strftime('%d %B %Y', $dateOut);
                                            if (strpos($SdateOut, '1970')) {
                                                $SdateOut = '-';
                                            }
                                        } catch (Exception $e) {
                                            $SdateOut = '-';
                                        }
                                    ?>
                                    <tr class="text-center">

                                        <td><?= $no ?></td>
                                        <td><?= $value->no_pl ?></td>
                                        <td><?= $SdateOut ?></td>
                                        <td><?= $value->area ?></td>

                                        <td><?= $value->corporate ?></td>
                                        <td>
                                            <?= $value->list_group ?>
                                        </td>
                                        <td><?= $shipping ?></td>
                                        <td><?= $nameship ?></td>

                                        <td>
                                            <a
                                                href="<?= base_url() ?>Admin/detail/<?= (isset($value->no_pl) ? $value->no_pl : "-") . "/" . rawurlencode($value->area) ?>">
                                                <span class="btn bg-gradient-dark mb-0">Detail</span> <span
                                                    class="btn bg-gradient-danger mb-0">Delete</span></a>


                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="col-lg-12 px-3 text-end">
                        <a type="button" href="<?= base_url('generator/generateAllExcel') ?>"
                            class="btn bg-gradient-warning ms-auto col-lg-2 justify-content-end py-2 px-2">
                            Download All Data</a>
                    </div>
                </div>
            </div>
    </main>


    <!-------- Modal ------------>
    <!-- <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<section class="section dashboard">

						<form class="row g-3 needs-validation  pt-0 justify-content-center" novalidate action="submit"
							method="post" enctype="multipart/form-data">



							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">No PL</label>
								<input name="no_pl" type="text" class="form-control" id="validationTooltip04"
									placeholder="No PL">

							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Shipping Method</label>
								<select class="form-select" aria-label="Default select example" name='doc_type'>
									<option value="" disabled selected>Choose One</option>
									<option value="pdf">Truck</option>
									<option value="exce">Vessel</option>

								</select>
							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Name</label>
								<input name="name" type="text" class="form-control" id="validationTooltip04"
									placeholder="Driver Or Vessel">

							</div>
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Phone</label>
								<input name="phone" type="text" class="form-control" id="validationTooltip04"
									placeholder="Phone">

							</div>






							<div class="d-flex justify-content-end">

								<div class="col-md-0 position-relative">
									<input type="submit" name="submit" class="btn btn-primary">
								</div>
							</div>
						</form>
					</section>
				</div>
			</div>
		</div>
	</div> -->


    <!-- Modal for editing data -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Data</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <section class="section dashboard">
                        <form class="row g-3 needs-validation pt-0 justify-content-center" novalidate action="submit"
                            method="post" enctype="multipart/form-data">
                            <!-- Hidden field for storing the no_pl value -->
                            <input type="hidden" name="no_pl_old" value="<?= $value->no_pl ?>">

                            <div class="col-md-12 position-relative">
                                <label for="validationTooltip01" class="form-label">No PL</label>
                                <input name="no_pl" type="text" class="form-control" id="validationTooltip04"
                                    value="<?= $value->no_pl ?>" required>
                                <div class="invalid-tooltip">
                                    Please enter a valid No PL.
                                </div>
                            </div>



                            <div class="col-md-12 position-relative">
                                <label for="validationTooltip01" class="form-label">Shipping Method</label>
                                <select class="form-select" aria-label="Default select example" name='shipping_method'>
                                    <option value="" disabled selected>Choose One</option>
                                    <option value="truck" <?= ($value->shipping_method == 'truck') ? 'elected' : '' ?>>
                                        Truck</option>
                                    <option value="vessel"
                                        <?= ($value->shipping_method == 'vessel') ? 'elected' : '' ?>>
                                        Vessel</option>
                                </select>
                            </div>

                            <div class="col-md-12 position-relative">
                                <label for="validationTooltip01" class="form-label">Name</label>
                                <input name="name" type="text" class="form-control" id="validationTooltip04"
                                    value="<?= $value->driver ?>" required>
                                <div class="invalid-tooltip">
                                    Please enter a valid name.
                                </div>
                            </div>
                            <div class="col-md-12 position-relative">
                                <label for="validationTooltip01" class="form-label">Phone</label>
                                <input name="phone" type="text" class="form-control" id="validationTooltip04"
                                    value="<?= $value->driver ?>" required>
                                <div class="invalid-tooltip">
                                    Please enter a valid name.
                                </div>
                            </div>
                        </form>
                        <div class="d-flex justify-content-end">
                            <div class="col-md-0 position-relative">
                                <input type="submit" name="submit" class="btn btn-primary">
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.8/js/dataTables.uikit.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>


    <script>
    new DataTable('#example3');
    $(document).ready(function() {
        $(".js-basic-multiple").select2({
            dropdownParent: $('#exampleModalCenter .modal-content')

        });
    });
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>
