insert into shipping (pl_id, method, name, no_shipping, no_vehicle, no_voyage) select p.id, pls.shipping, 
CASE 
  WHEN pls.driver is null THEN pls.vessel_name 
  WHEN LENGTH(pls.driver) = 0 THEN pls.vessel_name
  WHEN pls.driver = "-" THEN pls.vessel_name
  ELSE pls.driver 
END as name,
CASE 
   WHEN pls.phone is null THEN pls.vessel_no_segel
  WHEN LENGTH(pls.phone) = 0 THEN pls.vessel_no_segel
  WHEN pls.phone = "-" THEN pls.vessel_no_segel
  ELSE pls.phone 
END as no_shipping,
CASE 
  WHEN pls.plate is null THEN pls.vessel_no_cont 
  WHEN LENGTH(pls.plate) = 0 THEN pls.vessel_no_cont
  WHEN pls.plate = "-" THEN pls.vessel_no_cont
  ELSE pls.plate 
END as no_vehicle,
pls.vessel_no_voyage
from packing_list pls join pl p 
on p.no_pl = pls.no_pl and p.area = pls.area
GROUP by pls.no_pl,pls.area ORDER by pls.no_pl, pls.area;
