<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?= base_url() ?>/assets/img/bona.png">
	<title>
		Bona
	</title>
	<!--     Fonts and icons     -->
	<link href="<?= base_url() ?>/assets/css/fontgoogle.css" rel="stylesheet" />
	<!-- Nucleo Icons -->
	<link href="<?= base_url() ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
	<link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
	<link href="<?= base_url() ?>/assets/css/image.css" rel="stylesheet" />
	<!-- Font Awesome Icons -->
	<script src="<?= base_url() ?>/assets/js/all.min.js" crossorigin="anonymous">
	</script>
	<link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
	<link href="<?= base_url() ?>/assets/css/dataTables.uikit.css" rel="stylesheet" />
	<link href="<?= base_url() ?>/assets/css/select.dataTables.css" rel="stylesheet" />

	<link href="<?= base_url() ?>/assets/css/uikit.min.css" rel="stylesheet" />
	<!-- CSS Files -->
	<link id="pagestyle" href="<?= base_url() ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
	<!-- Nepcha Analytics (nepcha.com) -->
	<!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->

</head>

<body class="g-sidenav-show  bg-gray-100">

	<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
		<!-- Navbar -->

		<!-- End Navbar -->
		<?php if (count($details) > 0) { ?>

			<div class="container-fluid py-4">

				</a>
				<div class="row">
					<div class="col-lg-12">
						<div class="row">

							<div class="col-xl-6">
								<div class="row">


								</div>
							</div><?php $pll = isset($pl) ? base64url_encode($pl) : base64url_encode("-")?></php>

							<div class="col-md-12 mb-lg-0 mb-4">
								<div class="card mt-4">
									<?php if ($this->session->userdata('role') != 3): ?>
										<div class="card-header pb-0 p-3">
											<div class="row">
												<div class="col-2">
													<a href="<?= base_url('Packing/history/'.$pll) ?>" style="font-size:2rem"><span
															class="fa fa-arrow-circle-left"></span></a>
												</div>
												<?php $ples = isset($pl) ? $pl : "-" ?>
												<div class="col-10 text-end">
													<a class="btn bg-gradient-danger mb-0 mx-3"
														href="<?= base_url() ?>FileGenerator/generatePDF/<?= $pl . "/" . rawurlencode($area) ?>"
														id="prt">Generate
														PDF</a>
													<a class="btn bg-gradient-success mb-0 mx-3"
														href="<?= base_url() ?>Packing/generateExcel/<?= $pl . "/" . rawurlencode($area) ?>"
														id="prt">Generate
														Excel</a>
													<?php if ($ples == "-"): ?>
														<a class="btn bg-gradient-success mb-0 mx-3" href="#" id="ipl">insert to
															PL</a>
													<?php endif; ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
									<div class="card-body p-3">
										<div class="row">
											<div class="col-md-6 mb-md-0 mb-4">
												<h6 class="mb-0">No Packing List</h6>
												<div
													class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">


													<h6 class="mb-0">
														<?= isset($pl) ? $pl : "-" ?>
													</h6>

												</div>
											</div>
											<div class="col-md-6">
												<h6 class="mb-0">Area</h6>
												<div
													class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">

													<h6 class="mb-0" id="area">
														<?= isset($area) ? $area : "-" ?>

														</h5>
													</h6>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="row my-4">
					<div class="col-lg">
						<div class="card mb-4">
							<div class="card-body px-0 pt-0 pb-2">
								<div class="table-responsive p-3">
									<table id="example3" class="table table-striped table align-items-center mb-0"
										style="width:100%">
										<thead>
											<tr class="text-center">
												<th></th>
												<th
													class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
													No</th>
												<th>id</th>
												<th
													class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
													No PO</th>
												<th
													class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
													No SJI</th>
												<th
													class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
													Penerima</th>
												<th
													class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
													Detail
												</th>
											</tr>
										</thead>
										<tbody>
											<?php $no = 0;
											$no = 0;
											foreach ($details as $key => $value) {
												# code<?=base_url()
												$dateIn = date_create($value->date_in);
												$dateNow = date_create(date("Y-m-d"));
												$diff = date_diff($dateIn, $dateNow);
												$df = $diff->format("%a");
												$trs = "";
												$tds = "";
												if (intval($df) >= 30) {
													$trs = "background-color: #e25f59;";
													$tds = "color:white";
												}
												$poid = $value->id_po;
												$nopo = isset($value->no_po) ? $value->no_po : "-";
												$nosji = isset($value->no_sji) ? $value->no_sji : "-";
												$nopl = isset($pl) ? $pl : "-";
												$areas = isset($area) ? $area : "-";
												// $id = isset($value->id) ? $value->id : "-";
												$rawpo = rawurlencode(str_replace('/', '_', $nopo));
												$rawpl = rawurldecode(str_replace('/', '_', $nopl));
												$rawsji = rawurlencode(str_replace('/', '_', $nosji));
												$no++;
												$base64Url = base64url_encode(json_encode($value));
												$dtval = str_replace("'", "`",json_encode($value)); 
												// echo $base64Url;
												?>
												<tr class="text-center" style="<?=$trs?>">
													<td style="<?=$tds?>"></td>
													<td style="<?=$tds?>"><?= $no ?></td>
													<td style="<?=$tds?>"><?= $poid ?></td>
													<td style="<?=$tds?>"><?= isset($value->no_po) ? $value->no_po : "-" ?></td>
													<td style="<?=$tds?>"><?= isset($value->no_sji) ? $value->no_sji : "-" ?></td>
													<td style="<?=$tds?>"><?= isset($value->corporate) ? $value->corporate : "-" ?></td>
													<td style="<?=$tds?>">
														<button class="btn bg-gradient-dark mb-0"
															onclick='getPO(<?= $dtval ?>)'>Detail</button>
														|
														<?php if ($this->session->userdata('role') != 3): ?>
															<a href="<?= base_url() ?>Packing/poEdit/<?= $base64Url ?>"><span
																	class="btn bg-gradient-warning mb-0">Edit</span></a>
														<?php endif; ?>|
														<?php if ($this->session->userdata('role') == 1): ?>
															<button class="btn btn-danger mb-0"
																onclick="removePODB('<?= $poid ?>')">Delete</button>

														<?php endif; ?>
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>

		<footer class="footer pt-3  ">
			<div class="container-fluid">
				<div class="row align-items-center justify-content-lg-between">
					<div class="col-lg-6 mb-lg-0 mb-4">
						<div class="copyright text-center text-sm text-muted text-lg-start">
							©
							<script>
								document.write(new Date().getFullYear())
							</script>,
							made with <i class="fa fa-heart"></i> by
							<a href="https://www.breathid.tech" class="font-weight-bold">Breath
								Tech
								Ind</a>
							for a better applications.
						</div>
					</div>

				</div>
			</div>
		</footer>
		</div>
	</main>

	<!---------------- Modal Start --------------->
	<div class="modal" id="myModal">
		<div class="modal-dialog modal-fullscreen">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">PO & Receveiver Informations</h4>
					<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
				</div>
				<div class="modal-body">
					<div class="container-fluid py-4" id="poDetails">
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="modal" id="insertPLModal">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Insert PL</h4>
					<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
				</div>
				<div class="modal-body">

					<div class="container-fluid py-4" id="pllist">

						<div class="row">

							<div class="col-6">
								<div class="col-md-12">
									<label>NO PL</label>
								</div>
								<div class="col-12 mb-1">
									<input name="bulkPL" type="text" class="form-control" id="bulkpl"
										placeholder="NO PL" value="">
								</div>
								<div class="col-md-12">
									<label>Shipping Method</label>
								</div>
								<div class="col-12 mb-1">
									<select class="form-select" aria-label="Default select example" name='shipping'
										id="shipping">
										<option value="" disabled selected>Shipping Method</option>
										<option value="Truck">Truck</option>
										<option value="Vessel">Vessel</option>
									</select>
								</div>
								<div class="col-md-12">
									<label>PO LIST</label>
								</div>
								<div class="col-12 mb-1">
									<textarea name="nopo" rows="10" class="form-control" id="nopo" placeholder="NO PO"
										readonly></textarea>
								</div>
							</div>
							<div class="col-6">
								<div class="col-md-12">
									<label>Nama Pengangkut</label>
								</div>
								<div class="col-12 mb-1">
									<input name="shippingName" type="text" class="form-control" id="shippingName"
										placeholder="Driver / Vessel Name" value="">
									<input name="pl_id" type="hidden" class="form-control" id="pl_id">
								</div>
								<div class="col-md-12">
									<label>Nomor Kendaraan</label>
								</div>
								<div class="col-12 mb-1">

									<input name="noKendaraan" type="text" class="form-control" id="noKendaraan"
										placeholder="Plate / No Container" value="">
								</div>
								<div class="col-md-12">
									<label>Nomor Pengangkut</label>
								</div>
								<div class="col-12 mb-1">
									<input type="text" name="noPengangkut" rows="10" class="form-control"
										id="noPengangkut" placeholder="No Segel / Driver Phone"></input>
								</div>
								<div class="col-md-12">
									<label>Nomor Voyage</label>
								</div>
								<div class="col-12 mb-1">
									<input type="text" name="noVoyage" rows="10" class="form-control" id="noVoyage"
										placeholder="NO Voyage"></input>
								</div>
								<div class="col-12 mb-1">
									<label for="validationTooltip01" class="form-label">Date Out</label>
									<input name="date_out" type="date" class="form-control" id="date_out"
										placeholder="Date Out" value="">
								</div>
								<div class="col-12 mb-1">
									<input type="checkbox" id="isLot" name="isLot" value="false" style="display: none;">

								</div>
								<div class="col-12 mb-1" style="display: none;" id="divTagihan">
									<label for="validationTooltip01" class="form-label">Tagihan</label>
									<input name="tagihan" type="text" class="form-control" id="tagihan"
										placeholder="Tagihan" value="">
								</div>

							</div>

							<div class="modal-footer mt-3">
								<button type="button" class="btn btn-success" data-bs-dismiss="modal"
									onclick="submitbulk()">Submit</button>
								<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>



	<!-------------- Modal End ------------>

	<!--   Core JS Files   -->
	<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
	<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
	<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
	<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
	<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
	<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
	<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
	<script src="<?= base_url() ?>/assets/js/moment.js"></script>
	<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
	<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
	<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>


	<script>
		var win = navigator.platform.indexOf('Win') > -1;
		if (win && document.querySelector('#sidenav-scrollbar')) {
			var options = {
				damping: '0.5'
			}
			Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
		}
	</script>
	<!-- Github buttons -->
	<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
	<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
	<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
	<script>
		var base_url = "<?php echo base_url(); ?>";
		var tbl = new DataTable('#example3', {
			columnDefs: [{
				orderable: true,
				render: DataTable.render.select(),
				targets: 0
			}],
			fixedColumns: {
				start: 2
			},
			order: [
				[1, 'asc']
			],
			paging: true,
			scrollCollapse: true,
			scrollX: true,
			// scrollY: 600,
			select: {
				style: 'os',
				selector: 'td:first-child'
			}
		});
		tbl.column(2).visible(false);
		var role = <?= $this->session->userdata('role') ?>;
		if (role == 3)
			tbl.column(0).visible(false);
		tbl.column(2).visible(false);

		function getPO(a) {
			var output = $('#poDetails');


			$.ajax({
				type: "POST",
				url: base_url + "/Packing/poDetails/",
				data: {
					po: a,
				},
				dataType: "text",
				cache: false,
				success: function (response) {
					$(output).html(response);
					// alert(response);
					$('#myModal').modal('show');
				}
			});
		}
		$('#ipl').on('click', function (e) {
			e.preventDefault();
			getSelected();
		});

		function removePODB(data) {

			Swal.fire({
				title: "Apakah anda akan menghapus data ini?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: base_url + "Packing/deletePO",
						data: {
							data: data
						},
						cache: false,
						success: function (response) {
							console.log(response);
							if (response == 'success') {
								Swal.fire("Data Deleted!", "", "info").then(function () {
									location.reload();
								});
							} else {
								Swal.fire("Gagal Delete Data!", "", "error");
							}
						}
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
		var po = [];
		var poids = [];

		function getSelected() {
			po = [];
			var area = '<?= base64url_encode($area) ?>';
			poids = [];
			$.ajax({
				type: "POST",
				url: base_url + "Packing/getlastPL/" + area,
				cache: false,
				success: function (response) {
					$('#bulkpl').val(response);
					$('#insertPLModal').modal('show');
					var count = tbl.rows('.selected').count();
					var checked_rows = tbl.rows('.selected').data().toArray();
					checked_rows.forEach(element => {
						po.push(element[3]);
						poids.push(element[2]);
					});
					$('#nopo').val(po.join("\n"));
				}
			});
			console.log(poids);
		}
		var lot = false;
		$("#isLot").change(function () {
			if (this.checked) {
				$("#divTagihan").css('display', 'block');
				lot = true;
			} else {
				$("#divTagihan").css('display', 'none');
				lot = false;
			}
		});
		$('#bulkpl').change(function () {
			var no_pl = $(this).val();
			var area = '<?= $area ?>';
			// alert(no_pl+"|"+area);
			console.log(poids);
			$.ajax({
				type: "POST",
				url: base_url + "Packing/checkPL",
				data: {
					no_pl: no_pl,
					area
				},
				cache: false,
				success: function (response) {
					if (response != "Not Exist") {
						console.log(response);
						// var json = jQuery.parseJSON(response);
						var dt = JSON.parse(response);
						$('#shipping').val(dt.method);
						$('#pl_id').val(dt.pl_id);
						$('#shippingName').val(dt.name);
						$('#noKendaraan').val(dt.no_vehicle);
						$('#noPengangkut').val(dt.no_shipping);
						$('#noVoyage').val(dt.no_voyage);
						var mydate = new Date(dt.date_out);
						var date = moment(mydate).format('YYYY-MM-DD')
						$('#date_out').val(date);
						$('#tagihan').val(dt.tagihan);
					}
				}
			});
		});

		function submitbulk() {
			var polist = $('#nopo').val();
			var area = $('#area').html().trim();
			var shipping = $('#shipping').val();
			var noPL = $('#bulkpl').val();
			var shippingName = $('#shippingName').val();
			var noKendaraan = $('#noKendaraan').val();
			var noPengangkut = $('#noPengangkut').val();
			var noVoyage = $('#noVoyage').val();
			var date_out = $('#date_out').val();
			var tagihan = $('#tagihan').val();
			var pl_id = $('#pl_id').val();
			var data = {
				noPL: noPL,
				poList: po,
				poNums: poids,
				area: area,
				shipping: shipping,
				noKendaraan: noKendaraan,
				noPengangkut: noPengangkut,
				noVoyage: noVoyage,
				lot: lot,
				date_out: date_out,
				shippingName: shippingName,
				tagihan: tagihan,
				pl_id: pl_id,
			};
			// console.log(data);

			if (noPL.length < 1) {
				alert("Mohon isi NO PL d!");
			} else if (polist.length < 1) {
				alert("Mohon Pilih Nomor PO di table!");
			} else if (shipping == null) {
				alert("Mohon Pilih shipping method!");
			} else {
				Swal.fire({
					title: "Apakah anda akan menambahkan data?",
					// showDenyButton: true,
					showCancelButton: true,
					confirmButtonText: "Submit",
					cancelButtonText: "Tunggu"
				}).then((result) => {
					/* Read more about isConfirmed, isDenied below */
					if (result.isConfirmed) {

						$.ajax({
							type: "POST",
							url: base_url + "Packing/bulkPL",
							data: data,
							cache: false,
							success: function (response) {
								console.log(response);
								if (response == "Success") {
									Swal.fire("Saved!", "", "success").then(function () {
										window.location = base_url;
									});
								} else {
									Swal.fire("Gagal Update Data!", "", "error").then(function () {
										window.location = base_url;
									});
								}
							}
						});
					} else {
						Swal.fire("Silahkan Periksa data kembali", "", "info");
					}
				});
			}
		}
	</script>
</body>

</html>
