<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>
				<div class="col-lg-12 px-3 text-end">
					<button class="btn bg-gradient-success mb-0 ms-auto col-lg-2 justify-content-end py-2 px-2"
						onclick='getSelected()'>Print
					</button>

				</div>

				<div class=" card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="example3" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th></th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th>ID</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PL</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PO</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No SJN
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Group</th>

									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Region</th>

									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Date Out
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Doc Return
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Unloading Date
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Action
									</th>
								</tr>
							</thead>
							<tbody>

								<?php $no = 0;
								foreach ($docret as $key => $value):
									# code...
									$dateIn = date_create($value->unloading_date);
									$dateNow = date_create(date("Y-m-d"));
									$diff = date_diff($dateIn, $dateNow);
									$df = $diff->format("%a");
									$trs = "";
									$tds = "";
									if (intval($df) >= 30) {
										$trs = "background-color: #e25f59;";
										$tds = "color:white";
									}
									$no++;
									?>
									<tr class="text-center" style="<?= $trs ?>">
										<td style="<?= $tds ?>"></td>
										<td style="<?= $tds ?>"><?= $no ?></td>
										<td style="<?= $tds ?>"><?= $value->id ?></td>
										<td style="<?= $tds ?>"><?= $value->no_pl ?></td>
										<td style="<?= $tds ?>"><?= $value->no_po ?></td>
										<td style="<?= $tds ?>"><?= $value->no_sji ?></td>
										<td style="<?= $tds ?>"><?= $value->list_group ?></td>
										<td style="<?= $tds ?>"><?= $value->area ?></td>
										<td style="<?= $tds ?>"><?= $value->date_out ?></td>
										<td style="<?= $tds ?>"><?= $value->doc_return ?></td>
										<td style="<?= $tds ?>"><?= $value->unloading_date ?></td>
										<td style="<?= $tds ?>">


											<button class="btn bg-gradient-dark mb-0"
												onclick='getPO(<?= json_encode($value); ?>)'>Detail
											</button>
											<button class="btn bg-gradient-danger mb-0"
												onclick='rejectPO(<?= json_encode($value); ?>)'>Reject
											</button>
											<!-- <?= json_encode($value) ?> -->
										</td>

									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</main>


<div class="modal" id="myModal">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">PO & Receveiver Informations</h4>
				<button type="button" class="btn bg-gradient-warning mb-0" data-bs-dismiss="modal">X</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid py-4" id="poDetails">
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<!-- ------ Modal ------------>
<div class="modal fade modal-lg" id="modalDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Add Invoice Informations</h5>
				<!-- <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button> -->
			</div>
			<form id="docForm" method="post" action="<?= base_url("Accounting/printinvoice") ?>">
				<div class="modal-body">
					<section class="section dashboard">
						<div class="col-md-12 position-relative">
							<div class="row">

								<div class="col-md-12 position-relative pb-3">
									<input name="poids" type="hidden" class="form-control" id="poids"
										placeholder="poids">
									<label for="validationTooltip01" class="form-label">Nama</label>
									<input name="nama" type="text" class="form-control" id="nama" placeholder="Nama">
								</div>
							</div>
						</div>
						<div class="col-md-12 position-relative pb-3">
							<div class="row">
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Invoice Date</label>
									<input name="invoice_date" type="date" class="form-control" id="invoice_date"
										placeholder="Invoice Date" value="">
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Invoice Number</label>
									<input name="invoice_number" type="text" class="form-control" id="invoice_number"
										placeholder="Invoice Number" value="">
								</div>
							</div>
						</div>
						<div class="col-md-12 position-relative pb-3">
							<div class="row">

								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Rekening</label>
									<select name="rekening_type" class="form-control" id="rekening_type">
										<option value="PPN">PPN</option>
										<option value="NON PPN">NON PPN</option>
									</select>
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Total Harga</label>
									<select name="cost_type" class="form-control" id="cost_type">
										<option value="Sudah PPN">Sudah PPN</option>
										<option value="Belum PPN">Belum PPN</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12 position-relative pb-3">
							<div class="row">

								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Diskon</label>
									<input name="diskon" type="text" class="form-control" id="diskon"
										placeholder="diskon">
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Down Payment</label>
									<input name="dp" type="text" class="form-control" id="dp" placeholder="dp" onchange="changenum(this)">
								</div>
							</div>
						</div>
						<div class="col-md-12 position-center pb-3">
							<div class="row">

								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Jumlah PPN</label>
									<input name="jppn" type="text" class="form-control" id="jppn"
										placeholder="Jumlah PPN " onchange="changenum(this)">
								</div>
								<div hidden id="perkiraanDiv" class="col-md-3 position-relative">
									<label for="validationTooltip01" class="form-label">perkiraan PPN</label>
									<input name="perkiraan" type="text" class="form-control" id="perkiraan" disabled>
								</div>
								<div hidden id="total_hargaDiv" class="col-md-3 position-relative">
									<label for="validationTooltip01" class="form-label">TOTAL HARGA</label>
									<input name="total_harga" type="text" class="form-control" id="total_harga"
										disabled>
								</div>
							</div>


						</div>

						<div class="col-md-12 position-relative pb-3">
							<div class="row">
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Biaya Lain-Lain</label>
									<input name="biayalain" type="text" class="form-control" id="biayalain"
										placeholder="nama">
								</div>
								<div class="col-md-6 position-relative mt-1">
									<label for="validationTooltip01" class="form-label"></label>
									<input name="nominallain" type="text" class="form-control" id="nominallain"
										placeholder="harga" onchange="changenum(this)">
								</div>
							</div>
						</div>
						<div class="col-md-12 position-center pb-3">
							<div class="row">

								<div class="col-md-3 position-relative">
									<label for="validationTooltip01" class="form-label">Pembulatan Kubikasi</label>
									<input name="pemKub" type="text" class="form-control" id="pemKub">
								</div>
								<div class="col-md-3 position-relative">
									<label for="validationTooltip01" class="form-label">Nilai Kubikasi</label>
									<input name="nilKub" type="text" class="form-control" id="nilKub" disabled>
								</div>
								<div class="col-md-6 position-relative">
									<label for="validationTooltip01" class="form-label">Ditagihkan Ke</label>
									<select name="tujuan" class="form-control" id="tujuan">
										<option value="penerima">Penerima</option>
										<option value="Pengirim">Pengirim</option>
									</select>
								</div>
							</div>

							<div class="col-md-12 position-relative">
								<div class="row">
									<div class="col-md-12 position-relative">
										<label for="validationTooltip01" class="form-label">PO List</label>
										<textarea name="po_list" type="text" class="form-control" id="po_list" rows='10'
											readonly></textarea>
									</div>
								</div>
							</div>





							<div class="col-md-0 position-relative mt-4">
								<button class="btn btn-primary" onclick="updatedoc(event)">SUBMIT</button>
							</div>
					</section>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal for editing data -->





<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>
<script src="<?= base_url() ?>assets/js/material.js"></script>
<script>
	var base_url = "<?php echo base_url(); ?>";
	var area = "";
</script>
<script>
	var tbl = new DataTable('#example3', {
		columnDefs: [{
			orderable: true,
			render: DataTable.render.select(),
			targets: 0
		}],
		fixedColumns: {
			start: 2
		},
		order: [
			[1, 'asc']
		],
		paging: true,
		scrollCollapse: true,
		scrollX: true,
		// scrollY: 600,
		select: {
			style: 'os',
			selector: 'td:first-child'
		}
	});
	$('#cost_type').on('change', function (e) {
		// alert(a)
		if ($(this).val() == "Belum PPN") {

			$("#perkiraanDiv").removeAttr('hidden');
			$("#total_hargaDiv").removeAttr('hidden');
		} else {
			$("#perkiraanDiv").attr('hidden', 'hidden');
			$("#total_hargaDiv").attr('hidden', 'hidden');
		}
	});
	$('#rekening_type').on('change', function (e) {
		// alert(a)
		pn = $(this).val();
		if(pn == "NON PPN"){
			$('#invoice_number').val('<?=$lnpn?>');
		}else{
			$('#invoice_number').val('<?=$lpn?>');
		}
	});

	tbl.column(2).visible(false);
	var po = [];
	var poids = [];

	function getSelected() {
		po = [];
		poids = [];
		$('#modalDoc').modal('show');
		var count = tbl.rows('.selected').count();
		var checked_rows = tbl.rows('.selected').data().toArray();
		checked_rows.forEach(element => {
			po.push(element[4]);
			poids.push(element[2]);
		});
		$('#po_list').val(po.join("\n"));
		var pojo = poids.join(",");
		$('#poids').val(pojo);
		getTotalHarga(pojo);
		// console.log(poids);
	}
	$(document).ready(function () {
		pn = $("#rekening_type").val();
		if(pn == "NON PPN"){
			$('#invoice_number').val('<?=$lnpn?>');
		}else{
			$('#invoice_number').val('<?=$lpn?>');
		}
		// $(".js-basic-multiple").select2({
		// 	dropdownParent: $('#exampleModalCenter .modal-content')

		// });
	});
	var idpo = 0;

	function editDoc() {
		// console.log(a);
		// var jsn = JSON.parse(a);
		// console.log(jsn.id);
		// idpo = jsn.id;
		// var mydate = new Date(jsn.date_in);
		// var date_in = moment(mydate).format('YYYY-MM-DD');
		// $('#no_po').val(jsn.no_po);
		// $('#no_sji').val(jsn.no_sji);
		$('#modalDoc').modal('show');
		// getPL(a, area);

	}

	function getTotalHarga(a) {
		$.ajax({
			type: "POST",
			url: base_url + "/Accounting/getPPN/",
			data: {
				poids: a,
			},
			dataType: "text",
			cache: false,
			success: function (response) {
				console.log(response);

				var jsn = JSON.parse(response);
				console.log(jsn.total_harga);

				$("#perkiraan").val(formatMoney(jsn.PPN));
				// $("#perkiraanDiv").removeAttr('hidden');
				$("#total_harga").val(formatMoney(jsn.total_harga));
				$("#nilKub").val(jsn.kub);
				// $("#total_hargaDiv").removeAttr('hidden');
			}
		});
	}

	function getPO(a) {
		var output = $('#poDetails');


		$.ajax({
			type: "POST",
			url: base_url + "/Packing/poDetails/",
			data: {
				po: a,
			},
			dataType: "text",
			cache: false,
			success: function (response) {
				$(output).html(response);
				// alert(response);
				$('#myModal').modal('show');
			}
		});
	}

	function rejectPO(a) {
		var output = $('#poDetails');
		Swal.fire({
			title: "Apakah anda akan Mereject data ini?",
			// showDenyButton: true,
			showCancelButton: true,
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak"
		}).then((result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				$.ajax({
					type: "POST",
					url: base_url + "/Packing/rejectDoc",
					data: {
						po: a,
					},
					dataType: "text",
					cache: false,
					success: function (response) {
						console.log(response);
						if (response == "success") {
							Swal.fire("Saved!", "", "success").then(function () {
								location.reload();
							});
						} else {
							Swal.fire("Gagal Update Data!", "", "error").then(function () {
								location.reload();
							});
						}
					}
				});
			} else {
				Swal.fire("Silahkan Periksa data kembali", "", "info");
			}
		});


	}
	
	function formatMoney(number) {
		return parseInt(number).toLocaleString('in-ID', {
			style: "currency",
			currency: "IDR",
			minimumFractionDigits: 0,
			maximumFractionDigits: 0
		});

	}
	function changenum(a) {
		$(a).val(formatMoney($(a).val()));
	}
	function updatedoc(e) {
		e.preventDefault();
		var invDate = $('#invoice_date').val();
		var invoiceNumber = $('#invoice_number').val();
		if (invDate.length < 1) {
			alert("Mohon isi Invoice Date!");
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				var form_data = $('#docForm');
				console.log(form_data);
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					form_data.submit();
					setTimeout(function () {
						window.location = base_url + "/Packing/getCollection";
					}, 2000);

				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>

