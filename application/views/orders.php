<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!-- begin::Head -->

<head><!--begin::Base Path (base relative path for assets of this page) -->
    <!--end::Base Path -->
    <meta charset="utf-8" />

    <title>JASTIP MAM</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="<?= base_url('assets/css/demo1/pages/login/login-1.css') ?>" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href='<?= base_url("assets/vendors/global/vendors.bundle.css") ?>' rel="stylesheet" type="text/css" />
    <link href='<?= base_url("assets/css/demo1/style.bundle.css") ?>' rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href='<?= base_url("assets/css/demo1/skins/header/base/light.css") ?>' rel="stylesheet" type="text/css" />
    <link href='<?= base_url("assets/css/demo1/skins/header/menu/light.css") ?>' rel="stylesheet" type="text/css" />
    <link href='<?= base_url("assets/css/demo1/skins/brand/dark.css") ?>' rel="stylesheet" type="text/css" />
    <link href='<?= base_url("assets/css/demo1/skins/aside/dark.css") ?>' rel="stylesheet" type="text/css" />
    <!--end::Layout Skins -->

    <link rel="shortcut icon" href='<?= base_url("assets/media/logos/favicon.ico") ?>' />
</head>
<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">


    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside"
                    style="background-image: url(assets/media//bg/bg-4.jpg);">
                    <div class="kt-grid__item">
                        <a href="#" class="kt-login__logo">
                            <img src='<?= base_url("assets/media/logos/logo-4.png") ?>'>
                        </a>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Selamat Datang di JastipMAM &#10084;</h3>
                            <h4 class="kt-login__subtitle">Yuk input data pesanan kamu di sini ya! </h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                &copy 2023 MFN
                            </div>
                            <div class="kt-login__menu">
                                <a href="#" class="kt-link">Privacy</a>
                                <a href="#" class="kt-link">Legal</a>
                                <a href="<?=base_url()?>login" class="kt-link">Portal</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--begin::Aside-->

                <!--begin::Content-->
                <div
                    class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
                    <!--begin::Body-->
                    <div class="kt-login__body">

                        <!--begin::Signin-->
                        <div class="kt-login__form">
                            <div class="kt-login__title">
                                <h3>ORDER JASTIP</h3>
                            </div>

                            <!--begin::Form-->
                            <?php echo form_open_multipart('orders/order'); ?>
                            <form class="kt-form" id="orders">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" placeholder="Nama" name="name"
                                        autocomplete="off" pattern=".{2,100}" required>
                                </div>
                                <div class="form-group form-group-last">
                                    <label>Alamat</label>
                                    <textarea class="form-control" id="address" rows="3" placeholder="alamat"
                                        name="address" required></textarea>
                                </div>
                                <div class="form-group" style="margin-top: 20px;">
                                    <label>No. Handphone</label>
                                    <input class="form-control" type="text" placeholder="08XX-XXXX-XXXX" name="hp"
                                        id="hp" autocomplete="off" pattern=".{10,16}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleSelect1">Jasa Pengiriman</label>
                                    <select class="form-control" id="jasa" name="jasa">
                                        <option>Lion Parcel</option>
                                        <option>Gosend</option>
                                    </select>
                                </div>
                                <div class="form-group">

                                    <label>Upload Pesanan</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="order" name="order[]" multiple>
                                        <label class="custom-file-label" for="order">Pilih File</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Upload Bukti Pembayaran</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="prove" name="prove[]" multiple
                                            required>
                                        <label class="custom-file-label" for="prove">Pilih File</label>
                                    </div>
                                </div>
                                <!--begin::Action-->
                                <div class="kt-login__actions">
                                    <button id="kt_login_signin_submit"
                                        class="btn btn-primary btn-elevate kt-login__btn-primary">Submit</button>
                                </div>
                                <!--end::Action-->
                            </form>
                            <?php
                            echo form_close();
                            ?>
                            <!--end::Form-->
                        </div>
                        <!--end::Signin-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Content-->
            </div>
        </div>



    </div>

    <!-- end:: Page -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = { "colors": { "state": { "brand": "#5d78ff", "dark": "#282a3c", "light": "#ffffff", "primary": "#5867dd", "success": "#34bfa3", "info": "#36a3f7", "warning": "#ffb822", "danger": "#fd3995" }, "base": { "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"], "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"] } } };
    </script>
</body>
<!-- end::Body -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js" type="text/javascript"></script>
<script type="application/javascript">

    $('input[type="file"]').change(function (e) {
        var fileNames = e.target.files;
        var fnames = "";
        if (fileNames.length < 2) {
            for (const fname of fileNames) {
                console.log(fname);
                fnames += fname.name;
            }
        } else {
            fnames = fileNames.length + " File Dipilih";
        }

        $(e.target).siblings('.custom-file-label').text(fnames);
    });
    $('#hp')

        .keydown(function (e) {
            var key = e.which || e.charCode || e.keyCode || 0;
            $phone = $(this);


            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                if ($phone.val().length === 1 && ($phone.val() != '0' && $phone.val() != '6')) {
                    $phone.val('');
                }
                if ($phone.val().length === 2) {
                    if ($phone.val().charAt(0) == '0') {
                        if ($phone.val().charAt(1) != '8') {
                            $phone.val('0');
                        }
                    } else {
                        if ($phone.val().charAt(1) != '2') {
                            $phone.val('6');
                        }
                    }
                }
                if ($phone.val().length === 4) {
                    $phone.val($phone.val() + '-');
                }
                if ($phone.val().length === 9) {
                    $phone.val($phone.val() + '-');
                }
                // if ($phone.val().length === 9) {
                // 	$phone.val($phone.val() + '-');
                // }
            }

            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        })

        .bind('focus click', function () {
            $phone = $(this);

            // if ($phone.val().length === 0) {
            // 	$phone.val('(');
            // }
            // else {
            // 	var val = $phone.val();
            // 	$phone.val('').val(val); // Ensure cursor remains at the end
            // }
        })

        .blur(function () {
            $phone = $(this);

            if ($phone.val() === '(') {
                $phone.val('');
            }
        });
</script>

</html>