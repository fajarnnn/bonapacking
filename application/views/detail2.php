<!--
=========================================================
* Soft UI Dashboard - v1.0.7
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard
* Copyright 2023 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>/assets/img/bona.png">
    <title>
        Bona
    </title>
    <!--     Fonts and icons     -->
    <link href="<?= base_url() ?>/assets/css/fontgoogle.css" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="<?= base_url() ?>/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="<?= base_url() ?>/assets/js/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="<?= base_url() ?>/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="<?= base_url() ?>/assets/css/soft-ui-dashboard.css?v=1.0.7" rel="stylesheet" />
    <!-- Nepcha Analytics (nepcha.com) -->
    <!-- Nepcha is a easy-to-use web analytics. No cookies and fully compliant with GDPR, CCPA and PECR. -->
    <!-- <script defer data-site="YOUR_DOMAIN_HERE" src="https://api.nepcha.com/js/nepcha-analytics.js"></script> -->
</head>

<body class="g-sidenav-show  bg-gray-100">

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->

        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">

                        <div class="col-xl-6">
                            <div class="row">


                            </div>
                        </div>
                        <div class="col-md-12 mb-lg-0 mb-4">
                            <div class="card mt-4">
                                <div class="card-header pb-0 p-3">
                                    <div class="row">

                                        <div class="col-12 text-end">
                                            <a class="btn bg-gradient-danger mb-0"
                                                href="<?= base_url() ?>Packing/pdf_gen/<?= $details[0]->no_po . "/" . $details[0]->id ?>">Generate
                                                Report</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-3">
                                    <div class="row">
                                        <div class="col-md-6 mb-md-0 mb-4">
                                            <h6 class="mb-0">No Packing List</h6>
                                            <div
                                                class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">


                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->no_pl) ? $details[0]->no_pl : "-" ?>
                                                </h6>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h6 class="mb-0">Area</h6>
                                            <div
                                                class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">

                                                <h6 class="mb-0">
                                                    <?= isset($details[0]->area) ? $details[0]->area : "-" ?>

                                                    </h5>
                                                </h6>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6 mt-4">
                    <div class="card">
                        <div class="card-header pb-0 px-3">
                            <h6 class="mb-0">PO Information</h6>
                        </div>
                        <div class="card-body pt-4 p-3">
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-3 text-sm">
                                            <?= isset($details[0]->no_po) ? $details[0]->no_po : "-" ?></h6>
                                        <span class="mb-3 text-xs">Supplier
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->supplier) ? $details[0]->supplier : "-" ?></span></span>
                                        <span
                                            class="mb-3 text-xs">Material&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            : <span class="text-dark ms-sm-2 font-weight-bold">- <?php foreach ($details as $key => $value) {

                                                                                                    ?>
                                                <p class="text-sm">
                                                    <?= isset($value->material) ? $value->material : "-" ?></p>
                                                <?php } ?>
                                                <?php foreach ($details as $key => $value) {
                                                ?>
                                                <p class="text-sm"><?= isset($value->qty) ? $value->qty : "-" ?></p>
                                                <?php } ?> <br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
                                                <?php foreach ($details as $key => $value) {

                                                ?>
                                                <p class="text-sm">
                                                    <?= isset($value->material) ? $value->material : "-" ?></p>
                                                <?php } ?>
                                                <?php foreach ($details as $key => $value) {
                                                ?>
                                                <p class="text-sm"><?= isset($value->qty) ? $value->qty : "-" ?></p>
                                                <?php } ?><br>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
                                                <?php foreach ($details as $key => $value) {

                                                ?>
                                                <p class="text-sm">
                                                    <?= isset($value->material) ? $value->material : "-" ?></p>
                                                <?php } ?>
                                                <?php foreach ($details as $key => $value) {
                                                ?>
                                                <p class="text-sm"><?= isset($value->qty) ? $value->qty : "-" ?></p>
                                                <?php } ?>
                                            </span></span>
                                        <span
                                            class="mb-3 text-xs">Dimensi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span class="text-dark font-weight-bold ms-sm-2">P.&nbsp;&nbsp;&nbsp;<?php foreach ($details as $key => $value) {

                                                                                                                    ?>
                                                <p class="text-sm"><?= isset($value->panjang) ? $value->panjang : "-" ?>
                                                </p>
                                                <?php } ?>
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;L.&nbsp;&nbsp;&nbsp;<?php foreach ($details as $key => $value) {

                                                                                                                                                                                                                                                                ?>
                                                <p class="text-sm"><?= isset($value->lebar) ? $value->lebar : "-" ?></p>
                                                <?php } ?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T.&nbsp;&nbsp;&nbsp;<?php foreach ($details as $key => $value) {

                                                                                                                                                                                                                                                                    ?>
                                                <p class="text-sm"><?= isset($value->tinggi) ? $value->tinggi : "-" ?>
                                                </p>
                                                <?php } ?>
                                            </span></span>
                                        <span class="mb-3 text-xs">Kubikasi (m3)&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?php foreach ($details as $key => $value) {

                                                                                                                                                ?>
                                                <h5 class="text-sm mb-0">
                                                    <?= isset($value->kubikasi) ? $value->kubikasi : "-" ?>
                                                </h5>
                                                <?php } ?>
                                            </span></span>
                                        <span
                                            class="mb-3 text-xs">Berat(Kg)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?php foreach ($details as $key => $value) {

                                                                                                                                                                                            ?>
                                                <h5 class="text-sm mb-0">
                                                    <?= isset($value->berat) ? $value->berat : "-" ?>
                                                </h5>
                                                <?php } ?>
                                            </span></span>
                                        <span
                                            class="mb-3 text-xs">Quantity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark ms-sm-2 font-weight-bold">1</span></span>
                                        <span
                                            class="mb-3 text-xs">Satuan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark ms-sm-2 font-weight-bold"><?php foreach ($details as $key => $value) {

                                                                                                                                                                                                                ?>
                                                <p class="text-sm"><?= isset($value->satuan) ? $value->satuan : "-" ?>
                                                </p>
                                                <?php } ?>
                                            </span></span>
                                        <span class="mb-3 text-xs">Date
                                            In&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_in) ? $details[0]->date_in : "-" ?></span></span>
                                        <span class="mb-3 text-xs">Date
                                            Out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->date_out) ? $details[0]->date_out : "-" ?></span></span>
                                    </div>

                                    <div class="ms-auto">
                                        <h6 class="mb-3 text-sm">
                                            <?= isset($details[0]->no_sji) ? $details[0]->no_sji : "-" ?></h6>
                                        <span
                                            class="mb-3 text-xs">Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?php foreach ($details as $key => $value) {

                                                                                                                                                                                                                                        ?>
                                                <h5 class="text-sm mb-0">
                                                    <?= isset($value->cost) ? $value->cost : "-" ?>
                                                </h5>
                                                <?php } ?>/m3
                                                xxx/Kg
                                            </span></span>
                                        <br>
                                        <span
                                            class="mb-3 text-xs">Tagihan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2">RP 22.010/m3 xxx/Kg
                                            </span></span>
                                        <br>
                                        <span class="mb-3 text-xs">No
                                            POT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->no_pot) ? $details[0]->no_pot : "-" ?></span></span>
                                        <br>
                                        <span
                                            class="mb-3 text-xs">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->remark) ? $details[0]->remark : "-" ?></span></span>
                                        <br>
                                        <span class="mb-3 text-xs">Inv
                                            Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->invoice_date) ? $details[0]->invoice_date : "-" ?></span>
                                        <br>
                                        <span class="mb-3 text-xs">Inv
                                            No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->no_invoice) ? $details[0]->no_invoice : "-" ?></span>

                                    </div>



                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="card h-100">
                        <div class="card-header pb-0 px-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6 class="mb-0">Receiver & Shipping Informations</h6>
                                </div>

                            </div>
                        </div>
                        <div class="card-body pt-4 p-3">
                            <ul class="list-group">
                                <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-3 text-sm">4506441542</h6>
                                        <span class="mb-3 text-xs">Penerima
                                            &nbsp;&nbsp;: <span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->receiver) ? $details[0]->receiver : "-" ?>
                                                </p></span></span>
                                        <span class="mb-3 text-xs">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            : <span
                                                class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->address) ? $details[0]->address : "-" ?>
                                            </span></span>

                                        <span
                                            class="mb-3 text-xs">Jalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            : <span class="text-dark ms-sm-2 font-weight-bold">
                                                <p class="text-sm">
                                                    <?= isset($details[0]->street) ? $details[0]->street : "-" ?>
                                            </span></span>
                                        <span
                                            class="mb-3 text-xs">PIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span class="text-dark ms-sm-2 font-weight-bold">
                                                <p class="text-sm">
                                                    <?= isset($details[0]->pic) ? $details[0]->pic : "-" ?></p> |
                                                085268845886
                                            </span></span>
                                        <span
                                            class="mb-3 text-xs">Plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span
                                                class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->plant) ? $details[0]->plant : "-" ?></span></span>
                                        <span class="mb-3 text-xs">Doc Return&nbsp;:<span
                                                class="text-dark ms-sm-2 font-weight-bold"><?= isset($details[0]->doc_return) ? $details[0]->doc_return : "-" ?></span></span>
                                    </div>
                                    <div class="ms-auto">
                                        <h6 class="mb-3 text-sm">SJ.NO.MSI/SH/0/0/0/0</h6>
                                        <span class="text-xs">Transport Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2">Truck</span></span>
                                        <br>
                                        <span
                                            class="text-xs">Driver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->driver) ? $details[0]->driver : "-" ?>
                                            </span></span>
                                        <br>
                                        <span
                                            class="mb-2 text-xs">Plate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                            <span
                                                class="text-dark font-weight-bold ms-sm-2">-</span><?= isset($details[0]->plate) ? $details[0]->plate : "-" ?></span>
                                        <br>
                                        <span
                                            class="mb-2 text-xs">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->phone) ? $details[0]->phone : "-" ?></span></span>
                                        <br>
                                        <span
                                            class="mb-2 text-xs">Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span
                                                class="text-dark font-weight-bold ms-sm-2"><?= isset($details[0]->list_group) ? $details[0]->list_group : "-" ?></span></span>

                                    </div>



                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

            </div>

            <footer class="footer pt-3  ">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-lg-between">
                        <div class="col-lg-6 mb-lg-0 mb-4">
                            <div class="copyright text-center text-sm text-muted text-lg-start">
                                © <script>
                                document.write(new Date().getFullYear())
                                </script>,
                                made with <i class="fa fa-heart"></i> by
                                <a href="https://www.breathid.tech" class="font-weight-bold" target="_blank">Breath Tech
                                    Ind</a>
                                for a better applications.
                            </div>
                        </div>

                    </div>
                </div>
            </footer>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="../assets/js/core/popper.min.js"></script>
    <script src="../assets/js/core/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="../assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
    </script>
    <!-- Github buttons -->
    <script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>