<?php foreach ($images as $key => $value):
	# code..
	$no = 0;
	if (strpos($value->mime, "image") !== false):
		$no++;
		?>
		<div class="upload__img-box" id="<?= $value->id ?>_exist">
			<div data-number="<?= $value->id ?>_exist" data-file="<?= $value->id ?>_exist" class="img-bg"
				style="background-image: url('<?= base_url($value->path . $value->filename) ?>')">
				<div class="upload__img-close">x</div>
			</div>
		</div>
		<?php
	elseif (strpos($value->mime, "video") !== false): ?>
		<div class="upload__img-box" id="<?= $value->id ?>_exist">
			<div data-number="<?= $value->id ?>_exist" data-file="<?= $value->id ?>_exist" class="img-bg">
				<video id="myvideo" width="100%" controls>
					<source src="<?= base_url($value->path . $value->filename) ?>" type="video/mp4">
				</video>
				<div class="upload__img-close">x</div>
			</div>
		</div>
	<?php endif;
endforeach; ?>
