<div class="row">
	<div class="col-md-6 mt-4">
		<div class="card">
			<div class="card-header pb-0 px-3">
				<h6 class="mb-0">PO Information</h6>
			</div>
			<?php $roleid = $this->session->userdata('role'); ?>
			<div class="card-body pt-4 p-3 ">
				<!-- sini -->
				<ul class="list-group">
					<li class="list-group-item border-0 d-flex p-0 mb-2 bg-gray-100 border-radius-lg">
						<div class="d-flex flex-column px-3 py-3">
							<h6 class="mb-3 text-sm">
								<?= isset($details->no_po) ? $details->no_po : "-" ?>
							</h6>
							<span class="mb-3 text-xs">Supplier
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
								<span class="text-dark font-weight-bold ms-sm-2">
									<?= isset($details->supplier) ? $details->supplier : "-" ?>
								</span></span>



							<span class="mb-3 text-xs">Date
								In&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold">
									<?= isset($details->date_in) && !strpos($details->date_in, '1970') ? $details->date_in : "-" ?>
								</span></span>
							<span class="mb-3 text-xs">Date
								Out&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold">
									<?= isset($shipping['date_out']) && !strpos($shipping['date_out'], '1970') ? $shipping['date_out'] : "-" ?>
								</span></span>

						</div>
						<div class="mx-auto px-3 py-3">
							<h6 class="mb-3 text-sm">
								<?= isset($details->no_sji) ? $details->no_sji : "-" ?>
							</h6>
							<?php $tottot = 0;
							foreach ($material as $key => $value) {
								if (is_numeric($value->totalcost))
									$tottot += $value->totalcost;
							}
							?>
							<span class="mb-1 text-xs">Tagihan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">Rp.
									<?= number_format($tottot, 0, '', '.'); ?>
								</span></span>
							<br>
							<span class="mb-3 text-xs">No
								POT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">
									<?= isset($details->no_pot) ? $details->no_pot : "-" ?>
								</span></span>
							<br>
							<span class="mb-3 text-xs">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2">
									<?= isset($details->remark) ? $details->remark : "-" ?>
								</span></span>
							<br>
							<span class="mb-3 text-xs">Inv
								Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"></span>
								<?= isset($details->invoice_date) && !strpos($details->invoice_date, '1970') ? $details->invoice_date : "-" ?>
							</span>
							<br>
							<span class="mb-3 text-xs">Inv
								No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"></span>
								<?= isset($details->no_invoice) ? $details->no_invoice : "-" ?>
							</span>
							<br>
							<span class="mb-3 text-xs">Payment
								Date&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"></span>
								<?= isset($details->payment_date) ? $details->payment_date : "-" ?>
							</span>
						</div>
					</li>
				</ul>
			</div>

		</div>

	</div>


	<div class="col-md-6 mt-4">
		<div class="card h-100">
			<div class="card-header pb-0 px-3">
				<div class="row">
					<div class="col-md-12">
						<h6 class="mb-0">Receiver & Shipping Informations</h6>
					</div>

				</div>
			</div>
			<div class="card-body pt-4 p-3">
				<ul class="list-group">
					<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
						<div class="d-flex flex-column">

							<span class="mb-3 text-xs">Penerima
								&nbsp;&nbsp;: <span class="text-dark font-weight-bold ms-sm-2"><?= isset($details->corporate) ? $details->corporate : "-" ?>
									</p></span></span>
							<span class="mb-3 text-xs">Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								: <span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details->address) ? $details->address : "-" ?>
								</span></span>

							<span class="mb-3 text-xs">Jalan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								: <span class="text-dark ms-sm-2 font-weight-bold">

									<?= isset($details->street) ? $details->street : "-" ?>
								</span></span>
							<span class="mb-3 text-xs">PIC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
								<span class="text-dark ms-sm-2 font-weight-bold">

									<?= isset($details->pic) ? $details->pic : "-" ?>
								</span></span>
							<span class="mb-3 text-xs">Plant&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
								<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details->plant) ? $details->plant : "-" ?></span></span>
							<span class="mb-3 text-xs">Doc Return&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details->doc_return) && !strpos($details->doc_return, '1970') ? $details->doc_return : "-" ?></span></span>
							<span class="mb-3 text-xs">Unloading Date&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold"><?= isset($details->unloading_date) && !strpos($details->unloading_date, '1970') ? $details->unloading_date : "-" ?></span></span>
							<!-- <span class="mb-3 text-xs">Unloading
								Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark ms-sm-2 font-weight-bold">
									<?= isset($shipping['unloading_date']) && !strpos($shipping['unloading_date'], '1970') ? $shipping['unloading_date'] : "-" ?>
								</span></span> -->
						</div>
						<div class="mx-auto">

							<span class="text-xs">Transport Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= strtoupper($shipping['method']) ?></span></span>
							<br>
							<?php if ($roleid != 3) : ?>
								<span class="text-xs">Driver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
									<span class="text-dark font-weight-bold ms-sm-2"><?= $shipping['name'] ?>
									</span></span>
								<br>
								<span class="mb-2 text-xs">Plate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
									<span class="text-dark font-weight-bold ms-sm-2"></span><?= isset($shipping['no_vehicle']) ? $shipping['no_vehicle'] : "" ?></span>
								<br>
								<span class="mb-2 text-xs">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= $shipping['no_shipping'] ?></span></span>
								<br>
							<?php endif; ?>
							<span class="mb-2 text-xs">Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<span class="text-dark font-weight-bold ms-sm-2"><?= isset($details->list_group) ? $details->list_group : "-" ?></span></span>
						</div>
					</li>

				</ul>

			</div>


		</div>

	</div>

	<div class="col-md-12">
		<div class="row mt-4">
			<div class="col-lg-6 mb-lg-0 mb-4">
				<div class="card">
					<div class="card-body p-3">
						<h6>Pictures</h6>
						<div class="row">

							<?php $no = 0;
							foreach ($images as $key => $value) :
								# code...
								if (strpos($value->mime, "image") !== false) :
									$no++;
							?>
									<div class="col-lg-4">
										<img id="myImg<?= $no ?>" src="<?= base_url($value->path . $value->filename) ?>" alt="Snow" style="width:100%;max-width:300px;padding:0.2rem" onclick="showImg(this.id)">
									</div>
							<?php endif;
							endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 mb-lg-0 mb-4">
				<div class="card">
					<div class="card-body p-3">
						<h6>Videos</h6>
						<div class="row">
							<?php foreach ($images as $key => $value) :
								if (strpos($value->mime, "video") !== false) :
									// $nov++;
							?>
									<div class="col-lg-4">
										<video id="myvideo" width="100%" poster="<?= base_url("/assets/img/bona.png") ?>" controls>
											<source src="<?= base_url($value->path . $value->filename) ?>" type="video/mp4">
										</video>
									</div>
							<?php endif;
							endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12 mt-4">
		<div class="card">
			<div class="card-header pb-0 px-3">
				<h6 class="mb-0">Material Information</h6>
			</div>
			<div class="card-body pt-4 p-3">
				<div class="table-responsive p-3">
					<table id="tabledet" class="table table-striped table align-items-center mb-0" style="width:100%">
						<thead>
							<tr class="text-center">
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									No</th>
								<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Material</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Panjang
								</th>
								<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Lebar</th>
								<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
									Tinggi</th>
								<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
									Kubikasi</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Berat
								</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Qty
								</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Satuan
								</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Tonase
								</th>
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Cost
								</th>
								<!-- <th
									class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Cost Bill
								</th> -->
								<th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
									Total Cost
								</th>

							</tr>
						</thead>
						<tbody>
							<?php $no = 1;
							$totcol = 0;
							$totcost = 0;
							$tottot = 0;
							$tonton = 0;
							$totkub = 0.00;
							$totber = 0;
							foreach ($material as $key => $value) :
								if (is_numeric($value->qty))
									$totcol += $value->qty;
								if (is_numeric($value->cost))
									$totcost += $value->cost;
								if (is_numeric($value->totalcost))
									$tottot += $value->totalcost;
								if (is_numeric($value->kubikasi))
									$totkub += $value->kubikasi;
								if (is_numeric($value->berat))
									$totber += $value->berat;
								if (is_numeric($value->berat) && is_numeric($value->qty))
									$tonase = isset($value->tonase) ? $value->tonase : ($value->berat * $value->qty);
								else {
									$tonase = 0;
								}
								$tonton += $tonase;
							?>
								<tr class="text-center">
									<td><?= $no ?></td>
									<td><?= $value->material ?></td>
									<td><?= $value->panjang ?></td>
									<td><?= $value->lebar ?></td>
									<td><?= $value->tinggi ?></td>
									<td><?= is_numeric($value->kubikasi) ? number_format($value->kubikasi, 3, ',', ' ') : 0 ?>
									</td>
									<td><?= $value->berat ?></td>
									<td><?= $value->qty ?></td>
									<td><?= $value->satuan ?></td>
									<td><?= $tonase ?></td>
									<td><?= is_numeric($value->cost) ? number_format($value->cost, 0, '', '.') : 0 ?></td>
									<!-- <td><?= $value->costbill ?></td> -->
									<td><?= is_numeric($value->totalcost) ? number_format($value->totalcost, 0, '', '.') : 0 ?>
									</td>

								</tr>
							<?php $no++;
							endforeach; ?>

						</tbody>
						<tfoot>
							<tr class="text-center">
								<td colspan="5" style="font-weight: bolder;">TOTAL</td>
								<td style="font-weight: bolder;"><?= number_format($totkub, 3, ',', ' '); ?></td>
								<td style="font-weight: bolder;"></td>
								<td style="font-weight: bolder;"><?= $totcol ?></td>
								<td style="font-weight: bolder;">COLLY</td>
								<td style="font-weight: bolder;"><?= number_format($tonton, 0, '', '.'); ?></td>
									<td style="font-weight: bolder;"></td>
									<!-- <td></td> -->
									<td style="font-weight: bolder;"><?= number_format($tottot, 0, '', '.'); ?></td>
							</tr>
						</tfoot>
					</table>

				</div>
			</div>
		</div>
	</div>


</div>

<!-- The Modal Image -->
<div id="myInput" class="input">
	<span class="close">&times;</span>
	<img class="input-content" id="img01">
	<div id="caption"></div>
</div>


<!-- Modal -->
<div class="modal fade" id="vidExample" tabindex="-1" role="dialog" aria-labelledby="vidExampleLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">


				</div>
				<div class="modal-body">
					<?php foreach ($images as $key => $value) :
						if (strpos($value->ext, "mp4")) {
					?>
							<video id="myvideo" width="100%" controls>
								<source src="<?= base_url() ?>assets/img/1.mp4" type="video/mp4">
							</video>
					<?php }
					endforeach; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- <script src="js/index.js"></script> -->
<script>
	$('#vidExample').modal();
	var modal = document.getElementById("myInput");

	function showImg(ele) {
		// ele = ele.toString();
		// Get the image and insert it inside the modal - use its "alt" text as a caption
		var img = document.getElementById(ele);
		var modalImg = document.getElementById("img01");
		var captionText = document.getElementById("caption");

		modal.style.display = "block";
		modalImg.src = img.src;
		captionText.innerHTML = img.alt;

	}


	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}
	new DataTable('#tabledet');
	$(document).ready(function() {
		// 	// $('#vidsss').on('click', function(e){
		// 	// 	$('#vidExample').modal();
		// 	// });
		// 			//play/pause on double click on the video
		// 			$('video').on('click', function (e) {
		// 				if (this.paused) {
		// 					this.play();
		// 				} else {
		// 					this.pause();
		// 				}
		// 			});

		// 			// play / pause of the video when the modal opens / closes
		// 			$('.modal').on('show.bs.modal', function () { //show modal event for an element which has class 'modal'
		// 				var id = $(this).attr('id'); //saves in the var the ID value of the opened modal
		// 				var video = document.getElementById(id).querySelectorAll(
		// 					"video"); //Find the element 'video' inside of the modal defined by the ID previosly saved

		// 				$(video)[0].play(); //plays what we saved on 'video' variable
		// 			});

		// 			$('.modal').on('hidden.bs.modal', function () { //show modal event for an element which has class 'modal'
		// 				var id = $(this).attr('id'); //saves in the var the ID value of the closed modal
		// 				var video = document.getElementById(id).querySelectorAll(
		// 					"video"); //Find the element 'video' inside of the modal defined by the ID previosly saved

		// 				$(video)[0].pause(); //pauses the video
		// 				$(video)[0].currentTime =
		// 					0; //rests the video to 0 for have it from the beging when the user opens the modal again
		// 			});

		$('#vidExample').modal();
	});
</script>
