<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
	<!-- Navbar -->

	<!-- End Navbar -->
	<div class="row my-4">
		<div class="col-lg">
			<div class="card mb-4">
				<div class="card-header pb-0">
					<h6>Packing List</h6>
				</div>


				<div class="card-body px-0 pt-0 pb-2">
					<div class="table-responsive p-3">
						<table id="example3" class="table table-striped table align-items-center mb-0"
							style="width:100%">
							<thead>
								<tr class="text-center">
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PL</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No PO
									</th>
									<th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										No SJN</th>

									<th
										class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
										Group</th>

									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Region
									</th>
									<th
										class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
										Action
									</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 0;
								foreach ($all as $key => $value):
									$no++;
									$sji = base64url_encode($value->no_sji);
									$po = base64url_encode($value->id_pol);
								?>
									<tr class="text-center">

										<td><?= $no ?></td>
										<td><?= $value->no_pl ?></td>
										<td><?= $value->no_po ?></td>
										<td><?= $value->no_sji ?></td>
										<td><?= $value->list_group ?></td>
										<td><?= $value->area ?></td>
										<td>
											<?php if ($this->session->userdata('role') != 3): ?>
												<a class="btn bg-gradient-success mb-0"
													href="<?= base_url('FileGenerator/printSjn/' . $sji . '/print/'.$po) ?>">Download
												</a>
											<?php endif; ?>
										</td>

									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>
</main>


<!-- ------ Modal ------------>
<!-- <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Add Doc Return</h5>
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<section class="section dashboard">

						<form class="row g-3 needs-validation  pt-0 justify-content-center" novalidate action="submit" method="post" enctype="multipart/form-data">



							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">No PO</label>
								<input name="no_po" type="text" class="form-control" id="validationTooltip04" placeholder="No PO" readonly>

							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">No SJN</label>
								<input name="no_sjn" type="text" class="form-control" id="validationTooltip04" placeholder="No SJN" readonly>

							</div>

							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Date In</label>
								<input name="date_in" type="date" class="form-control" id="date_in" placeholder="Date in" value="" readonly>

							</div>
							<div class="col-md-12 position-relative">
								<label for="validationTooltip01" class="form-label">Doc Return</label>
								<input name="docreturn" type="date" class="form-control" id="date_out" placeholder="Doc Return" value="">

							</div>






							<div class="d-flex justify-content-end">

								<div class="col-md-0 position-relative">
									<input type="submit" name="submit" class="btn btn-primary">
								</div>
							</div>
						</form>
					</section>
				</div>
			</div>
		</div>
	</div> -->


<!-- Modal for editing data -->





<!--   Core JS Files   -->
<script src="<?= base_url() ?>/assets/js/jquery-3.7.1.js"></script>
<script src="<?= base_url() ?>/assets/js/uikit.min.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.uikit.js"></script>
<script src="<?= base_url() ?>/assets/js/select.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/js/dataTables.select.js"></script>
<script src="<?= base_url() ?>/assets/js/core/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/js/core/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/js/swal2.js"></script>
<script src="<?= base_url() ?>/assets/js/moment.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="<?= base_url() ?>/assets/js/plugins/chartjs.min.js"></script>

<script>
	var base_url = "<?php echo base_url(); ?>";
	var area = "";
	var lot = false;
	$("#isLot").change(function() {
		if (this.checked) {
			$("#divTagihan").css('display', 'block');
			lot = true;
		} else {
			$("#divTagihan").css('display', 'none');
			lot = false;
		}
	});

	function getPL(pl) {
		$.ajax({
			type: "POST",
			url: base_url + "Packing/checkPL",
			data: {
				no_pl: pl,
				area
			},
			cache: false,
			success: function(response) {
				if (response != "Not Exist") {
					console.log(response);
					// var json = jQuery.parseJSON(response);
					var dt = JSON.parse(response);
					$('#shipping').val(dt.method);
					$('#pl_id').val(dt.pl_id);
					$('#shippingName').val(dt.name);
					$('#noKendaraan').val(dt.no_vehicle);
					$('#noPengangkut').val(dt.no_shipping);
					$('#noVoyage').val(dt.no_voyage);
					var mydate = new Date(dt.date_out);
					if (dt.isLot == 0)
						lot = false;
					else
						lot = true;
					$('#isLot').prop('checked', lot);
					if (lot)
						$("#divTagihan").css('display', 'block');
					else
						$("#divTagihan").css('display', 'none');
					var date = moment(mydate).format('YYYY-MM-DD')
					if (!td.date_out.includes('1970'))
						$('#date_out').val(date);
					$('#tagihan').val(dt.tagihan);
				}
			}
		});
	}
	$('#bulkpl').change(function() {
		var no_pl = $(this).val();

		// alert(no_pl+"|"+area);
		getPL(no_pl, area)

	});

	function singlePl() {
		var shipping = $('#shipping').val();
		var noPL = $('#bulkpl').val();
		var shippingName = $('#shippingName').val();
		var noKendaraan = $('#noKendaraan').val();
		var noPengangkut = $('#noPengangkut').val();
		var noVoyage = $('#noVoyage').val();
		var date_out = $('#date_out').val();
		var date_out = $('#date_out').val();
		var tagihan = $('#tagihan').val();
		var pl_id = $('#pl_id').val();
		var data = {
			noPL: noPL,
			area: area,
			shipping: shipping,
			noKendaraan: noKendaraan,
			noPengangkut: noPengangkut,
			noVoyage: noVoyage,
			lot: lot,
			date_out: date_out,
			shippingName: shippingName,
			tagihan: tagihan,
			pl_id: pl_id,
		};
		// console.log(data);

		if (noPL.length < 1) {
			alert("Mohon isi NO PL d!");
		} else if (shipping == null) {
			alert("Mohon Pilih shipping method!");
		} else {
			Swal.fire({
				title: "Apakah anda akan menambahkan data?",
				// showDenyButton: true,
				showCancelButton: true,
				confirmButtonText: "Submit",
				cancelButtonText: "Tunggu"
			}).then((result) => {
				/* Read more about isConfirmed, isDenied below */
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						url: base_url + "Packing/updateSinglePL",
						data: data,
						cache: false,
						success: function(response) {
							console.log(response);
							if (response == "Success") {
								Swal.fire("Saved!", "", "success").then(function() {
									location.reload();
								});
							} else {
								Swal.fire("Gagal Update Data!", "", "error").then(function() {
									location.reload();
								});
							}
						}
					});
				} else {
					Swal.fire("Silahkan Periksa data kembali", "", "info");
				}
			});
		}
	}
</script>
<script>
	new DataTable('#example3');
	$(document).ready(function() {
		// $(".js-basic-multiple").select2({
		// 	dropdownParent: $('#exampleModalCenter .modal-content')

		// });
	});

	function editPL(a, b) {
		$('#insertPLModal').modal('show');
		$('#bulkpl').val(a);
		area = b;
		getPL(a, area);
	}
</script>
<!-- Github buttons -->
<script async defer src="<?= base_url() ?>/assets/js/buttons.js"></script>
<!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?= base_url() ?>/assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
</body>

</html>
