<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Calibri:400,700,400italic,700italic" rel="stylesheet" />
	<style>
		@page {
			size: A4;
			margin: 1rem;
			padding: 1rem;
		}
	</style>
</head>

<body>
	<div class="grid-container">
		<table style="border:none; border-spacing: 0px;width: 100%;">
			<tr>
				<td style="width: 60%; vertical-align:top">
					<h2 style="margin: 0px;">PT.BONA NUSANTARA RAYA SAKTI
					</h2>
					<h3 style="margin: 0px;">JL. GARUDA NO. 80F - KEMAYORAN JAKARTA PUSAT</h3>
					<h3 style="margin: 0px;">TELP: 021-4202660</h3>

				</td>
				<td style="width:40%; text-align:center;vertical-align: top;">
					<h2 style="margin: 0px;">INVOICE
					</h2>
					<h3 style="margin: 0px;"><?= $invoice_number ?>
					</h3>
				</td>
			</tr>
			<tr>
				<TD></TD>
			</tr>
		</table>
		<table style="border:none; border-spacing: 0px;width: 100%; margin-top: 1rem;">
			<tr>
				<td style="width: 60%; vertical-align:top">
					<h2 style="margin: 0px;"><?= $tujuan == "penerima" ? $corporate : $supplier ?>
					</h2>
					<?php
					$tujuan = [];
					foreach ($data as $key => $value) {
						foreach (array_keys($value) as $tuj => $val) {
							$tujuan[$val] = 0;
						}
					} ?>
					<h3 style="margin: 0px;">TUJUAN: <?= implode(",", array_keys($tujuan)) ?></h3>
					<h3 style="margin: 0px;">JAKARTA, <?= toIndoWord(localIndoToDate($invoice_date)) ?></h3>

				</td>
			</tr>
			<tr>
				<TD></TD>
			</tr>
		</table>
		<div class="table">
			<table>
				<thead>
					<tr>
						<th> NO <BR>URUT</th>
						<th> BIAYA PENGIRIMAN UNTUK BARANG</th>
						<th> JUMLAH <BR>COLLY</th>
						<th> JENIS <BR>COLLY</th>
						<th style="padding-left: 5px;padding-right: 5px;"> VOLUME / <BR>TONASE </th>
						<th style="padding-left: 5px;padding-right: 5px;"> TARIF <BR>RP</th>
						<th> JUMLAH <BR>RP</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 0;
					foreach ($col2 as $key => $value) {


						?>
						<tr>
							<td style="text-align: center;"><?= $col1[$no] ?></td>
							<td><?= $value ?></td>
							<td style="text-align: center;"><?= $col3[$no] ?></td>
							<td style="text-align: center;"><?= $col4[$no] ?></td>
							<td style="text-align: center;"><?= $col5[$no] ?></td>
							<td style="text-align: right;"><?= $col6[$no] ?></td>
							<td style="text-align: right;"><?= $col7[$no] ?></td>
						</tr>

						<?php $no++;
					} ?>
					<tr>
						<td></td>
						<td colspan="1" style="border-right: 1px solid;">
							<?php if ($rekening_type == "PPN"): ?>
								<p style="font-family: 'Calibri'; margin-top:0.2rem">
									NAMA&nbsp;&nbsp;: PT.BONA
									NUSANTARA RAYA SAKTI
									<br>
									BANK
									&nbsp;&nbsp;:
									BRI<br>
									AC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
									<strong>0434-01-001066-303</strong><br>

								</p>

							<?php else: ?>
								<p style="font-family: 'Calibri'; margin-top:0.2rem">
									NAMA&nbsp;&nbsp;: RICO FAHLEVI
									HASIBUAN
									<br>
									BANK
									&nbsp;&nbsp;:
									BCA<br>
									AC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong>6.840.299.161</strong><br>

								</p>
							<?php endif; ?>

						</td>

						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td rowspan="<?= $rekening_type == 'PPN' ? '2' : '1' ?>"
							style="font-weight: bold;border: 1px solid;">
							<div>
								CATATAN : <br>
								DIHARAPKAN MENCANTUMKAN KETERANGAN NOMOR<BR>
								KWITANSI SAAT MELAKUKAN PEMBAYARAN
							</div>
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td style="text-align: center;font-weight: bold;border: 1px solid;">JUMLAH</td>
						<td style="text-align: right;font-weight: bold;border: 1px solid;"><?= $total ?></td>
					</tr>
					<?php if ($rekening_type == 'PPN'): ?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="1" style="text-align: center; font-weight: bold;border: 1px solid;">PPN</td>
							<td style="text-align: center;">1,1%</td>
							<td style="text-align: right;"><?= $ppn ?></td>

						</tr>
					<?php endif; ?>
					<tr>
						<td colspan="2" style="text-align: center;font-weight: bold;border: 1px solid;"></td>
						<td colspan="1" style="text-align: center;font-weight: bold;border: 1px solid"><?= $totqty ?>
						</td>
						<td colspan="1" style="text-align: center;font-weight: bold;border: 1px solid">COLLY</td>
						<td colspan="2" style="text-align: center;border: 1px solid">TOTAL</td>
						<td colspan="1" style="text-align: center;border: 1px solid"><?= $allTotal ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<table style="width: 100%;">
			<tr>
				<td style="width:50%; text-align:left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami</td>

			</tr>
			<tr>
				<td style="width:50%; text-align:left;padding-top: 50px;">
					(Rico Fahlevi Hasibuan)
				</td>

			</tr>
		</table>
	</div>
</body>

</html>
<style>
	p {
		margin: 0;
	}
	/* .table table tbody tr td */
	.table>table {
		/* border: 10px; */
		border-collapse: collapse;
		width: 100%;
		border: 1px solid;
	}

	td {
		padding: 5px;
	}

	.table table tbody tr td {
		/* border: 10px; */
		border-right: 1px solid;
	}

	.table table thead tr th {
		/* border: 10px; */
		border: 1px solid;
	}



	.grid-container {
		/* display: grid; */
		font-family: 'Calibri';
		font-style: normal;
		font-size: 11px;
		/* grid-template-areas:
			'header header header header header header'
			'main main main main main main'
			'thead thead thead thead thead thead'
			'table table table table table table'
			'footer footer footer footer footer footer'; */
		/* gap: 5px; */
		/* background-color: #2196F3; */
		padding: 10px 10px 10px 10px;
	}

	.grid-container>div {
		/* background-color: rgba(255, 255, 255, 0.8); */
		/* text-align: center; */
		/* padding: 5px 0; */
	}
</style>
