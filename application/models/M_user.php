<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{
	function init()
	{
		$this->db->select('users.*, role.role', false);
		$this->db->join('role', 'users.role_id = role.id');
		return $this->db->get('users');
	}
	public function login($table, $where)
	{
		return $this->db->get_where($table, $where);
	}
	public function updateLogin($id, $bool)
	{
		$this->db->where('id', $id);
		return $this->db->update('users', array('isLogin' => $bool));
	}
	public function updateUser($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update('users', $data);
	}
	public function add_user($data)
	{
		return $this->db->insert('users', $data);
	}
	public function delete_user($id)
	{
		return $this->db->delete('users', array('id'=>$id));
	}

}

/* End of file M_user.php */
