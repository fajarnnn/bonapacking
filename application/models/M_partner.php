<?php

defined('BASEPATH') or exit('No direct script access allowed');
class M_partner extends CI_Model
{

    public function init($table, $group = null)
    {
        $this->db->select()->from($table);

        if (isset($group)) {
            $this->db->group_by($group);
        }
        return $this->db->get();
    }
    public function getDetail($table, $where)
    {
        $this->db->group_by(array('no_po', 'no_sji'));
        return $this->db->get_where($table, $where);
    }
    public function getPrintsDetail($table, $where)
    {
        // $this->db->group_by(array('no_po','no_sji'));
        return $this->db->get_where($table, $where);
    }


    public function data_detail($id)
    {
        return $this->db->get_where('packing_list', ['id' => $id])->row_array();
    }


    public function add_data($data)
    {
        return $this->db->insert('packing_list', $data);
    }

    public function edit_data_all()
    {
        $data = [
            'no_pl' => $this->input->post('no_pl'),
            'no_po' => $this->input->post('no_po'),
            'no_sji' => $this->input->post('no_sji'),
            'date_in' => $this->input->post('date_in'),
            'date_out' => $this->input->post('date_out'),
            'supplier' => $this->input->post('supplier'),
            'material' => $this->input->post('material'),
            'qty' => $this->input->post('qty'),
            'satuan' => $this->input->post('satuan'),
            'panjang' => $this->input->post('panjang'),
            'lebar' => $this->input->post('lebar'),
            'tinggi' => $this->input->post('tinggi'),
            'kubikasi' => $this->input->post('kubikasi'),
            'berat' => $this->input->post('berat'),
            'area' => $this->input->post('area'),
            'corporate' => $this->input->post('corporate'),
            'address' => $this->input->post('addres'),
            'street' => $this->input->post('street'),
            'pic' => $this->input->post('pic'),
            'plant' => $this->input->post('plant'),
            'doc_return' => $this->input->post('doc_return'),
            'cost_kubik' => $this->input->post('cost_kubik'),
            'cost_berat' => $this->input->post('cost_berat'),
            'tagihan_kubik' => $this->input->post('tagihan_kubik'),
            'tagihan_berat' => $this->input->post('tagihan_berat'),
            'no_pot' => $this->input->post('nopot'),
            'remark' => $this->input->post('remark'),
            'invoice_date' => $this->input->post('invoice_date'),
            'no_invoice' => $this->input->post('no_invoice'),
            'list_group' => $this->input->post('list_group'),
            'vessel_name' => $this->input->post('vessel_name'),
            'vessel_no_cont' => $this->input->post('vessel_no_cont'),
            'vessel_no_segel' => $this->input->post('vessel_no_segel'),
            'input_by' => $this->input->post('input_by')
        ];

        $this->db->where('id', $this->input->get('id'));
        $this->db->update('packing_list', $data);
    }
}
