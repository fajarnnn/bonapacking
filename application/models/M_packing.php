<?php

defined('BASEPATH') or exit('No direct script access allowed');
class M_packing extends CI_Model
{

	public function init($table, $group = null, $where = null)
	{
		$this->db->select()->from($table);
		$this->db->select_max('shipping');
		$this->db->select_max('driver');
		$this->db->select_max('vessel_name');
		if (isset($where))
			$this->db->where($where);

		$this->db->order_by('no_pl', 'DESC')
			->order_by('area', 'ASC')
			->order_by('no_po', 'ASC')
			->order_by('corporate', 'ASC');
		if (isset($group)) {
			$this->db->group_by($group);
		}
		return $this->db->get();
	}
	public function plDashboard($where = null)
	{

		$this->db->join('shipping', 'shipping.pl_id = pl.id');
		$this->db->order_by('no_pl', 'DESC')
			->order_by('area', 'ASC');
		if ($where != null)
			return $this->db->get_where('pl', $where);
		else
			return $this->db->get('pl');
	}
	public function getArchive($where = null)
	{
		$where = array(
			'po_list.doc_return !=' => NULL,
			'po_list.unloading_date !=' => NULL,
			'pl.date_out !=' => NULL,
			'i.invoice_date !=' => NULL,
			'i.invoice_date !=' => "",
			'i.no_invoice !=' => NULL,
			'i.payment_date !=' => NULL
		);
		$this->db->select('pl.*, po_list.*, i.*');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->where($where);
		$this->db->group_by('i.no_invoice');
		$query = $this->db->get();
		return $query;
	}
	public function partnerDashboard($where = null)
	{

		$this->db->join('shipping', 'shipping.pl_id = pl.id');
		$this->db->join('po_list p', 'p.pl_id = pl.id');
		$this->db->group_by('pl.id');
		$this->db->order_by('no_pl', 'DESC')
			->order_by('area', 'ASC');
		if ($where != null)
			return $this->db->get_where('pl', $where);
		else
			return $this->db->get('pl');
	}
	public function getPLID($where)
	{
		return $this->db->get_where("pl", $where)->row();
	}
	public function getPoList($where)
	{
		$this->db->join('invoice i', 'i.id_po = po_list.id');
		$this->db->order_by('po_list.list_group', 'asc');
		$this->db->order_by('po_list.corporate', 'asc');
		return $this->db->get_where("po_list", $where)->result();
	}
	public function getSJN($where)
	{
		$where = array(
			'pl.no_pl !=' => "-"
		);
		$this->db->select("*, pol.id as id_pol");
		$this->db->join('po_list pol', 'pl.id = pol.pl_id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		$this->db->where("pol.no_sji is not NULL");
		$this->db->where("pl.no_pl is not NULL");
		// $this->db->group_by(array('pl.no_pl', 'pl.area', 'pol.no_sji'));
		$this->db->order_by('pl.id', 'DESC')
			->order_by('pl.area', 'ASC');
		return $this->db->get_where("pl", $where)->result();
	}
	public function getSJNMaterial($where)
	{
		$this->db->join('po_list pol', 'pl.id = pol.pl_id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		$this->db->join('material m', 'm.id_po = pol.id');
		// $this->db->group_by(array('no_pl','area'));
		$this->db->order_by('pl.no_pl', 'DESC')
			->order_by('pl.area', 'ASC');
		return $this->db->get_where("pl", $where)->result();
	}
	public function getSJNGroup($where)
	{
		$this->db->join('po_list pol', 'pol.pl_id = pl.id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		$this->db->group_by(array('pl.no_pl', 'pl.area', 'pol.no_sji'));
		$this->db->order_by('pl.no_pl', 'DESC')
			->order_by('pl.area', 'ASC');
		return $this->db->get_where("pl", $where)->result();
	}
	public function getDetail($table, $where)
	{
		$this->db->group_by(array('no_po', 'no_sji'));
		return $this->db->get_where($table, $where);
	}
	public function getMaterial($where)
	{
		// $this->db->group_by(array('no_po', 'no_sji'));
		return $this->db->get_where("material", $where);
	}
	public function getImages($where)
	{
		// $this->db->group_by(array('no_po', 'no_sji'));
		return $this->db->get_where("images", $where);
	}
	public function deleteImages($where)
	{
		// $this->db->group_by(array('no_po', 'no_sji'));
		return $this->db->delete("images", $where);
	}
	public function getCorporate()
	{
		$this->db->select('list_group')->from('packing_list');
		$this->db->group_by('list_group', 'asc');
		return $this->db->get();
	}
	public function getPrintsDetail($table, $where)
	{
		// $this->db->group_by(array('no_po','no_sji'));
		return $this->db->get_where($table, $where);
	}


	public function data_detail($id)
	{
		return $this->db->get_where('packing_list', ['id' => $id])->row_array();
	}


	public function add_data($data)
	{
		return $this->db->insert('packing_list', $data);
	}
	function dataAdd($data)
	{
		$isplexist = false;
		$idpl = 0;
		$idpo = 0;
		foreach ($data as $key => $value) {
			# code...
			if ($key == "pl") {
				$q = $this->db->get_where("pl", array('no_pl' => $value['no_pl'], 'area' => $value['area']));
				$cnt = $q->num_rows();
				if ($cnt > 0) {
					$isplexist = true;
					$idpl = $q->result()[0]->id;
					$this->db->where('id', $idpl);
					$this->db->update($key, $value);
				} else {
					if ($this->db->insert($key, $value)) {
						$idpl = $this->db->insert_id();
					} else {
						return false;
					}
				}
			} else if ($key == "shipping") {
				if ($isplexist) {
					$this->db->where('pl_id', $idpl);
					$this->db->update($key, $value);
				} else {
					$value['pl_id'] = $idpl;
					if (!$this->db->insert($key, $value)) {
						return false;
					}
				}
			} else if ($key == "po_list") {
				$value['pl_id'] = $idpl;

				if ($this->db->insert($key, $value)) {

					$idpo = $this->db->insert_id();
				} else {
					return false;
				}
			} else if ($key == "invoice") {

				$value['id_po'] = $idpo;
				if (!$this->db->insert($key, $value)) {
					return false;
				}
			} else if ($key == "material") {

				for ($i = 0; $i < count($value['material']); $i++) {
					$datas['id_po'] = $idpo;
					$datas['material'] = $value['material'][$i];
					$datas['panjang'] = $value['panjang'][$i];
					$datas['lebar'] = $value['lebar'][$i];
					$datas['tinggi'] = $value['tinggi'][$i];
					$datas['kubikasi'] = $value['kubikasi'][$i];
					$datas['qty'] = $value['qtty'][$i];
					$datas['satuan'] = $value['satuan'][$i];
					$datas['berat'] = $value['berat'][$i];
					$datas['cost'] = $this->convertCurrency($value['cost'][$i]);
					$datas['costbill'] = $value['costbill'][$i];
					$datas['tonase'] = $value['tonase'][$i];
					$datas['totalcost'] = $this->convertCurrency($value['totalcost'][$i]);
					if (!$this->db->insert($key, $datas)) {
						return false;
					}
				}
			}
		}
		return true;
	}
	function dataEdit($data)
	{
		$idpl = 0;
		$isplexist = false;
		$idpo = 0;
		$area = "";
		$upload = isset($data['upload']) ? $data['upload'] : "";
		foreach ($data as $key => $value) {
			if ($key == "pl") {
				$q = $this->db->get_where("pl", array('no_pl' => $value['no_pl'], 'area' => $value['area']));
				$cnt = $q->num_rows();
				if ($cnt > 0) {
					$isplexist = true;
					$idpl = $q->result()[0]->id;
					$this->db->where('id', $idpl);
					$this->db->update($key, $value);
				} else {
					if ($this->db->insert($key, $value)) {
						$idpl = $this->db->insert_id();
					} else {
						return false;
					}
				}
			} else if ($key == "shipping") {
				if ($isplexist) {
					$this->db->where('pl_id', $idpl);
					$this->db->update($key, $value);
				} else {
					$value['pl_id'] = $idpl;
					if (!$this->db->insert($key, $value)) {
						return false;
					}
				}
			} else if ($key == "po_list") {

				$value['pl_id'] = $idpl;
				$idpo = $value['id'];
				$this->db->where('id', $idpo);
				$this->db->update($key, $value);
			} else if ($key == "invoice") {
				$this->db->where('id_po', $idpo);
				$this->db->update($key, $value);
			} else if ($key == "material") {

				for ($i = 0; $i < count($value['material']); $i++) {
					$matid = $value['id'][$i];
					$datas['id_po'] = $idpo;
					$datas['material'] = $value['material'][$i];
					$datas['panjang'] = $value['panjang'][$i];
					$datas['lebar'] = $value['lebar'][$i];
					$datas['tinggi'] = $value['tinggi'][$i];
					$datas['kubikasi'] = $value['kubikasi'][$i];
					$datas['qty'] = $value['qtty'][$i];
					$datas['satuan'] = $value['satuan'][$i];
					$datas['berat'] = $value['berat'][$i];
					$datas['cost'] = $this->convertCurrency($value['cost'][$i]);
					$datas['costbill'] = $value['costbill'][$i];
					$datas['tonase'] = $value['tonase'][$i];
					$datas['totalcost'] = $this->convertCurrency($value['totalcost'][$i]);
					// echo "<pre>";
					if (!isset($matid)) {
						if (!$this->db->insert($key, $datas)) {
							return false;
						}
					} else {
						$this->db->where('id', $matid);
						$this->db->update($key, $datas);
					}
				}
			} else if ($key == "images") {
				foreach ($value as $key5 => $val) {
					$datai['id_po'] = $idpo;
					$datai['path'] = $upload;
					$datai['filename'] = $val['file_name'];
					$datai['ext'] = $val['file_ext'];
					$datai['size'] = $val['file_size'];
					$datai['mime'] = $val['file_type'];
					$this->db->insert($key, $datai);
				}
			}
		}
		return true;
	}
	function saveImages($data, $idpo, $upload)
	{
		foreach ($data as $key => $val) {
			$datai['id_po'] = $idpo;
			$datai['path'] = $upload;
			$datai['filename'] = $val['file_name'];
			$datai['ext'] = $val['file_ext'];
			$datai['size'] = $val['file_size'];
			$datai['mime'] = $val['file_type'];
			// 	echo "<pre>";
			// var_dump($datai);
			// echo "</pre>";
			// exit();
			$this->db->insert('images', $datai);
		}
	}
	function rejectDoc($id)
	{
		$this->db->where('id', $id);
		$data = array(
			'unloading_date' => NULL,
			'doc_return' => NULL
		);
		return $this->db->update('po_list', $data);
	}
	function rejectInvoice($id)
	{
		$this->db->where('no_invoice', $id);
		$data = array(
			'no_invoice' => NULL,
			'invoice_date' => NULL,
			'printed' => 0,
			'invoice_name' => NULL,
			'ppn' => 0,
			'in_kubikasi' => 0,
			'discount' => NULL,
			'additional_cost' => 0,
			'down_payment' => 0,
			'tujuan' => NULL,
			'rekening_type' => NULL,
			'cost_type' => NULL
		);
		return $this->db->update('invoice', $data);
	}
	function rejectPL($id)
	{
		$this->db->where('id', $id);
		$data = array(
			'date_out' => NULL,
		);
		return $this->db->update('pl', $data);
	}
	function convertCurrency($curr)
	{
		return preg_replace('/[^0-9]/', '', $curr);
	}
	public function edit_data_all($data, $id)
	{

		$this->db->where('id', $id);
		$this->db->update('packing_list', $data);
	}
	public function delete_material($id)
	{
		return $this->db->delete('material', array('id' => $id));
	}
	public function deletePO($where)
	{
		return $this->db->delete('packing_list', $where);
	}
	public function poDelete($poid)
	{
		$this->db->delete('material', array('id_po' => $poid));
		$this->db->delete('invoice', array('id_po' => $poid));
		return $this->db->delete('po_list', array('id' => $poid));
	}
	public function genPdf($where)
	{
		$this->db->join('po_list pol', 'pol.pl_id = pl.id');
		$this->db->join('material m', 'm.id_po = pol.id');
		$this->db->join('shipping s', 's.pl_id = pl.id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		$this->db->order_by('pol.list_group', 'asc');
		return $this->db->get_where('pl', $where);
	}
	public function joinAll()
	{
		$this->db->join('po_list pol', 'pol.pl_id = pl.id');
		$this->db->join('material m', 'm.id_po = pol.id');
		$this->db->join('shipping s', 's.pl_id = pl.id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		return $this->db->get('pl');
	}
	public function updateBulk($is, $data)
	{
		if ($is == "pl") {
			if (isset($data['pl_id'])) {
				$this->db->where('id', $data['pl_id']);
				unset($data["pl_id"]);
				return $this->db->update($is, $data);
			} else {
				unset($data["pl_id"]);
				$this->db->insert($is, $data);
				return $this->db->insert_id();
			}
		} else
			if ($is == "po_list") {
			$this->db->where_in('id', $data['id']);
			$this->db->set('pl_id', $data['pl_id']);
			return $this->db->update($is);
		} else if ($is == "shipping") {
			if (isset($data['exist'])) {
				unset($data["exist"]);
				$this->db->where('pl_id', $data['pl_id']);
				// unset($data["pl_id"]);
				return $this->db->update($is, $data);
			} else {
				$this->db->insert($is, $data);
			}
		}
	}
	function getdocreturn()
	{
		$this->db->select('po_list.*, pl.no_pl, pl.area, pl.date_out');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->where('(po_list.doc_return is NULL OR po_list.unloading_date is NULL)');
		$this->db->where('pl.date_out !=', NULL);
		$query = $this->db->get();
		return $query;
	}
	function addInvoice($data, $ids)
	{
		// echo "PRINT";
		$this->db->where_in('id_po', $ids);
		return $this->db->update('invoice', $data);
	}
	function printInvoice($invoiceNumber)
	{
		$where = array(
			'i.no_invoice' => $invoiceNumber
		);
		$this->db->select('po_list.id,po_list.pl_id, po_list.corporate, po_list.address, po_list.plant, po_list.no_po, po_list.no_sji, po_list.list_group, po_list.doc_return, po_list.unloading_date, pl.no_pl, pl.area, pl.date_out, i.*, m.*');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->join('material m', 'po_list.id = m.id_po');
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}
	function getInvoices()
	{
		$where = array(
			'po_list.doc_return !=' => NULL,
			'po_list.unloading_date !=' => NULL,
			'pl.date_out !=' => NULL,
		);
		$this->db->select('po_list.*, pl.no_pl, pl.area, pl.date_out');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->where('(i.invoice_date IS NULL OR i.no_invoice is NULL OR LENGTH(i.invoice_date) < 2 )');
		$this->db->where($where);

		$query = $this->db->get();
		return $query;
	}
	function getCollection()
	{
		$where = array(
			'po_list.doc_return !=' => NULL,
			'po_list.unloading_date !=' => NULL,
			'pl.date_out !=' => NULL,
			'i.invoice_date !=' => NULL,
			'i.invoice_date !=' => "",
			'i.no_invoice !=' => NULL,
			'i.payment_date ' => NULL
		);
		$this->db->select('po_list.*, GROUP_CONCAT(po_list.no_po separator "<br>") as no_pos, pl.no_pl, pl.area, pl.date_out, i.invoice_date, i.no_invoice, i.invoice_name, i.cost_type, i.rekening_type');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->where($where);
		$this->db->group_by('i.no_invoice');
		$query = $this->db->get();
		return $query;
	}

	function getCollectionPPN()
	{
		$where = array(
			'po_list.doc_return !=' => NULL,
			'po_list.unloading_date !=' => NULL,
			'pl.date_out !=' => NULL,
			'i.invoice_date !=' => NULL,
			'i.invoice_date !=' => "",
			'i.no_invoice !=' => NULL,
			'i.payment_date ' => NULL,
			'i.rekening_type' => "PPN"
		);
		$this->db->select('po_list.*, GROUP_CONCAT(po_list.no_po separator "<br>") as no_pos, pl.no_pl, pl.area, pl.date_out, i.invoice_date, i.no_invoice, i.invoice_name, i.cost_type, i.rekening_type');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->where($where);
		$this->db->group_by('i.no_invoice');
		$query = $this->db->get();
		return $query;
	}
	function getCollectionNonPPN()
	{
		$where = array(
			'po_list.doc_return !=' => NULL,
			'po_list.unloading_date !=' => NULL,
			'pl.date_out !=' => NULL,
			'i.invoice_date !=' => NULL,
			'i.invoice_date !=' => "",
			'i.no_invoice !=' => NULL,
			'i.payment_date ' => NULL,
			'i.rekening_type' => "NON PPN"
		);
		$this->db->select('po_list.*, GROUP_CONCAT(po_list.no_po separator "<br>") as no_pos, pl.no_pl, pl.area, pl.date_out, i.invoice_date, i.no_invoice, i.invoice_name, i.cost_type, i.rekening_type');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->where($where);
		$this->db->group_by('i.no_invoice');
		$query = $this->db->get();
		return $query;
	}
	function updateDocReturn($id, $data)
	{
		// 	echo "<pre>";
		// var_dump($data);
		// echo "</pre>";
		// exit();
		$this->db->where('id', $id);
		return $this->db->update('po_list', $data);
	}
	function updateInvoice($ids, $data)
	{
		$this->db->where_in('id_po', $ids);
		return $this->db->update('invoice', $data);
	}
	function updatePayment($ids, $data)
	{
		$this->db->where('no_invoice', $ids);
		return $this->db->update('invoice', $data);
	}
	function removeEMptyPL()
	{
		$q = "DELETE FROM shipping where pl_id in (SELECT pl.id from pl LEFT JOIN po_list on pl.id = po_list.pl_id where po_list.pl_id is null)";
		$this->db->query($q);
		$q = "DELETE FROM pl where id in (SELECT pl.id from pl LEFT JOIN po_list on pl.id = po_list.pl_id where po_list.pl_id is null)";
		return $this->db->query($q);
	}
	public function singlePL($is, $data)
	{
		if ($is == "pl") {
			$this->db->where('id', $data['id']);
			unset($data["id"]);
			return $this->db->update($is, $data);
		} else if ($is == "shipping") {
			$this->db->where('pl_id', $data['pl_id']);
			$this->db->update($is, $data);
		}
	}
	function checkPLexistant($data)
	{
		$this->db->join('shipping s', 's.pl_id = pl.id');
		return $this->db->get_where("pl", array('no_pl' => $data['no_pl'], 'area' => $data['area']));
	}
}
