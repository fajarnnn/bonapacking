<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_accounting extends CI_Model
{

	public function login()
	{


	}
	function addInvoice($data, $ids)
	{
		// echo "PRINT";
	
		$this->db->where_in('id_po', $ids);
		return $this->db->update('invoice', $data);
	}
	function updMat($data, $idmat)
	{
		// echo "PRINT";
		$this->db->where('id', $idmat);
		return $this->db->update('material', $data);
	}
	function getTotalHarga($ids){
		$this->db->select("sum(m.totalcost) as tot, sum(CAST(m.kubikasi AS DECIMAL( 9, 3 ))) as kub");
		$this->db->from('po_list pol');
		$this->db->join('material m', 'm.id_po = pol.id');
		$this->db->where_in('id_po', $ids);
		return $this->db->get();
	}	
	function getlatestPPN(){
		$this->db->select("max(SUBSTRING(no_invoice, -4)) as lastest");
		$this->db->like('no_invoice', 'KW-BONA', 'after');
		return $this->db->get_where('invoice');
	}
	function getlatestNonPPN(){
		$this->db->select("max(SUBSTRING(no_invoice, -4)) as lastest");
		$this->db->like('no_invoice', 'KW-BNRS', 'after');
		return $this->db->get_where('invoice');
	}
	function printInvoice($invoiceNumber)
	{
		$where = array(
			'i.no_invoice' => $invoiceNumber
		);
		$this->db->select('po_list.id,po_list.pl_id, po_list.corporate, po_list.supplier, po_list.address, po_list.plant, po_list.no_po, po_list.no_sji, po_list.list_group, po_list.doc_return, po_list.unloading_date, pl.no_pl, pl.area, pl.date_out, i.*, m.*');
		$this->db->from('po_list');
		$this->db->join('pl', 'po_list.pl_id = pl.id', 'inner');
		$this->db->join('invoice i', 'po_list.id = i.id_po');
		$this->db->join('material m', 'po_list.id = m.id_po');
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}
	public function genPdf($where)
	{
		$this->db->select("*, m.id as idmat");
		$this->db->join('po_list pol', 'pol.pl_id = pl.id');
		$this->db->join('material m', 'm.id_po = pol.id');
		$this->db->join('shipping s', 's.pl_id = pl.id');
		$this->db->join('invoice i', 'i.id_po = pol.id');
		return $this->db->get_where('pl', $where);
	}

}

/* End of file M_Accounting.php */

